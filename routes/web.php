<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['register' => false]);
Route::group(['middleware' => ['web']], function () {
    Route::get('/clear-cache', function () {
        $exitCode = Artisan::call('config:clear');
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('config:cache');
        return 'DONE'; //Return anything
    });
});
Route::get('/customer-home', 'Frontend\\CustomerController@index');


Route::get('access-denied', function () {
    return view('admin.access-denied');
})->middleware('auth:admin')->name('access-denied');


Route::prefix('siteadmin')->group(function () {

    Route::get('/', 'Admin\\AdminController@index')->name('admin.home');
    Route::get('/my-profile', 'Admin\\AdminController@adminProfile');
    Route::post('/my-profile', 'Admin\\AdminController@updateUser');
    Route::get('/login', 'Auth\\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\\AdminLoginController@logout')->name('admin.logout');
    Route::get('/register', 'Auth\\AdminRegisterController@showAdminRegisterForm')->name('admin.register');
    Route::post('/register', 'Auth\\AdminRegisterController@create')->name('admin.register.submit');
    Route::get('/doc-dels', 'Admin\\AdminController@frontendUsers')->name('doc-dels');
    Route::get('/doc-dels/make-verified/{id}', 'Admin\\AdminController@frontendUsersMakeVerified')->name('doc-dels.make-verified');
    Route::get('/doc-dels/make-rejected/{id}', 'Admin\\AdminController@frontendUsersMakeRejected')->name('doc-dels.make-rejected');

    Route::get('/changeUserStatus', 'Admin\\UserController@changeUserStatus')->name('user.change_status');
    Route::resource('/users', 'Admin\\UserController');
    Route::resource('/user-role', 'Admin\\UserRoleController');
    Route::resource('/pages', 'Admin\\PagesController');
    Route::resource('/testimonials', 'Admin\\TestimonialsController');
    Route::resource('/blog', 'Admin\\BlogController');
    Route::resource('/factory-images', 'Admin\\FactoryImageController');
    Route::resource('/activities', 'Admin\\ActivityController');
    Route::resource('/news', 'Admin\\NewsController');
    Route::resource('/category', 'Admin\\CategoryController');
    Route::resource('/settings', 'Admin\\SettingsController');
    Route::resource('/product', 'Admin\\ProductsController');
    Route::resource('/tenders', 'Admin\\TendersController');
    Route::resource('/departments', 'Admin\\DepartmentsController');
    Route::resource('/media-images', 'Admin\\MediaImagesController');
    Route::resource('/media-videos', 'Admin\\MediaVideosController');
    Route::resource('/contacts', 'Admin\\ContactsController');
    Route::resource('/banner', 'Admin\\BannerController');
    Route::resource('/home-cms', 'Admin\\HomePageController');
    Route::post('/quotations/fetch', 'Admin\\QuotationsController@fetch')->name('admin.product.fetch');
    Route::post('/quotations/fetchPrice', 'Admin\\QuotationsController@fetchPrice')->name('admin.product.price');
    Route::post('/quotations/updateStatus/{id}', 'Admin\\QuotationsController@updateStatus');
    Route::get('/product-report', 'Admin\\QuotationsController@productWiseReport');
    Route::get('/quotation-product-report', 'Admin\\QuotationsController@productWiseReport');
    Route::get('/dealer-report', 'Admin\\QuotationsController@dealerWiseReport');
    Route::get('/quotation-report', 'Admin\\QuotationsController@QuotationReport');
    Route::get('/performance-report', 'Admin\\QuotationsController@PerformanceReport');
    Route::get('/quotation-category-report', 'Admin\\QuotationsController@categoryWiseReport');

    Route::get('/cancelled-quotations', 'Admin\\QuotationsController@cancelledQuotations');
    Route::resource('/quotations', 'Admin\\QuotationsController');
    Route::get('/raw-materials/importData', 'Admin\\RawMaterialsController@importData');
    Route::post('/raw-materials/importData', 'Admin\\RawMaterialsController@importDataPost');
    Route::resource('/raw-materials', 'Admin\\RawMaterialsController');

    Route::resource('/notifications', 'Admin\\NotificationsController');
    Route::resource('/careers', 'Admin\\CareersController');
    Route::resource('/downloads', 'Admin\\DownloadsController');
    Route::resource('/addresses', 'Admin\\AddressController');

});


Route::group(['middleware' => 'auth'], function () {

    /*
      * manage admin routes
      * under the auth and admin middleware
    */
    Route::group(['middleware' => 'user-verification'], function () {
        Route::group(['prefix' => 'user'], function () {

            Route::get('/', 'User\\HomeController@index')->name('user.home');
            Route::get('/home', 'User\\HomeController@index')->name('user.home');
            Route::get('profile', 'User\\HomeController@profile')->name('user.profile');
            Route::post('profile', 'User\\HomeController@profileSubmit')->name('user.profileSubmit');
            Route::get('password', 'User\\HomeController@password')->name('user.password');
            Route::post('password', 'User\\HomeController@passwordSubmit')->name('user.passwordSubmit');
            Route::post('/quotations/fetch', 'User\\QuotationController@fetch')->name('user.product.fetch');
            Route::post('/quotations/fetchPrice', 'User\\QuotationController@fetchPrice')->name('user.product.price');
            Route::post('/cancel-quotation', 'User\\QuotationController@cancelQuotation');
            Route::get('/cancelled-quotations', 'User\\QuotationController@cancelledQuotations');
            Route::resource('quotations', 'User\\QuotationController');
            Route::resource('notifications', 'User\\NotificationsController');
        });
    });
});

Route::group(['middleware' => ['web']], function () {


    Route::get('page-not-found', function () {
        return view('frontend.page-not-found');
    })->name('page-not-found');

    Route::get('/', 'Frontend\\HomeController@index')->name('/');
    Route::get('about-us', 'Frontend\\PagesController@aboutUs')->name('about-us');
    Route::get('activities', 'Frontend\\PagesController@activities')->name('activities');
    Route::get('media', 'Frontend\\MediaController@index')->name('media');
    Route::get('media/{media_id}', 'Frontend\\MediaController@details');
    Route::get('tenders', 'Frontend\\PagesController@tenders')->name('tenders');
    Route::get('contact-us', 'Frontend\\PagesController@contactUs')->name('contact-us');
    Route::post('contact-us', 'Frontend\\PagesController@contactUsFormSubmit')->name('contact-us.formSubmit');
    Route::get('careers', 'Frontend\\PagesController@careers')->name('careers');
    Route::post('careers', 'Frontend\\PagesController@careersFormSubmit')->name('careers.formSubmit');
    Route::get('products/{featured?}', 'Frontend\\ProductController@index')->name('products');
    Route::get('s-products/{featured?}', 'Frontend\\ProductController@sProducts')->name('s-products');
    Route::get('news', 'Frontend\\NewsController@index')->name('news');
    Route::get('news/{slug}', 'Frontend\\NewsController@details');
    Route::get('blog/{slug}', 'Frontend\\PagesController@blog');
    Route::get('c/{slug?}/{featured?}', 'Frontend\\ProductController@index');
    Route::get('cs/{slug?}/{featured?}', 'Frontend\\ProductController@sProductsCategory');
    Route::get('product/{slug?}', 'Frontend\\ProductController@details');
    Route::get('verify-account/{user_id}/{cus_token}', 'Frontend\\HomeController@verifyAccount');
    Route::get('news', 'Frontend\\NewsController@index')->name('news');
    Route::get('directors', 'Frontend\\PagesController@boardDirectors')->name('directors');
    Route::get('officers', 'Frontend\\PagesController@officers')->name('officers');
    Route::get('downloads', 'Frontend\\PagesController@downloads')->name('downloads');
    Route::get('departments', 'Frontend\\PagesController@departments')->name('departments');
    Route::get('raw-materials-required', 'Frontend\\PagesController@rawMaterialReq')->name('raw-materials-required');
    Route::post('raw-materials-required', 'Frontend\\PagesController@rawMaterialReqEnq')->name('raw-materials-required');
    Route::get('r-and-d-activities', 'Frontend\\PagesController@activitiesOfRD')->name('r-and-d-activities');
    Route::get('hospital', 'Frontend\\PagesController@hospital')->name('hospital');
    Route::post('set-lang-session','Frontend\HomeController@set_language')->name('set.lang');

    Route::any('/{url?}/{id?}', 'Frontend\PagesController@index');
});
