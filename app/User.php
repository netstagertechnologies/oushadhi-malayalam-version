<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use App\Notifications\PasswordReset;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','phone','district','state','user_type',
        'address','cus_token','status','is_verified','is_admin_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUserDetails($user_id){
        $user=DB::table('users')->select('name', 'email', 'mobile','phone','district','state','user_type','address','is_verified')
            ->where('id',(int)$user_id)->first();
        return $user;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }
    public static function getUserType($user_id){
        $user=DB::table('users')->select('user_type')
            ->where('id',(int)$user_id)->first();
        if($user)
            return $user->user_type;
        return 1;
    }
}
