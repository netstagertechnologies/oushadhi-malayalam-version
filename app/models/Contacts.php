<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    protected $primaryKey = 'contact_id';
    public $timestamps = false;
    protected $fillable = [
        'name' ,'name_ml' , 'res_no' ,'mob_no',  'designation' ,'designation_ml','email', 'status' ,'contact_type','sort_order', 'date_added' , 'date_modified',
    ];
}
