<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
class RawMaterial extends Model
{
    protected $table = 'raw_materials';

    protected $primaryKey = 'raw_material_id';
    public $timestamps = false;
    protected $fillable = [
        'title', 'title_en', 'title_hn','image', 'size', 'botanical_name', 'quantity','raw_type',
        'status', 'date_added', 'date_modified',
    ];

    public static function getRawMaterialTypeTitle($raw_type=0){

       return DB::table('raw_material_types')->select(DB::raw('IFNULL(title,\'NA\') as title'))->where('id', (int)$raw_type)->value('title');

    }
}
