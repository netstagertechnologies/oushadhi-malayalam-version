<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notifications extends Model
{

    public static function getUnreadNotificationsCount($user_id,$type)
    {
        return DB::table('notifications')->select('*')
            ->where('read_status', 0)->where('user_id', (int)$user_id)
            ->where('type',$type)
            ->count();

    }

    public static function UnreadNotifications($user_id,$type)
    {
        $data=array();
        $nots= DB::table('notifications')->select('*')
            ->where('read_status', 0)->where('user_id', (int)$user_id)
            ->where('type',$type)
            ->limit(10)
            ->get();
        if($nots)
            foreach ($nots as $not){
                $link='';
                if($not->not_type=='quotation')
                    $link='/quotations/'.$not->not_ref_id;
                elseif($not->not_type=='raw_material')
                    $link='/raw-materials/'.$not->not_ref_id;

                $data[]=array(
                    'id'=>$not->id,'ref_id'=>$not->not_ref_id,'title'=>$not->not_msg,'desc'=>$not->not_description,'date_added'=>$not->date_added,
                    'date_modified'=>$not->date_modified,'read_status'=>$not->read_status,'alink'=>$link
                );
            }

            return $data;
    }

    public static function addNotification($user_id,$not_type = '',$type='', $not_ref_id = 0, $not_msg = '', $not_description = '')
    {
        $nit = DB::table('notifications')->insertGetId(array('user_id'=>$user_id,'type'=>$type,'not_type' => $not_type, 'not_ref_id' => (int)$not_ref_id, 'not_msg' => $not_msg,
            'not_description' => $not_description, 'read_status' => 0, 'date_added' => now(), 'date_modified' => now()));
        return $nit;
    }
}
