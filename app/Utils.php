<?php

namespace App;

use App\models\Category;
use App\models\Country;
use Auth;
use Illuminate\Support\Str;
use Route;
use Session;
use DB;
use App\Permissions;

final class Utils
{

    static $val;
    /**
     * Generate a cdn asset path.
     *
     * @param string $path
     *
     * @return string
     */

    /*
      * get the current actvate parent menu;
    */
    public static function getParentRoute($routes = null)
    {
        $current_url = Route::currentRouteName();
        $urarr = explode(".", $current_url);
        $current_url = $urarr[0];
        $permissons = MenuMaster::where('role_name', $current_url)->get(['menu_parent']);
        if (count($permissons) > 0)
            $val = $permissons[0]['menu_parent'];
        else
            $val = 0;
        return $val;
    }

    /**
     * @param array|string $routes
     * @return bool
     */
    public static function checkRoute($routes)
    {
        $data = Route::currentRouteName();
        $urarr = explode(".", $data);
        $current_url = $urarr[0];
        if (is_string($routes)) {
            return $current_url == $routes;
        } elseif (is_array($routes)) {
            return in_array(Route::currentRouteName(), $routes);
        }
        return false;
    }

    /*
      * get current page crud permissions

    */

    public static function getSideMenuData($routes = null)
    {
        $user_role = Auth::user()->user_role;

        if ($user_role == 1) {
            $get_permissions = Permissions::user_permissions();
        } else {
            $get_permissions = Permissions::user_permissions($user_role);
        }
        return $get_permissions;
    }


    public static function crudPermissions()
    {

        $is_super_admin = (Auth::user()->user_role == 1) ? 1 : 0;//Auth::user()->is_super;
        if (!$is_super_admin) {
            // get current route
            $current_role = Auth::user()->user_role;
            // get current route name
            $data = Route::currentRouteName();
            $urarr = explode(".", $data);
            $current_url = $urarr[0];
            // get the menu id from the db
            $permissons_menu = MenuMaster::where('menu_path', $current_url)->get(['id']);
            $menu_active_id = $permissons_menu[0]->id;
            $menu_cruds = UserPermissionMap::where('role_id', $current_role)
                ->where('menu_id', $menu_active_id)->get(['permissions']);
            if (isset($menu_cruds[0]))
                if (strpos($menu_cruds[0]->permissions, ',') !== false) {
                    $current_role = explode(",", $menu_cruds[0]->permissions);
                } else {
                    $current_role = array();
                    $current_role[] = $menu_cruds[0]->permissions;
                }
            else {
                $current_role = array();
            }

        } else {
            $current_role = [1, 2, 3, 4];
        }

        return $current_role;
    }


    /**
     * @return array
     */
    public static function getLogosNumber()
    {
        return [1, 2, 3, 4, 5];
    }

    /**
     * @param int $logoNumber
     * @return string
     */
    public static function logoPath($logoNumber = 1)
    {
        $logoNumber = static::getValidLogoNumber($logoNumber);

        return "/images/profile/avatar_{$logoNumber}.png";
    }

    /**
     * @param int $logoNumber
     * @return string
     */
    public static function getValidLogoNumber($logoNumber = 1)
    {
        return (in_array($logoNumber, static::getLogosNumber())) ? $logoNumber : 1;
    }

    /**
     * @param null $guard
     * @return string
     */
    public static function getUserRoleLabel($guard = null)
    {
        if (!Auth::guard($guard)->guest()) {
            $user = Auth::guard($guard)->user();
            if ($user->is_admin) {
                return 'Administrator';
            } else {
                return 'Member';
            }
        }

        return 'Anonymous';
    }


    /*
        * get user admin route url from the application
    */

    public static function getUrlRoute($guard = null)
    {
        if (!Auth::guard($guard)->guest()) {

            return 'siteadmin';

        }
    }

    public static function getUserUrlRoute($guard = null)
    {
        if (!Auth::guard($guard)->guest()) {

            return 'user';

        }
    }

    public static function craeteUniqueSlugForTable($tablename, $column_name, $slug, $id_check = '', $id_value = 0)
    {
        $slug_new =Str::slug($slug);

        $query = DB::table($tablename)->whereRaw('(' . $column_name . '= \'' . $slug_new . '\' or slug LIKE \'' . $slug_new . '-%\')');
        if ($id_value)
            $query->where($id_check, '!=', $id_value);
        $nos = $query->count();
        if ($nos > 0)
            $slug_new .= '-' . ($nos + 1);

        return $slug_new;
    }

    public static function footerProducts()
    {
        return $query = DB::table('product')->where('status', 1)->orderBy('Product_id', 'DESC')->limit(5)->get();
    }
    public static function roundPrice($price)
    {
        if ((int)$price>=0)
            return sprintf('%0.2f', round($price, 2));
        return  '0.00';

    }
    public static function formatPrice($price)
    {
        return 'Rs '.self::roundPrice($price);

    }
}
