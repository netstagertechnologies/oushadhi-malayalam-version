<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CareersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Careers";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('careers')->orderBy('id', 'DESC')->paginate($per_page);
            return view('admin.catalog.careers.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Careers";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Careers";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.careers.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => "required",
            "last_date" => "required",
            "dept" => "required",
        ], [
            'title.required' => 'This field is required',
            'last_date.required' => 'please enter last date and time',
            'dept.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'status' => $request->status,
            'last_date' => $request->last_date,
            'dept' => $request->dept,
            'dept_ml' => $request->dept_ml,
            'contact' => $request->contact,
            'contact_email' => $request->contact_email,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $id = DB::table('careers')->insertGetId($insertValues);
        if (isset($request->career_file)) {
            foreach ($request->career_file as $tf) {

                $im = (isset($tf['name'])&&!empty($tf['name'])) ? $tf['name'] : '';
                if (!empty($im)) {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/careers';
                    $im->move($file_saving_path, $changed_fileName);

                    DB::table('career_files')->insert(array('career_id' => (int)$id, 'title' => $tf['title'],'title_ml' => $tf['title_ml'], 'file_link' => $changed_fileName));


                }
            }

        }
        if ($id)
            $request->session()->flash('success', 'Success: Careers Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/careers');
    }

    public function edit($id)
    {
        $data['page_title'] = "Careers";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Careers";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('careers')->where('id', $id)->first();
            $data['career_files'] = DB::table('career_files')->where('career_id', $id)->get();
            $data['edit_id'] = $id;
            return view('admin.catalog.careers.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "title" => "required",
            "last_date" => "required",
            "dept" => "required",
        ], [
            'title.required' => 'This field is required',
            'last_date.required' => 'please enter last date and time',
            'dept.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'dept_ml' => $request->dept_ml,
            'last_date' => $request->last_date,
            'dept' => $request->dept,
            'contact' => $request->contact,
            'contact_email' => $request->contact_email,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = DB::table('careers')->where('id', $id)->update($insertValues);
        DB::table('career_files')->where('career_id', $id)->delete();
        if (isset($request->career_file)) {
            foreach ($request->career_file as $tf) {

                $im = (isset($tf['name'])&&!empty($tf['name'])) ? $tf['name'] : '';
                if (!empty($im)) {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/careers';
                    $im->move($file_saving_path, $changed_fileName);
                    if (isset($tf['file_id']))
                        DB::table('career_files')->insert(array('file_id' => $tf['file_id'], 'career_id' => (int)$id, 'title' => $tf['title'],'title_ml' => $tf['title_ml'], 'file_link' => $changed_fileName));
                    else
                        DB::table('career_files')->insert(array('career_id' => (int)$id, 'title' => $tf['title'],'title_ml' => $tf['title_ml'], 'file_link' => $changed_fileName));


                } else {
                    if (isset($tf['file_id']))
                        DB::table('career_files')->insert(array('file_id' => $tf['file_id'], 'career_id' => (int)$id, 'title' => $tf['title'],'title_ml' => $tf['title_ml'], 'file_link' => $tf['file_link']));
                    else
                        DB::table('career_files')->insert(array('career_id' => (int)$id, 'title' => $tf['title'],'title_ml' => $tf['title_ml'], 'file_link' => ''));


                }
            }
        }
        if ($up_status)
            $request->session()->flash('success', 'Success: Careers Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/careers');
    }


    public  function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {


            $delete_status = DB::table('careers')->where('id', $id)->delete();
            $trr = DB::table('career_files')->where('career_id', (int)$id)->get();
            if (isset($trr))
                foreach ($trr as $trr_f) {
                    if (!empty($trr_f->file_link) && $trr_f->file_link != null)
                        if (file_exists(public_path('/uploads/careers/' . $trr_f->file_link)))
                            unlink(public_path('/uploads/careers/' . $trr_f->file_link));
                    DB::table('career_files')->where('file_id', (int)$trr_f->file_id)->delete();
                }
            if ($delete_status) {
                $request->session()->flash('success', 'Careers removed successfully');

            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public
    function show()
    {
    }
}
