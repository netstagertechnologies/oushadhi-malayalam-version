<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\models\Category;
use App\models\Product;
use App\Utils;
use DB;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Product";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = Product::orderBy('title', 'ASC');

            if ($request->category != "")
                $data['page_data'] = $data['page_data']->where('category_id', $request->category);

            if ($request->search != "")
                $data['page_data'] = $data['page_data']->where('title', 'LIKE', "%$request->search%");


            if ($request->featured != "") {
                if ($request->featured == 1)
                    $data['page_data'] = $data['page_data']->where('featured', 1);
                elseif ($request->featured == 0)
                    $data['page_data'] = $data['page_data']->where('featured', 0);
            }


            $data['page_data'] = $data['page_data']->paginate($per_page);
            $data['categories'] = Category::where('status', 1)->orderBy('title', 'ASC')->get();

            return view('admin.manage_product.product.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Product";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Product";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            $data['taxes'] = Product::getAllTaxes();
            $data['categories'] = Category::where('status', 1)->orderBy('title', 'ASC')->get();
            return view('admin.manage_product.product.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
            "meta_title" => "required",
            "category_id" => "required",
            "model" => "required",

        ], [
            'title.required' => 'This field is required',
            'meta_title.required' => 'This field is required',
            'category_id.required' => 'Please select a category',
            'model.required' => 'Product Code required',
        ]);
        $changed_fileName = '';
        $page_slug = Utils::craeteUniqueSlugForTable('product', 'slug', $request->title, 'product_id', 0);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/product';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'slug' => $page_slug,
            'description' => $request->description,
            'description_ml' => $request->description_ml,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'keywords' => $request->keywords,
            'status' => $request->status,
            'category_id' => $request->category_id,
            'image' => $changed_fileName,
            'model' => $request->model,
            'featured' => $request->featured,
            'show_home' => isset($request->show_home) ? 1 : 0,
            'total_ingradients' => $request->total_ingradients,
            'main_ingradients_ml' => $request->main_ingradients_ml,
            'indication_ml' => $request->indication_ml,
            'special_indication_ml' => $request->special_indication_ml,
            'dosage' => $request->dosage,
            'dosage_ml' => $request->dosage_ml,
            'p_usage_ml' => $request->p_usage_ml,
            'tax' => (int)$request->tax,
            'rating' => (int)$request->rating,
            'presentation' => $request->presentation,
            'views' => 0,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $product_id = Product::insertGetId($insertValues);

        if (isset($request->product_price)) {
            foreach ($request->product_price as $product_p) {
                DB::table('product_price')->insert(array('product_id' => (int)$product_id, 'unit_value' => strtolower($product_p['unit']), 'price' => (int)$product_p['price'], 'dealer_price' => (int)$product_p['dealer_price']));
            }
        }


        if (isset($request->additional_image)) {
            foreach ($request->additional_image as $tf) {
                $im = isset($tf['name']) ? $tf['name'] : '';
                if ($im != '') {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/product';
                    $im->move($file_saving_path, $changed_fileName);
                    DB::table('product_images')->insert(array('product_id' => (int)$product_id, 'image' => $changed_fileName, 'sort_order' => $tf['sort_order'], 'date_added' => now()));
                }
            }
        }

        if ($product_id)
            $request->session()->flash('success', 'Success: Product Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/product');
    }

    public function edit($id)
    {
        $data['page_title'] = "Product";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Product";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {

            $data['taxes'] = Product::getAllTaxes();
            $data['page_data'] = Product::where('product_id', $id)->first();
            $data['edit_id'] = $id;
            $data['product_prices'] = DB::table('product_price')->where('product_id', $id)->orderBy('price_id', 'ASC')->get();
            $data['additional_images'] = DB::table('product_images')->where('product_id', $id)->orderBy('sort_order', 'ASC')->get();
            $data['categories'] = Category::where('status', 1)->orderBy('title', 'ASC')->get();
            return view('admin.manage_product.product.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $product_id)
    {
        // dd($request);
        $request->validate([
            "title" => "required",
            "meta_title" => "required",
            "category_id" => "required",
            "model" => "required",

        ], [
            'title.required' => 'This field is required',
            'meta_title.required' => 'This field is required',
            'category_id.required' => 'Please select a category',
            'model.required' => 'Product Code required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('product', 'slug', $request->title, 'product_id', $product_id);

        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'slug' => $page_slug,
            'description' => $request->description,
            'description_ml' => $request->description_ml,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'keywords' => $request->keywords,
            'status' => $request->status,
            'category_id' => $request->category_id,
            'model' => $request->model,
            'featured' => $request->featured,
            'show_home' => isset($request->show_home) ? 1 : 0,
            'total_ingradients' => $request->total_ingradients,
            'main_ingradients' => $request->main_ingradients,
            'main_ingradients_ml' => $request->main_ingradients_ml,
            'indication' => $request->indication,
            'indication_ml' => $request->indication_ml,
            'special_indication' => $request->special_indication,
            'special_indication_ml' => $request->special_indication_ml,
            'p_usage' => $request->p_usage,
            'p_usage_ml' => $request->p_usage_ml,
            'rating' => (int)$request->rating,
            'tax' => (int)$request->tax,
            'dosage' => $request->dosage,
            'dosage_ml' => $request->dosage_ml,
            'presentation' => $request->presentation,
            'date_modified' => now(),
        );
        $up_status = Product::where('product_id', $product_id)->update($insertValues);

        DB::table('product_price')->where('product_id', (int)$product_id)->delete();
        if (isset($request->product_price)) {
            foreach ($request->product_price as $product_p) {
                if (isset($product_p['price_id']))
                    DB::table('product_price')->insert(array('price_id' => $product_p['price_id'], 'product_id' => (int)$product_id, 'unit_value' => strtolower($product_p['unit']), 'price' => (int)$product_p['price'], 'dealer_price' => (int)$product_p['dealer_price']));
                else
                    DB::table('product_price')->insert(array('product_id' => (int)$product_id, 'unit_value' => strtolower($product_p['unit']), 'price' => (int)$product_p['price'], 'dealer_price' => (int)$product_p['dealer_price']));

            }
        }

        // remove existed file
        $image = Product::where("product_id", $product_id)->get(['image']);
        $image_path = (isset($image[0]->image)) ? $image[0]->image : '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/product';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/product/' . $image_path)))
                    unlink(public_path('/uploads/product/' . $image_path));

            $up_status = Product::where('product_id', $product_id)->update(['image' => $changed_fileName]);
        }


        DB::table('product_images')->where('product_id', (int)$product_id)->delete();
        if (isset($request->additional_image)) {
            foreach ($request->additional_image as $tf) {
                $im = isset($tf['name']) ? $tf['name'] : '';
                if ($im != '') {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/product';
                    $im->move($file_saving_path, $changed_fileName);

                    if (isset($tf['additional_image_id']))
                        DB::table('product_images')->insert(array('additional_image_id' => $tf['additional_image_id'], 'product_id' => (int)$product_id, 'image' => $changed_fileName, 'sort_order' => (int)$tf['sort_order'], 'date_added' => now()));
                    else
                        DB::table('product_images')->insert(array('product_id' => (int)$product_id, 'image' => $changed_fileName, 'sort_order' => (int)$tf['sort_order'], 'date_added' => now()));
                } else {
                    if (isset($tf['additional_image_id']))
                        DB::table('product_images')->insert(array('additional_image_id' => $tf['additional_image_id'], 'product_id' => (int)$product_id, 'image' => $tf['image'], 'sort_order' => (int)$tf['sort_order'], 'date_added' => now()));
                    else
                        DB::table('product_images')->insert(array('product_id' => (int)$product_id, 'image' => $tf['image'], 'sort_order' => (int)$tf['sort_order'], 'date_added' => now()));

                }
            }
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Product Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/product');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = Product::where("product_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image)) ? $image[0]->image : '';


            $delete_status = Product::where('product_id', $id)->delete();
            DB::table('product_price')->where('product_id', (int)$id)->delete();
            DB::table('product_review')->where('product_id', (int)$id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Product removed successfully');
                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/product/' . $image_path)))
                        unlink(public_path('/uploads/product/' . $image_path));
            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }

    public function productViewsReport(Request $request)
    {
        $past_month_data = DB::table('product as p')
            ->where('p.status', 1);


        if ($request->search != "")
            $past_month_data = $past_month_data->where('p.title', 'LIKE', "%$request->search%");

        $past_month_data = $past_month_data->orderby('p.title', 'ASC')
            ->get();

        $data['products'] = $past_month_data;
        return view('admin.reports.product_views', $data);
    }
}
