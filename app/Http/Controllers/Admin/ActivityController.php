<?php

namespace App\Http\Controllers\Admin;

use App\models\Activity;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Activities";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = Activity::orderBy('activity_id', 'DESC')->paginate($per_page);
            return view('admin.catalog.activity.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Activity";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Activity";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.activity.create', $data);
        } else {
            return redirect('access-denied');
        }
    } 

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
            "description" => "required",
        ], [
            'title.required' => 'This field is required',
            'description.required' => 'This field is required',
        ]);
        $changed_fileName = '';
        $page_slug = Utils::craeteUniqueSlugForTable('activities', 'slug', $request->title, 'activity_id', 0);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/activity';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'slug' => $page_slug,
            'description' => $request->description,
            'description_ml' => $request->description_ml,
            'image' => $changed_fileName,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $activity_id = Activity::insertGetId($insertValues);


        if ($activity_id)
            $request->session()->flash('success', 'Success: Activity Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/activities');
    }

    public function edit($id)
    {
        $data['page_title'] = "Activity";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Activity";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = Activity::where('activity_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.activity.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $activity_id)
    {
        $this->validate($request, [
            "title" => "required",
            "description" => "required",
        ], [
            'title.required' => 'This field is required',
            'description.required' => 'This field is required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('activities', 'slug', $request->title, 'activity_id', $activity_id);

        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'description_ml' => $request->description_ml, 
            'description' => $request->description,
            'slug' => $page_slug,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = Activity::where('activity_id', $activity_id)->update($insertValues);
        // remove existed file
        $image = Activity::where("activity_id", $activity_id)->get(['image']);
        $image_path = (isset($image[0]->image))?$image[0]->image:'';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/activity';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/activity/' . $image_path)))
                    unlink(public_path('/uploads/activity/' . $image_path));

            $up_status = Activity::where('activity_id', $activity_id)->update(['image'=>$changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Activity Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/activities');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = Activity::where("activity_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image))?$image[0]->image:'';

            $delete_status = Activity::where('activity_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Activity removed successfully');

                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/activity/' . $image_path)))
                        unlink(public_path('/uploads/activity/' . $image_path));
            }else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
