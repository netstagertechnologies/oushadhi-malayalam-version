<?php

namespace App\Http\Controllers\Admin;

use App\models\Settings;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "Settings";
        $data['page_subtitle'] = "Settings";
        $data['page_subtitle_desc'] = "";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['crud_permissions'] = Utils::crudPermissions();
            $data['settings_data'] = Settings::getSettings();
            return view('admin.system.settings', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        Settings::editSettings($request->except('_token'));
        return redirect(Utils::getUrlRoute() . '/settings');
    }

}
