<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DepartmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Departments";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('departments')->orderBy('id', 'DESC')->paginate($per_page);
            return view('admin.catalog.departments.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Departments";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Department";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.departments.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'page_content_ml' => $request->page_content_ml,
            'page_content' => $request->page_content,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $id = DB::table('departments')->insertGetId($insertValues);


        if ($id)
            $request->session()->flash('success', 'Success: departments Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/departments');
    }

    public function edit($id)
    {
        $data['page_title'] = "departments";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify department";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('departments')->where('id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.departments.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'page_content' => $request->page_content,
            'title_ml' => $request->title_ml,
            'page_content_ml' => $request->page_content_ml,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = DB::table('departments')->where('id', $id)->update($insertValues);


        if ($up_status)
            $request->session()->flash('success', 'Success: department Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/departments');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {
            $delete_status = DB::table('departments')->where('id', $id)->delete();

            if ($delete_status) {
                $request->session()->flash('success', 'departments removed successfully');

            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
