<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\models\UserRole;
use Auth;
use App\MenuMaster;
use App\Permissions;
use App\UserPermissionMap;
use App\Utils;

class UserRoleController extends Controller
{
    public function __construct()

    {

        $this->middleware('auth:admin');

    }
    public function index(Request $request)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(2, $crud_permissions)) {

            $role_data = UserRole::where('creator', Auth::user()->id)
                ->paginate(20);

            $page_title = 'User';
            $page_subtitle = 'User role(Permissions)';
            return view('admin.user_manage.role', compact('page_title', 'page_subtitle', 'role_data','crud_permissions'));
        } else {
            return redirect('access-denied');
        }
    }

    /*
       * create user role
   */

    public function create()
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(1, $crud_permissions)) {

            $page_title = 'User';
            $page_subtitle = 'User role(Permissions)';
            $page_description = 'Add role(Permissions)';
            $get_menu_list = '';
            return view('admin.user_manage.create_role', compact('page_title', 'page_subtitle', 'page_description', 'get_menu_list'));
        } else {
            return redirect('access-denied');
        }
    }

    // insert new role to database

    public function store(Request $request)
    {
        $this->validate($request, [
            "role_name" => "required|unique:user_roles",
        ], [
            "role_name.required" => "The role name field is required!",
        ]);
        $roleStatus = UserRole::create([
            'role_name' => $request->role_name,
            'creator' => Auth::user()->id,
            'is_reserved' => 0,

        ]);
        if ($roleStatus)
            $request->session()->flash('success', 'User role inserted successfully');
        else
            $request->session()->flash('success', 'User role creation failed');

        return redirect('admin/user-role');
    }

    // edit view role

    public function edit($id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(3, $crud_permissions)) {

            $user_role = UserRole::where("id", $id)->get();
            $logg_role = Auth::user()->user_role;
            $is_admin = Auth::user()->is_admin;
            if ($is_admin) {
                $get_permissions = Permissions::user_permissions();
            } else {
                $get_permissions = Permissions::user_permissions($logg_role);
            }
            $page_title = 'User';
            $page_subtitle = 'permission role';
            $page_description = 'Edit User Role(Permission)';
            $get_menu_list = $this->menu_category_list($id);
            return view('admin.user_manage.edit_role', compact('page_title', 'page_subtitle', 'page_description', 'user_role', 'get_menu_list'));
        } else {
            return redirect('access-denied');
        }
    }


    // update role details
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "role_name" => "required",
        ], [
            "role_name.required" => "The role name field is required!",
        ]);
        UserRole::find($id)->update(['role_name' => $request->role_name]);
        $menu_list = $request->menu_list;
        $user_role_map_count = UserPermissionMap::where('role_id', $id)->count();
        if ($user_role_map_count > 0) {
            $clear_field = UserPermissionMap::where('role_id', $id)->delete();
        }
        if (!empty($menu_list)) {
            if (sizeof($menu_list) > 0) {
                foreach ($menu_list as $row => $value) {

                    $roleMapDataInsert = UserPermissionMap::create([
                        'role_id' => $id,
                        'menu_id' => $row,
                        'permissions' => implode(",", $value)
                    ]);
                }
            }
        }
        $request->session()->flash('success', 'User role updated successfully');
        return redirect('admin/user-role');
    }

    // remove role details

    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $user = UserRole::find($id);
            $user->delete();
            $user1 = UserPermissionMap::where('role_id', $id)->delete();
            $request->session()->flash('success', 'User role removed successfully');
            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function menu_category_list($id)
    {
        // check is admin or not
        $is_admin = 1;//Auth::user()->is_super;
        // get menu list from the permitted roles
        $html = '<div class="col-md-4"></div><div class="col-md-6"><div class="list-group">';
        if ($is_admin) {
            // complete menu from the database table
            $permissons = MenuMaster::where('status', 1)
                ->where('menu_parent', '!=', 0)
                ->get();
        } else {

            // get member permission cat
            $user_role = Auth::user()->user_role;
            // select the allow menu and permissions from the given role
            $role_menu = UserPermissionMap::where('role_id', $user_role)->get(['menu_id']);
            foreach ($role_menu as $row) {
                $role_menu_array[] = $row->menu_id;
            }
            // select the menu from the master with role permissions
            $permissons = MenuMaster::where('status', 1)
                ->where('menu_parent', '!=', 0)
                ->where('admin_access', '!=', 1)
                ->whereIn('id', $role_menu_array)
                ->get();

        }
        $role_menu_list = UserPermissionMap::where('role_id', $id)->get();
        //echo json_encode($role_menu_list);exit();
        foreach ($role_menu_list as $row) {
            $role_menu_array_id[] = $row->menu_id;
        }
        // get crud permision array
        $crud = ['Create' => 1, 'Read' => 2, 'Update' => 3, 'Delete' => 4];

        foreach ($permissons as $row) {
            $menu_id = $row->id;
            // genarate crud permissions
            $per = '<p>';
            foreach ($crud as $key => $value) {
                // get crud permission of the loop menu
                $this_menu_cruds = $this->get_permission_from_map($id, $menu_id);
                // check given crud is already set or not
                // validate check box
                if (in_array($value, $this_menu_cruds)) {
                    $per .= '<input type="checkbox" checked name="menu_list[' . $menu_id . '][]"  value="' . $value . '" id="' . $value . '" >&nbsp<label>' . $key . '</label>';
                } else {
                    $per .= '<input type="checkbox"  name="menu_list[' . $menu_id . '][]"  value="' . $value . '" id="' . $value . '" >&nbsp<label>' . $key . '</label>';
                }
            }
            $per .= '</p>';

            //
            if (!empty($role_menu_array_id)) {
                if (in_array($row->id, $role_menu_array_id)) {

                    $html .= '<li class="list-group-item">
                <div class="row"><div class="col-md-5"><h5 class="mb-1"><b>' . $row->menu_name . '</b></h5></div>
                <div class="col-md-7"><h5 class="mb-1"><b>' . $per . '</b></h5></div>
                </div></li>';

                } else {
                    $html .= '<li class="list-group-item">
              <div class="row"><div class="col-md-5"><h5 class="mb-1"><b>' . $row->menu_name . '</b></h5></div>
              <div class="col-md-7"><h5 class="mb-1"><b>' . $per . '</b></h5></div>
              </div></li>';
                }
            } else {
                $html .= '<li class="list-group-item">
            <div class="row"><div class="col-md-5"><h5 class="mb-1"><b>' . $row->menu_name . '</b></h5></div>
            <div class="col-md-7"><h5 class="mb-1"><b>' . $per . '</b></h5></div>
            </div></li>';
            }
        }
        $html .= '</div></div>';
        return $html;

    }

    function get_permission_from_map($role_id, $menu_id)
    {
        $role_menu_list = UserPermissionMap::where('role_id', $role_id)->where('menu_id', $menu_id)->get(['permissions']);
        if (count($role_menu_list) > 0)
            return $prmission_list = explode(",", $role_menu_list[0]->permissions);
        else
            return [];
    }


}
