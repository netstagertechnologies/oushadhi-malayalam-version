<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Common;
use App\models\Attribute;
use DB;
use App\Utils;
use Illuminate\Support\Facades\Route;

class HomePageController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "CMS";
        $data['page_subtitle'] = "Page Management";
        $data['page_subtitle_desc'] = "";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['cms'] = DB::table('home_page_cms')->get();
            return view('admin.cms.index-page.home_page', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            "title1" => "required",
            "description1" => "required",
            "link1" => "required",
            "title2" => "required",
            "description2" => "required",
            "link2" => "required",
        ], [
            'title1.required' => 'This field is required',
            'description1.required' => 'This field is required',
            'link1.required' => 'This field is required',
            'title2.required' => 'This field is required',
            'description2.required' => 'This field is required',
            'link2.required' => 'This field is required',
        ]);

        $update = DB::table('home_page_cms')
            ->where('h_section', 1)
            ->update(['h_caption' => $request->caption1,
                'h_caption_ml' => $request->caption1_ml,
                'h_title' => $request->title1,
                'h_title_ml' => $request->title1_ml,
                'h_description' => $request->description1,
                'h_description_ml' => $request->description1_ml,
                'h_link' => $request->link1]);
        $update = DB::table('home_page_cms')
            ->where('h_section', 2)
            ->update([
                'h_title' => $request->title2,
                'h_description' => $request->description2,
                'h_link' => $request->link2,]);

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/home-cms';
            $file->move($file_saving_path, $changed_fileName);
            $update = DB::table('home_page_cms')->where('h_section', 2)->update(['h_image' => $changed_fileName]);
        }

        $msg = "CMS Updated";
        $request->session()->flash('success', $msg);

        return redirect(Utils::getUrlRoute() . '/home-cms');
    }

}
