<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Mails\AccountRejected;
use App\Mails\AccountVerified;
use App\User;
use App\Utils;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;

class AdminController extends Controller

{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $data['pending_count'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 1)->count();
        $data['review_count'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 2)->count();
        $data['processing_count'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 3)->count();
        $data['delivered_count'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 4)->count();
        $data['cancelled_count'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 5)->count();
        //$data['pending_count']= DB::table('quotations')->where('status', 1)->where('quotation_status',1)->count();
        $data['latest_quotations'] = DB::table('quotations as q')
            ->join('users as u', 'u.id', '=', 'q.user_id')
            ->where('q.status', 1)->orderBy('quotation_id', 'DESC')->limit(8)->get();

        $data['latest_products'] = DB::table('product as p')
            ->join('category as c', 'c.category_id', '=', 'p.category_id')
            ->select('p.product_id', 'c.title as category', 'p.title as title', 'p.image as image')
            ->where('p.status', 1)
            ->orderBy('p.product_id', 'DESC')->limit(5)->get();

        $data['latest_dealers'] = User::where('status', 1)->limit(8)->get();

        $past_month_data = DB::table('quotations as q')
            ->join('quotation_details as qd', 'qd.quotation_id', '=', 'q.quotation_id')
            ->select(DB::raw('count(DISTINCT qd.quotation_id) as quotation_count,sum(qd.qty) as total_qty,count(DISTINCT qd.product_id) as product_count'))
            ->where('q.status', 1)->where('qd.is_deleted', 0)->whereRaw('q.date_added > DATE_SUB(now(), INTERVAL 1 MONTH)')
            ->orderby('qd.date_added', 'DESC')
            ->first();
        if (isset($past_month_data)) {
            $data['past_quot_count'] = $past_month_data->quotation_count;
            $data['past_qty_count'] = $past_month_data->total_qty;
            $data['past_pdt_count'] = $past_month_data->product_count;
        }

        $data['pending_count_month'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 1)->whereRaw('date_added > DATE_SUB(now(), INTERVAL 1 MONTH)')->count();
        $data['review_count_month'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 2)->whereRaw('date_added > DATE_SUB(now(), INTERVAL 1 MONTH)')->count();
        $data['processing_count_month'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 3)->whereRaw('date_added > DATE_SUB(now(), INTERVAL 1 MONTH)')->count();
        $data['delivered_count_month'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 4)->whereRaw('date_added > DATE_SUB(now(), INTERVAL 1 MONTH)')->count();
        $data['cancelled_count_month'] = DB::table('quotations')->where('status', 1)->where('quotation_status', 5)->whereRaw('date_added > DATE_SUB(now(), INTERVAL 1 MONTH)')->count();

        return view('admin.home', $data);

    }

    public function adminProfile()
    {
        $users = Admin::where("id", (int)Auth::user()->id)->get();
        $page_description = 'Update Profile';
        return view('admin.my-profile', compact('page_title', 'page_subtitle', 'page_description', 'users'));

    }

    public function updateUser(Request $request)
    {
        $this->validate($request, [
            "name" => "required|min:3|max:255",
            "password" => "nullable|confirmed|min:6",
        ]);
        $updateValues = array(
            'name' => $request->name,
            'date_modified' => now()
        );
        $password = $request->input('password', null);
        if (!empty($password)) {
            $updateValues['password'] = Hash::make($password);
        }


        $up_status = Admin::find((int)Auth::user()->id)->update($updateValues);
        if ($up_status)
            $request->session()->flash('success', 'Success: Profile Updated');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/my-profile');
    }

    public function frontendUsers(Request $request)
    {
        $data['page_title'] = "Doctors and Dealers";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = User::where('is_admin_verified','!=',2)->orderBy('id', 'DESC');
            if ($request->search != "")
                $data['page_data'] = $data['page_data']->where('name', 'LIKE', "%$request->search%");

            $data['page_data'] = $data['page_data']->paginate(20);
            return view('admin.user_manage.feusers', $data);
        }

    }

    public function frontendUsersMakeVerified(Request $request, $id)
    {

        if (!isset($id)) {
            $request->session()->flash('error', 'Invalid user');
            return Redirect()->back();
        }
        if ($id) {
            $up_status = User::where('id', (int)$id)->update(['is_admin_verified' => 1]);
            if ($up_status) {
                $request->session()->flash('success', 'Success: User verified');
                $user_details = User::where('id', (int)$id)->first();
                if (isset($user_details->email) && !empty($user_details->email))
                    Mail::to(strtolower($user_details->email))->send(new AccountVerified(array('name' => $user_details->name)));
            } else
                $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            $request->session()->flash('error', 'Invalid user');
            return Redirect()->back();
        }
    }

    public function frontendUsersMakeRejected(Request $request, $id)
    {

        if (!isset($id)) {
            $request->session()->flash('error', 'Invalid user');
            return Redirect()->back();
        }
        if ($id) {
            $up_status = User::where('id', (int)$id)->update(['is_admin_verified' => 2]);
            if ($up_status) {
                $request->session()->flash('success', 'Success: User Rejected');
                $user_details = User::where('id', (int)$id)->first();
                if (isset($user_details->email) && !empty($user_details->email))
                    Mail::to(strtolower($user_details->email))->send(new AccountRejected(array('name' => $user_details->name)));

            } else
                $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            $request->session()->flash('error', 'Invalid user');
            return Redirect()->back();
        }
    }

}
