<?php

namespace App\Http\Controllers\Admin;

use App\models\Blog;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Blog";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = Blog::orderBy('blog_id', 'DESC')->paginate($per_page);
            return view('admin.catalog.blog.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Blog";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Blog";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.blog.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
            "short_description"=>"required",
            "description" => "required",
        ], [
            'title.required' => 'This field is required',
            "short_description.required"=>"This field is required",
            'description.required' => 'This field is required',
        ]);
        $changed_fileName = '';
        $page_slug = Utils::craeteUniqueSlugForTable('blog', 'slug', $request->title, 'blog_id', 0);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/blog';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'slug' => $page_slug,
            'short_description_ml' => $request->short_description_ml,
            'short_description' => $request->short_description,
            'description_ml' => $request->description_ml,
            'description' => $request->description,
            'image' => $changed_fileName,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $blog_id = Blog::insertGetId($insertValues);
        if ($blog_id)
            $request->session()->flash('success', 'Success: Blog Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/blog');
    }

    public function edit($id)
    {
        $data['page_title'] = "Blog";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Blog";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = Blog::where('blog_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.blog.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $blog_id)
    {
        $this->validate($request, [
            "title" => "required",
            "short_description"=>"required",
            "description" => "required",
        ], [
            'title.required' => 'This field is required',
            "short_description.required"=>"This field is required",
            'description.required' => 'This field is required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('blog', 'slug', $request->title, 'blog_id', $blog_id);

        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'short_description_ml' => $request->short_description_ml,
            'description_ml' => $request->description_ml,
            'short_description' => $request->short_description,
            'description' => $request->description,
            'slug' => $page_slug,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = Blog::where('blog_id', $blog_id)->update($insertValues);
        // remove existed file
        $image = Blog::where("blog_id", $blog_id)->get(['image']);
        $image_path = (isset($image[0]->image))?$image[0]->image:'';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/blog';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/blog/' . $image_path)))
                    unlink(public_path('/uploads/blog/' . $image_path));

            $up_status = Blog::where('blog_id', $blog_id)->update(['image'=>$changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Blog Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/blog');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = Blog::where("blog_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image))?$image[0]->image:'';


            $delete_status = Blog::where('blog_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Blog removed successfully');
                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/blog/' . $image_path)))
                        unlink(public_path('/uploads/blog/' . $image_path));
            }else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
