<?php

namespace App\Http\Controllers\admin;

use App\models\Attribute;
use App\models\Category;
use App\models\Options;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Common;
use DB;
use App\Utils;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Validator;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $data['page_title'] = "Notifications";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";

        $data['crud_permissions'] = Utils::crudPermissions();
        $data['notifications'] = array();
        $data['notifications'] = DB::table('notifications')
            ->select('*')
            ->where('user_id', (int)Auth::user()->id)
            ->where('type', 'admin')
            ->orderby('date_added', 'DESC')
            ->paginate(20);

        return view('admin.notifications', $data);

    }

    public function show($not_id = 0)
    {
        $nots = DB::table('notifications')->select('*')
            ->where('id', (int)$not_id)
            ->first();
        if ($nots) {
            $link = url(\App\Utils::getUrlRoute());
            if ($nots->not_type == 'quotation')
                $link = url(\App\Utils::getUrlRoute() . '/quotations/' . $nots->not_ref_id);
            elseif($nots->not_type=='raw_material')
                $link=url(\App\Utils::getUrlRoute() .'/raw-materials/'.$nots->not_ref_id);

            DB::table('notifications')
                ->where('id', (int)$not_id)->update(array('read_status' => 1));
            return redirect($link);
        } else
            return redirect(Utils::getUrlRoute() . '/quotations');
    }
}
