<?php

namespace App\Http\Controllers\Admin;

use App\models\Contact;
use App\models\Contacts;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Contacts";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = Contacts::orderBy('contact_id', 'DESC')->paginate($per_page);
            return view('admin.system.contacts.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Contacts";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Contact";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.system.contacts.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "name" => "required",
            "designation" => "required",
            "contact_type"=>"required",
        ], [
            'name.required' => 'This field is required',
            'designation.required' => 'This field is required',
            "contact_type.required"=> 'This field is required',
        ]);
        $changed_fileName = '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/contacts';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'name' => $request->name,
            'name_ml' => $request->name_ml,
            'designation' => $request->designation,
            'designation_ml' => $request->designation_ml,
            'mob_no' => $request->mob_no,
            'res_no' => $request->res_no,
            'email' => $request->email,
            'image' => $changed_fileName,
            'status' => $request->status,
            'sort_order'=>(int)$request->sort_order,
            'contact_type'=>$request->contact_type,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $contact_id = Contacts::insertGetId($insertValues);


        if ($contact_id)
            $request->session()->flash('success', 'Success: Contact Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/contacts');
    }

    public function edit($id)
    {
        $data['page_title'] = "Contact";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Contact";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = Contacts::where('contact_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.system.contacts.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $contact_id)
    {
        $request->validate([
            "name" => "required",
            "designation" => "required",
            "contact_type"=>"required",
        ], [
            'name.required' => 'This field is required',
            'designation.required' => 'This field is required',
            "contact_type.required"=> 'This field is required',
        ]);

        $insertValues = array(
            'name' => $request->name,
            'designation' => $request->designation,
            'name_ml' => $request->name_ml,
            'designation_ml' => $request->designation_ml,
            'mob_no' => $request->mob_no,
            'res_no' => $request->res_no,
            'email' => $request->email,
            'contact_type'=>$request->contact_type,
            'sort_order'=>(int)$request->sort_order,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = Contacts::where('contact_id', $contact_id)->update($insertValues);
        // remove existed file
        $image = Contacts::where("contact_id", $contact_id)->get(['image']);
        $image_path = (isset($image[0]->image)) ? $image[0]->image : '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/contacts';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/contacts/' . $image_path)))
                    unlink(public_path('/uploads/contacts/' . $image_path));

            $up_status = Contacts::where('contact_id', $contact_id)->update(['image' => $changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Contact Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/contacts');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = Contacts::where("contact_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image))?$image[0]->image:'';


            $delete_status = Contacts::where('contact_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Contact removed successfully');
                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/contacts/' . $image_path)))
                        unlink(public_path('/uploads/contacts/' . $image_path));
            }else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
