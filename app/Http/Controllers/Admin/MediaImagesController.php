<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MediaImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Media Images";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('media')->orderBy('media_id', 'DESC')->paginate($per_page);
            return view('admin.media.images.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Media Images";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Media Images";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.media.images.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',

        ]);
        $changed_fileName = '';
        $page_slug = Utils::craeteUniqueSlugForTable('media', 'slug', $request->title, '', 0);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/media';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'slug' => $page_slug,
            'cover' => $changed_fileName,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $media_id = DB::table('media')->insertGetId($insertValues);

        foreach ($request->file('uploadFile') as $key => $value) {

            $imageName = time() . $key . '.' . $value->getClientOriginalExtension();

            $value->move(public_path() . '/uploads/media', $imageName);
            DB::table('media_images')->insert(array('media_id' => (int)$media_id, 'image' => $imageName, 'sort_order' => 0));
        }


        if ($media_id)
            $request->session()->flash('success', 'Success: Media Images Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/media-images');
    }

    public function edit($id)
    {
        $data['page_title'] = "Media Images";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Media Images";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('media')->where('media_id', $id)->first();
            $data['media_images'] = array();
            $data['edit_id'] = $id;
            if ($data['page_data']) {

                $data['media_images'] = DB::table('media_images')->where('media_id', (int)$id)->get();
            }
            return view('admin.media.images.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $media_id)
    {

        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',

        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('media', 'slug', $request->title, '', 0);


        $insertValues = array(
            'title' => $request->title,
            'title_ml' => $request->title_ml,
            'slug' => $page_slug,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = DB::table('media')->where('media_id', $media_id)->update($insertValues);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/media';
            $file->move($file_saving_path, $changed_fileName);
            DB::table('media')->where('media_id', $media_id)->update(array('cover' => $changed_fileName));
        }

        DB::table('media_images')->where('media_id', (int)$media_id)->delete();
        if (isset($request->media_images)) {
            foreach ($request->media_images as $mimg) {
                DB::table('media_images')->insert(array('media_id' => (int)$media_id, 'image' => $mimg['image'], 'sort_order' => 0));
            }

        }
        if ($request->hasFile('uploadFile'))
            foreach ($request->file('uploadFile') as $key => $value) {

                $imageName = time() . $key . '.' . $value->getClientOriginalExtension();

                $value->move(public_path() . '/uploads/media', $imageName);
                DB::table('media_images')->insert(array('media_id' => (int)$media_id, 'image' => $imageName, 'sort_order' => 0));
            }


        if ($up_status)
            $request->session()->flash('success', 'Success: banner Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/media-images');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {


            $delete_status = DB::table('media')->where('media_id', $id)->delete();
            $trr = DB::table('media_images')->where('media_id', (int)$id)->get();
            if (isset($trr))
                foreach ($trr as $trr_f) {
                    if (!empty($trr_f->image) && $trr_f->image != null)
                        if (file_exists(public_path('/uploads/media/' . $trr_f->image)))
                            unlink(public_path('/uploads/media/' . $trr_f->image));
                }

            DB::table('media_images')->where('media_id', (int)$id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'banner removed successfully');
            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
