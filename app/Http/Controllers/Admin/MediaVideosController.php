<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MediaVideosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Videos";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('media_videos')->orderBy('media_video_id', 'DESC')->paginate($per_page);
            return view('admin.media.videos.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Videos";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Videos";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.media.videos.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "link" => "required",
        ], [
            'link.required' => 'This field is required',

        ]);
        $insertValues = array(
            'link' => $request->link,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $media_video_id = DB::table('media_videos')->insertGetId($insertValues);


        if ($media_video_id)
            $request->session()->flash('success', 'Success: Videos Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/media-videos');
    }

    public function edit($id)
    {
        $data['page_title'] = "Video";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Video";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('media_videos')->where('media_video_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.media.videos.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $media_video_id)
    {
        $request->validate([
            "link" => "required",
        ], [
            'link.required' => 'This field is required',

        ]);
        $insertValues = array(
            'link' => $request->link,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = DB::table('media_videos')->where('media_video_id', $media_video_id)->update($insertValues);

        if ($up_status)
            $request->session()->flash('success', 'Success: Video Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/media-videos');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $delete_status = DB::table('media_videos')->where('media_video_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Video removed successfully');
            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
