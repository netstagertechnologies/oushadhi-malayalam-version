<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\models\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\MenuMaster;
use App\Permissions;
use App\UserPermissionMap;
use App\Utils;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    public function __construct()

    {

        $this->middleware('auth:admin');

    }



    public function index(Request $request)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(2, $crud_permissions)) {

            $type_not = [1];
            $users = Admin::where('is_super', 0);
            if ($request->search != "")
                $users = $users->where('name', 'LIKE', "%$request->search%");

            $users = $users->paginate(20);
            $roles = UserRole::where('creator', '!=', 0)->get();
            $page_title = 'User';
            $page_subtitle = 'User Management';
            return view('admin.user_manage.users', compact('page_title', 'page_subtitle', 'users', 'crud_permissions', 'roles'));
        } else {
            return redirect('access-denied');
        }
    }

    /*
       * create user role
   */

    public function create()
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(1, $crud_permissions)) {

            $page_title = 'User';
            $page_subtitle = 'User Management';
            $page_description = 'Add User';
            $roles = UserRole::where('creator', '!=', 0)->get();

            return view('admin.user_manage.create', compact('page_title', 'page_subtitle', 'page_description', 'roles'));
        } else {
            return redirect('access-denied');
        }
    }

    // insert new role to database

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|min:3|max:255",
            "email" => "required|email|max:255|unique:users",
            "role" => "required",
            "password" => "required|nullable|confirmed|min:6",

        ]);

        $userDataInsert = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'is_super' => 0,
            'user_role' => $request->role,
            'date_added' => now(),
            'date_modified' => now()
        ])->id;

        if ($userDataInsert)
            $request->session()->flash('success', 'Success: User created');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/users');
    }

    // edit view role

    public function edit($id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(3, $crud_permissions)) {

            $users = Admin::where("id", $id)->get();
            $logg_role = Auth::user()->user_role;
            $is_admin = Auth::user()->is_admin;
            if ($is_admin) {
                $get_permissions = Permissions::user_permissions();
            } else {
                $get_permissions = Permissions::user_permissions($logg_role);
            }
            $roles = UserRole::where('creator', '!=', 0)->get();

            $page_title = 'User';
            $page_subtitle = 'User Manage';
            $page_description = 'Edit User';
            return view('admin.user_manage.edit', compact('page_title', 'page_subtitle', 'page_description', 'users', 'roles'));
        } else {
            return redirect('access-denied');
        }
    }


    // update role details
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required|min:3|max:255",
            "email" => "required|email|max:255|unique:users,email," . $id,
            "role" => "required",
            "password" => "nullable|confirmed|min:6",
        ]);
        $updateValues = array(
            'name' => $request->name,
            'email' => $request->email,
            'user_role' => $request->role,
            'date_modified' => now()
        );
        $password = $request->input('password', null);
        if (!empty($password)) {
            $updateValues['password'] = Hash::make($password);
        }


        $up_status=Admin::find($id)->update($updateValues);
        if ($up_status)
            $request->session()->flash('success', 'Success: User Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/users');
    }

    // remove role details

    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $user = Admin::find($id);
            $user->delete();
            $user1 = UserPermissionMap::where('role_id', $id)->delete();
            $request->session()->flash('success', 'User role removed successfully');
            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function menu_category_list($id)
    {
        // check is admin or not
        $is_admin = Auth::user()->is_admin;
        // get menu list from the permitted roles
        $html = '<div class="col-md-4"></div><div class="col-md-6"><div class="list-group">';
        if ($is_admin) {
            // complete menu from the database table
            $permissons = MenuMaster::where('status', 1)
                ->where('menu_parent', '!=', 0)
                ->get();
        } else {

            // get member permission cat
            $user_role = Auth::user()->user_role;
            // select the allow menu and permissions from the given role
            $role_menu = UserPermissionMap::where('role_id', $user_role)->get(['menu_id']);
            foreach ($role_menu as $row) {
                $role_menu_array[] = $row->menu_id;
            }
            // select the menu from the master with role permissions
            $permissons = MenuMaster::where('status', 1)
                ->where('menu_parent', '!=', 0)
                ->where('admin_access', '!=', 1)
                ->whereIn('id', $role_menu_array)
                ->get();

        }
        $role_menu_list = UserPermissionMap::where('role_id', $id)->get();
        //echo json_encode($role_menu_list);exit();
        foreach ($role_menu_list as $row) {
            $role_menu_array_id[] = $row->menu_id;
        }
        // get crud permision array
        $crud = ['Create' => 1, 'Read' => 2, 'Update' => 3, 'Delete' => 4];

        foreach ($permissons as $row) {
            $menu_id = $row->id;
            // genarate crud permissions
            $per = '<p>';
            foreach ($crud as $key => $value) {
                // get crud permission of the loop menu
                $this_menu_cruds = $this->get_permission_from_map($id, $menu_id);
                // check given crud is already set or not
                // validate check box
                if (in_array($value, $this_menu_cruds)) {
                    $per .= '<input type="checkbox" checked name="menu_list[' . $menu_id . '][]"  value="' . $value . '" id="' . $value . '" >&nbsp<label>' . $key . '</label>';
                } else {
                    $per .= '<input type="checkbox"  name="menu_list[' . $menu_id . '][]"  value="' . $value . '" id="' . $value . '" >&nbsp<label>' . $key . '</label>';
                }
            }
            $per .= '</p>';

            //
            if (!empty($role_menu_array_id)) {
                if (in_array($row->id, $role_menu_array_id)) {

                    $html .= '<li class="list-group-item">
                <div class="row"><div class="col-md-5"><h5 class="mb-1"><b>' . $row->menu_name . '</b></h5></div>
                <div class="col-md-7"><h5 class="mb-1"><b>' . $per . '</b></h5></div>
                </div></li>';

                } else {
                    $html .= '<li class="list-group-item">
              <div class="row"><div class="col-md-5"><h5 class="mb-1"><b>' . $row->menu_name . '</b></h5></div>
              <div class="col-md-7"><h5 class="mb-1"><b>' . $per . '</b></h5></div>
              </div></li>';
                }
            } else {
                $html .= '<li class="list-group-item">
            <div class="row"><div class="col-md-5"><h5 class="mb-1"><b>' . $row->menu_name . '</b></h5></div>
            <div class="col-md-7"><h5 class="mb-1"><b>' . $per . '</b></h5></div>
            </div></li>';
            }
        }
        $html .= '</div></div>';
        return $html;

    }

    function get_permission_from_map($role_id, $menu_id)
    {
        $role_menu_list = UserPermissionMap::where('role_id', $role_id)->where('menu_id', $menu_id)->get(['permissions']);
        if (count($role_menu_list) > 0)
            return $prmission_list = explode(",", $role_menu_list[0]->permissions);
        else
            return [];
    }


    public function changeUserStatus(Request $request)
    {
        $user_id = $request->user;
        $result_data = DB::table('users')->where('id', $user_id)->get(['status']);
        $status = $result_data[0]->verified;
        if ($status == 1) {
            $updateValues['status'] = 0;
        } else if ($status == 0) {
            $updateValues['status'] = 1;
        }
        DB::table('users')
            ->where('id', $user_id)
            ->update($updateValues);
        $result = 1;
        echo json_encode($result);
    }

}
