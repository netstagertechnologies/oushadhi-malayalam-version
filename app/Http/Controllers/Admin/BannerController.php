<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Banner";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Banner";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('banner')->where('status', 1)->first();
            $data['banner_images'] = array();
            $data['edit_id'] = 0;
            if ($data['page_data']) {
                $data['edit_id'] = $data['page_data']->banner_id;
                $data['banner_images'] = DB::table('banner_images')->where('banner_id', $data['page_data']->banner_id)->get();
            }
            return view('admin.cms.banner.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $banner_id)
    {

        $insertValues = array(
            'title' => $request->title,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = DB::table('banner')->where('banner_id', $banner_id)->update($insertValues);


        DB::table('banner_images')->where('banner_id', (int)$banner_id)->delete();

        if (isset($request->banner_image)) {
            foreach ($request->banner_image as $tf) {
                $im = isset($tf['name']) ? $tf['name'] : '';
                if ($im != '') {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/banner';
                    $im->move($file_saving_path, $changed_fileName);
                    if (isset($tf['banner_image_id']))
                        DB::table('banner_images')->insert(array('banner_image_id' => $tf['banner_image_id'], 'banner_id' => (int)$banner_id, 'title' => $tf['title'], 'image' => $changed_fileName, 'link' => $tf['link'], 'sort_order' => (int)$tf['sort_order']));
                    else
                        DB::table('banner_images')->insert(array('banner_id' => (int)$banner_id, 'title' => $tf['title'], 'image' => $changed_fileName, 'link' => $tf['link'], 'sort_order' => (int)$tf['sort_order']));


                } else {
                    if (isset($tf['banner_image_id']))
                        DB::table('banner_images')->insert(array('banner_image_id' => $tf['banner_image_id'], 'banner_id' => (int)$banner_id, 'title' => $tf['title'], 'image' => $tf['image'], 'link' => $tf['link'], 'sort_order' => (int)$tf['sort_order']));
                    else
                        DB::table('banner_images')->insert(array('banner_id' => (int)$banner_id, 'title' => $tf['title'], 'image' => $tf['image'], 'link' => $tf['link'], 'sort_order' => (int)$tf['sort_order']));

                }
            }
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: banner Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/banner');
    }


    public
    function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {


            $delete_status = DB::table('banner')->where('banner_id', $id)->delete();
            $trr = DB::table('banner_images')->where('banner_id', (int)$id)->get();
            if (isset($trr))
                foreach ($trr as $trr_f) {
                    if (!empty($trr_f->image) && $trr_f->image != null)
                        if (file_exists(public_path('/uploads/banner/' . $trr_f->image)))
                            unlink(public_path('/uploads/banner/' . $trr_f->image));
                    DB::table('banner_images')->where('banner_image_id', (int)$trr_f->banner_image_id)->delete();
                }
            if ($delete_status) {
                $request->session()->flash('success', 'banner removed successfully');
            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public
    function show()
    {
    }
}
