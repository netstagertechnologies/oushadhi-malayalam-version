<?php

namespace App\Http\Controllers\Frontend;

use App\models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Session;

class NewsController extends Controller
{
    public function index(Request $request)
    {
         if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'വാർത്തകൾ', 'link' => '#');
            $data['page_title'] = 'മീഡിയ';
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'News', 'link' => '#');
            $data['page_title'] = 'Media';
        }
     
        $data['news'] = News::where('status', 1)->get();
        
        return view('frontend.news.index', $data);
    }

    public function details(Request $request, $slug = '')
    {
        if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'വാർത്തകൾ', 'link' => URL::to('/') . '/news');
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'News', 'link' => URL::to('/') . '/news');
        }
    
        $medias = News::where('slug', $slug)->first();
        if ($medias) {
            if(Session::get('language')=="ml"){
                $data['breadcrumbs'][] = array('name' => $medias->title_ml, 'link' => '#');
            }else{
                $data['breadcrumbs'][] = array('name' => $medias->title, 'link' => '#');
            }            
            $data['page_title'] = $medias->title;
            $data['page_data'] = $medias;
            return view('frontend.news.details', $data);
        } else
            return redirect('page-not-found');

        return view('frontend.news.details', $data);
    }
}
