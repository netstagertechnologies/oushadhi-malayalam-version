<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use DB;
use Session;
class MediaController extends Controller
{
    public function index(Request $request)
    {
        if(Session::get('language')=="ml"){
        $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'മീഡിയ', 'link' => '#');
        }else{
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Media', 'link' => '#');   
        }
        $data['medias'] = DB::table('media')->where('status', 1)->get();
        $data['media_videos'] = DB::table('media_videos')->where('status', 1)->get();
        $data['page_title'] = 'Media';
        return view('frontend.media.index', $data);
    }
    public function details(Request $request,$slug='')
    {
        if(Session::get('language')=="ml"){
         $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'മീഡിയ', 'link' => URL::to('/').'/media');
        }else{
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Media', 'link' => URL::to('/').'/media');
        }

        $medias = DB::table('media')->where('slug', $slug)->first();
        if($medias) {
            if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => $medias->title_ml, 'link' => '#');
            $data['page_title'] = $medias->title_ml;
            }else{
            $data['breadcrumbs'][] = array('name' => $medias->title, 'link' => '#');
            $data['page_title'] = $medias->title;
            }      
            $data['media_images'] = DB::table('media_images')->where('media_id', (int)$medias->media_id)->get();
        } else
            return redirect('page-not-found');

        return view('frontend.media.gallery', $data);
    }

}
