<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Mails\RawMaterialRequest;
use App\models\Activity;
use App\models\Blog;
use App\models\Contacts;
use App\models\Notifications;
use App\models\RawMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use DB;
use Mail;
use Session;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
       
        $data['slug'] = \Request::segment(1);
        $pages = DB::table('pages')->where('slug', $data['slug'])->first();
        if(Session::get('language')=="ml"){
         $breadcrumbs[] = array('name' => 'ഹോം', 'link' => URL::to('/'));
         $page_title = "ഹോം";
        }else{
          $breadcrumbs[] = array('name' => 'Home', 'link' => URL::to('/'));
          $page_title = "Oushadhi";
        }     

        if ($pages != null) {
            $page_details = $pages;
            if(Session::get('language')=="ml"){
             $breadcrumbs[] = array('name' => $pages->title_ml, 'link' => '#');
             $page_title = $pages->title_ml;
            }else{
               $page_title = $pages->title;
             $breadcrumbs[] = array('name' => $pages->title, 'link' => '#');
            }    
          
            return view('frontend.pages.cms', compact('page_details', 'breadcrumbs', 'page_title'));
        } else {
            return redirect('page-not-found');
        }
    }

    public function aboutUs()
    {
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'About Us', 'link' => '#');
        return view('frontend.pages.about-us', $data);
    }

    public function activities()
    {
        if(Session::get('language')=="ml"){
        $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'പ്രവർത്തനങ്ങൾ', 'link' => '#');
        }else{
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Activities', 'link' => '#');
        }
        $data['activities'] = Activity::where('status', 1)->orderBy('activity_id', 'DESC')->get();

        return view('frontend.pages.activities', $data);
    }

    public function media()
    {
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Media', 'link' => '#');

        return view('frontend.pages.media', $data);
    }

    public function tenders()
    {
        if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'ടെൻഡറുകൾ', 'link' => '#');
            $data['page_title'] = "ടെൻഡറുകൾ";
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'Tenders', 'link' => '#');
            $data['page_title'] = "Tenders";
        }
        $data['tenders'] = array();
        $tenders = DB::table('tenders')->where('status', 1)->orderBy('tender_id', 'DESC')->get();
        foreach ($tenders as $tender)
            $data['tenders'][] = array(
                'tender_id' => $tender->tender_id,
                'title' => $tender->title,
                'title_ml' => $tender->title_ml,
                'tender_type' => ($tender->tender_type == 2) ? 2 : 1,
                'dept' => $tender->dept,
                'dept_ml' => $tender->dept_ml,
                'contact' => $tender->contact,
                'contact_email' => $tender->contact_email,
                'tender_link' => $tender->tender_link,
                'from_date' => $tender->from_date,
                'to_date' => $tender->to_date,
                'last_date' => $tender->last_date,
                'last_time' => $tender->last_time,
                'tender_files' => DB::table('tender_files')->where('tender_id', (int)$tender->tender_id)->orderBy('file_id', 'DESC')->get()
            );
          
        return view('frontend.pages.tenders', $data);
    }


    public function contactUs()
    {
        if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'ഞങ്ങളെ സമീപിക്കുക', 'link' => '#');  
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'Contact Us', 'link' => '#');
        }
    
        $data['contacts'] = DB::table('addresses')->where('status', 1)->orderby('sort_order', 'ASC')->get();
        return view('frontend.pages.contact-us', $data);
    }

    public function downloads()
    {
        if(Session::get('language')=="ml"){
             $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
             $data['breadcrumbs'][] = array('name' => 'ഡൗൺലോഡുകൾ', 'link' => '#');
        }else{
             $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
             $data['breadcrumbs'][] = array('name' => 'Downloads', 'link' => '#');
        }
       
        $downloads = DB::table('downloads')->where('status', 1)->orderby('id', 'ASC')->get();
        foreach ($downloads as $download)
            $data['downloads'][] = array(
                'download_id' => $download->id,
                'title' => $download->title,
                'title_ml' => $download->title_ml,
                'download_files' => DB::table('download_files')->where('download_id', (int)$download->id)->orderBy('file_id', 'DESC')->get()
            );
            
        return view('frontend.pages.downloads', $data);
    }

    public function careers()
    {
        if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'കരിയറുകൾ', 'link' => '#');
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'Careers', 'link' => '#');
        }
       
        $downloads = DB::table('careers')->where('status', 1)->orderby('id', 'ASC')->get();
        $data['careers'] = array();
        foreach ($downloads as $download)
            $data['careers'][] = array(
                'career_id' => $download->id,
                'title' => $download->title,
                'title_ml' => $download->title_ml,
                'dept' => $download->dept,
                'dept_ml' => $download->dept_ml,
                'contact' => $download->contact,
                'contact_email' => $download->contact_email,
                'last_date' => $download->last_date,
                'career_files' => DB::table('career_files')->where('career_id', (int)$download->id)->orderBy('file_id', 'DESC')->get()
            );

        return view('frontend.pages.careers', $data);
    }

    public function contactUsFormSubmit(Request $request)
    {

        $request->validate([
            "email" => "required|string|min:3|max:50",
            "name" => "required|string|email|max:255",
            "mob_no" => "required|numeric|digits_between:9,12",
            "message" => "required|min:3"
        ]);

        DB::table('contact_form')->insert(array('name' => $request->name,
            'email' => $request->email,
            'mob_no' => $request->mob_no,
            'message' => $request->message,
            'is_read' => 0,
            'is_replied' => 0,
            'date_added' => now(),
            'date_modified' => now()
        ));


        $request->session()->flash('success', 'Thank you. We will contact you soon!');
        return redirect('contact-us');
    }


    /*public function careers()
    {
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Careers', 'link' => '#');

        return view('frontend.pages.careers', $data);
    }*/

    public function rawMaterialReqEnq(Request $request)
    {
        $raw_m = RawMaterial::where('raw_material_id', (int)$request->raw_material)->first();
        if ($raw_m) {
            $insertData = array(
                'raw_material_id' => $request->raw_material,
                'name' => $request->rm_name,
                'email' => $request->rm_email,
                'phone' => $request->rm_phone,
                'message' => $request->rm_message,
                'is_read' => 0,
                'date_added' => now(),
                'date_modified' => now()
            );
            $id = DB::table('raw_material_enquiries')->insertGetId($insertData);
            $raw_material_data = array('name' => $raw_m->contact_person,
                'email' => $raw_m->contact_email,
                'title' => $raw_m->title,
                'common_name' => $raw_m->common_name
            );
            if ($id) {
                $request->session()->flash('success', 'Thank you. Your Enquiry has been submitted. We will contact you soon!');
                Mail::to(trim($raw_m->contact_email))->send(new RawMaterialRequest(
                    array('request_data' => $insertData, 'material_data' => $raw_material_data)));

                $mg = 'New Enquiry received for`' . $raw_m->title . '`';
                Notifications::addNotification(1, 'raw_material', 'admin', (int)$request->raw_material, $mg, '');

            } else
                $request->session()->flash('error', 'Sorry. Something went wrong please try again!');
        } else
            $request->session()->flash('error', 'Sorry. Raw material request not found');

        return Redirect()->back();
    }

    public function careersFormSubmit(Request $request)
    {

        $request->validate([
            "name" => "required|string|min:3|max:50",
            "email" => "required|string|email|max:255",
            "mob_no" => "required|numeric|digits_between:9,12",
            "message" => "required|min:3",
            'resume' => 'nullable|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
        ]);

        $id = DB::table('career_form')->insertGetId(array('name' => $request->name,
            'email' => $request->email,
            'mob_no' => $request->mob_no,
            'message' => $request->message,
            'is_read' => 0,
            'is_replied' => 0,
            'date_added' => now(),
            'date_modified' => now()
        ));

        if ($request->hasFile('resume')) {
            $file = $request->file('resume');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = 'CRR' . uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/careers';
            $file->move($file_saving_path, $changed_fileName);
            DB::table('career_form')->where('career_id', (int)$id)->update(['doc_path' => $changed_fileName]);
        }
        $request->session()->flash('success', 'Thank you. We will contact you soon!');
        return redirect('careers');
    }

    public function blog(Request $request, $slug = null)
    {
        if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'ബ്ലോഗ്', 'link' => '#');
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'Blog', 'link' => '#');
        }       

        if ($slug != null) {
            $blog = Blog::where('slug', $slug)->first();
            if ($blog) {
                if(Session::get('language')=="ml"){
                     $data['page_title'] = $blog->title_ml;
                }else{
                     $data['page_title'] = $blog->title;
                }  
               
                $data['page_data'] = $blog; 
                return view('frontend.pages.blog', $data);
            } else
                return redirect('page-not-found');
        } else
            return redirect('page-not-found');
    }

    public function boardDirectors()
    {
      
        if(Session::get('language')=="ml"){
        $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'ഡയറക്ടർ ബോർഡ്', 'link' => '#');
        }else{
              $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Board Of Directors', 'link' => '#');
        }
        $data['contacts'] = Contacts::where('status', 1)->where('contact_type', 'directors')->orderby('sort_order', 'ASC')->get();
        return view('frontend.pages.directors', $data);
    }

    public function officers()
    {
         if(Session::get('language')=="ml"){
          $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
          $data['breadcrumbs'][] = array('name' => 'ഉദ്യോഗസ്ഥർ', 'link' => '#');
        }else{
          $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
          $data['breadcrumbs'][] = array('name' => 'Officers', 'link' => '#');
        }
      

        $data['contacts'] = Contacts::where('status', 1)->where('contact_type', 'officers')->orderby('sort_order', 'ASC')->get();
        return view('frontend.pages.officers', $data);
    }

    public function rawMaterialReq()
    {
        if(Session::get('language')=="ml"){
            $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'ആവശ്യമായ അസംസ്കൃത വസ്തുക്കൾ', 'link' => '#');
        }else{
            $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
            $data['breadcrumbs'][] = array('name' => 'Raw Materials Required', 'link' => '#');
        }
        
        $raw_materials = RawMaterial::where('status', 1)->orderBy('raw_material_id', 'ASC')->get();
        $data['raw_materials'] = array();
        foreach ($raw_materials as $raw_material) {
            $data['raw_materials'][$raw_material->raw_type]['title'] = RawMaterial::getRawMaterialTypeTitle($raw_material->raw_type);
            $data['raw_materials'][$raw_material->raw_type]['materials'][] = $raw_material;
        }

        $data['raw_types'] = DB::table('raw_material_types')->select('id','title')->orderby('title','ASC')->get();

        return view('frontend.pages.raw_materials', $data);
    }

    public function activitiesOfRD()
    {
        if(Session::get('language')=="ml"){
        $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'ഗവേഷണ-വികസന പ്രവർത്തനങ്ങൾ', 'link' => '#');
        }else{
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Activities of R & D', 'link' => '#');
        }      

        return view('frontend.pages.activities-rd', $data);
    }

    public function departments()
    {
        if(Session::get('language')=="ml"){
          $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
          $data['breadcrumbs'][] = array('name' => 'ഞങ്ങളേക്കുറിച്ച്', 'link' => route('about-us'));
          $data['breadcrumbs'][] = array('name' => 'വകുപ്പുകൾ', 'link' => '#');
        }else{
           $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'About Us', 'link' => route('about-us'));
        $data['breadcrumbs'][] = array('name' => 'Departments', 'link' => '#');
        }
      
        $data['departments'] = DB::table('departments')->where('status', 1)->orderBy('title', 'ASC')->get();

        return view('frontend.pages.departments', $data);
    }
    public function hospital()
    {   
        if(Session::get('language')=="ml"){
        $data['breadcrumbs'][] = array('name' => 'ഹോം', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'ആശുപത്രി', 'link' => '#');
        $data['page_title'] = 'ആശുപത്രി';
        }else{
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Hospital', 'link' => '#');
        $data['page_title'] = 'Hospital';
        }
       
        return view('frontend.pages.hospital', $data);
    }
}
