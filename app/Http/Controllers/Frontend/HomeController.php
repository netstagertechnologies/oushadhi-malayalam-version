<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\models\Blog;
use App\models\FactoryImage;
use App\models\News;
use App\models\Product;
use App\models\Testimonial;
use App\User;
use Illuminate\Http\Request;
use DB;
use Session;
use App;
use Illuminate\Support\Facades\Crypt;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       $locale = Session::get('language');
        App::setlocale($locale);
        $data = array();
        $banner = DB::table('banner')->where('status', 1)->first();
        if (isset($banner->banner_id))
        $data['banners'] = DB::table('banner_images')->where('banner_id', (int)$banner->banner_id)->orderBy('sort_order', 'ASC')->get();
        $data['latest_news'] = News::where('status', 1)->orderBy('news_id', 'DESC')->limit(5)->get();
        $data['featured_products'] = Product::where('status', 1)->where('featured', 1)->where('show_home', 1)->orderBy('title', 'ASC')->limit(4)->get();
        $data['latest_blog'] = Blog::where('status', 1)->orderBy('blog_id', 'DESC')->limit(2)->get();
        $data['testimonials'] = Testimonial::where('status', 1)->orderBy('testimonial_id', 'DESC')->get();
        $data['factory_images'] = FactoryImage::get();
        $home_cms = DB::table('home_page_cms')->get();
        if ($home_cms)
            foreach ($home_cms as $cms)
                $data['home_cms'][$cms->h_section] = array(
                    'caption' => $cms->h_caption,
                    'caption_ml' => $cms->h_caption_ml,
                    'title' => $cms->h_title,
                    'title_ml' => $cms->h_title_ml,
                    'description' => $cms->h_description,
                    'description_ml' => $cms->h_description_ml,
                    'image' => $cms->h_image,
                    'link' => $cms->h_link
                );

        // dd($data);
        return view('frontend.home', $data);
    }

    public function verifyAccount(Request $request,$user_id,$cus_token){
        if($user_id){
            $user=(int)Crypt::decrypt($user_id);
            $user_details=DB::table('users')->where('id',(int)$user)->first();
            if($user_details) {
                if ($user_details->cus_token == $cus_token) {
                    $up = User::where('id', (int)$user)->update(['is_verified' => 1, 'cus_token' => md5(rand())]);
                    if ($up)
                        $request->session()->flash('success', 'Success: User verification completed');
                    else
                        $request->session()->flash('error', 'Sorry: User verification failed. Please try again');
                } else
                    $request->session()->flash('error', 'Sorry: link expired');
            }else
                $request->session()->flash('error', 'Sorry: User not found');
        }
        return redirect('login');
    }
    public function set_language(){
       Session::forget('language');
       Session::put('language', request()->lang);
       echo(Session::get('language'));
    }
}
