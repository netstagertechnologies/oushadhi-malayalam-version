<?php

namespace App\Mails;

use App\models\Settings;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountRejected extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $mydata;


    public function __construct($datas)
    {
        $this->mydata = $datas;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_store = Settings::getEmailSettings();
        $this->from($email_store, 'Oushadhi');
        return $this->view('frontend.emails.account_rejected',$this->mydata);
    }
}
