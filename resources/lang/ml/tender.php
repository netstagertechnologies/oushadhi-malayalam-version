<?php
return ['Last date & Time'=>'അവസാന തീയതിയും സമയവും',
        'Department'=>'വകുപ്പ്',
        'Contact No.'=>'ബന്ധപ്പെടേണ്ട നമ്പർ',
        'Contact Email'=>'ബന്ധപ്പെടേണ്ട ഇമെയിൽ',
        'Tender Link'=>'ടെണ്ടർ ലിങ്ക്'];