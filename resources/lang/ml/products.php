<?php 

return ['Total Ingredients'=>'ആകെ ചേരുവകൾ',
        'Main Ingredients'=>'പ്രധാന ചേരുവകൾ',
        'Indication'=>'സൂചന',
        'Special Indication'=>'പ്രത്യേക സൂചന',
        'Usage'=>'ഉപയോഗം',
        'Dosage'=>'അളവ്',
        'Presentation'=>'അവതരണം',
        'Share'=>'പങ്കിടുക',
        'Review'=>'അവലോകനം',
        'Roll over image to zoom in'=>'സൂം ഇൻ ചെയ്യുന്നതിന് ചിത്രത്തിന് മുകളിലൂടെ റോൾ ചെയ്യുക',
        'Description'=>'വിവരണം',
        'Additional information'=>'അധിക വിവരങ്ങൾ',
         '--Search Product--'=>'--ഉൽപ്പന്നം തിരയുക--',
       

        ];