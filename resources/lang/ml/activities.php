<?php 
return [ 'New product development'=>'പുതിയ ഉൽപ്പന്ന വികസനം',
         'Process modification'=>'പ്രോസസ്സ് പരിഷ്‌ക്കരണം',
         'Product modification'=>'ഉൽപ്പന്ന പരിഷ്‌ക്കരണം',
         'Standardization at Raw Material in process and product levels'=>'പ്രക്രിയയിലും ഉൽപ്പന്ന തലത്തിലും അസംസ്കൃത വസ്തുക്കളിൽ സ്റ്റാൻഡേർഡൈസേഷൻ',
         'Inter disciplinary work pattern'=>'ഇന്റർ ഡിസിപ്ലിനറി വർക്ക് പാറ്റേൺ',
         'Corrective &preventive actions'=>'തിരുത്തൽ, പ്രതിരോധ പ്രവർത്തനങ്ങൾ',
         'Extension activities- Training programme for Ayurveda pharmacy MD students, Biotechnology
                    scholars, Microbiology graduates, Biochemistry scholars etc'=>'വിപുലീകരണ പ്രവർത്തനങ്ങൾ - ആയുർവേദ ഫാർമസി എംഡി വിദ്യാർത്ഥികൾക്കുള്ള പരിശീലന പരിപാടി, ബയോടെക്നോളജി
                    പണ്ഡിതന്മാർ, മൈക്രോബയോളജി ബിരുദധാരികൾ, ബയോകെമിസ്ട്രി പണ്ഡിതന്മാർ തുടങ്ങിയവർ',
                    'Demonstration Garden & Drug Gallery'=>'ഡെമോൺസ്ട്രേഷൻ ഗാർഡൻ & ഡ്രഗ് ഗാലറി',
                    'Collaborative projects'=>'സഹകരണ പദ്ധതികൾ',    

];