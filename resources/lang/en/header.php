<?php


return [
    'home'       => 'home',
    'about us'       => 'About Us',
     'Board Directors'=>'Board Directors',
    'Officers'=>'Officers',
    'Departments'=>'Departments',
    'Products'       => 'Products',
    'Ayurveda'       => 'Ayurveda',
    'Siddha'       => 'Siddha',
    'Activities'       => 'Activities',
    'Hospital'       => 'Hospital',
    'Media'       => 'Media',
    'Tenders'       => 'Tenders',
    'Careers'       => 'Careers',
    'Contact Us'       => 'Contact Us',
    'Raw Materials'       => 'Raw Materials',
    'Raw Materials Required'=>'Raw Materials Required',
    'FEATURED PRODUCTS'       => 'FEATURED PRODUCTS',
    'NEWS & EVENTS'       => 'NEWS & EVENTS',
    'TESTIMONIALS'=>'TESTIMONIALS',
    'LATEST BLOG'=>'LATEST BLOG',
    'About Oushadhi'=>'About Oushadhi',
     'QUICK LINKS'=>'QUICK LINKS',
     'FOLLOW US'=>'FOLLOW US',
         'copyright'=>'Oushadhi.org All Rights Reserved',
         'RTI'=>'RTI Act',
         'Downloads'=>'Downloads',
         'R&D Activities'=>'R&D Activities',
         'CSR Activities'=>'CSR Activities',
         'Medicinal Plant Cultivation'=>'Medicinal Plant Cultivation',
         'Product Categories'=>'Product Categories',
        'Sort By'=>'Sort By',



];