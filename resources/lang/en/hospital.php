<?php


return [
    'introduction'=>'God’s own country ‘Kerala’ is known all over the world for many unique things and the traditional
                    Ayurveda is one among them. There are more than 12,000 traditional healers and 800 Ayurveda medicine
                    manufacturing units in the state. Oushadhi is the largest manufacturer of Ayurvedic Medicine in the
                    government sector in India. Oushadhi Panchakarma Hospital & Research Institute located in the heart
                    of Thrissur, the cultural capital of Kerala, offers varieties of Ayurveda treatments at affordable
                    price using quality medicines produced by ‘Oushadhi’. There is always heavy rush for the treatment
                    in this government run Panchakarma Hospital and bookings are done well in advance.',
    'Our Specialties'=>'Our Specialties',
    'PANCHAKARMA THERAPY'=>'PANCHAKARMA THERAPY',

    'PANCHAKARMA DESC'=>'Panchakarma therapy (penta fold purificatory measures) play a vital role in Ayurvedic therapies and
                    as such, they occupy an important place in Ayurvedic system of Medicine. The classical Ayurveda
                    Panchakarma therapy is the comprehensive method of internal purification of the body by VAMANA,
                    VIRECHANA, VASTHI, NASYA & VAMANA',
    'SEC1'=>'Process by which vitiated humours are eliminated through oral route, for the purification of the
                    upper half of the body.(Induced emesis-specific therapy for kaphaja disorders)',
                   
    'VIRECHANA'=>'VIRECHANA',
    'VIRECHANA DESC'=>'rocess by which vitiated humours are eliminated through anal
                            route, for the purification of the lower half of the body. (Induced
                            Purgation/Laxation-specific therapy for paithika Disorders)',
    'VASTHI'=>'VASTHI',
    'VASTHI DESC'=>'Purificatory measure which consists of suitable medicaments in the
                            form of enemata through rectal, vaginal or urethral routes (Oleus and Unoleus
                            Enemata-specified therapy for Vathika Disorders)',
    'NASYA'=>'NASYA',
    'NASYA DESC'=>'Purificatory measures by which suitable medicaments are introduced
                            through nasal passage in liquid or in powder form. (Errhines/Snuffing-therapy for diseases
                            related with head)',

    'RAKTHAMOKSHAM'=>'RAKTHAMOKSHAM',
    'RAKTHAMOKSHAM DESC'=>'Purificatory measures in diseases related with impure blood by
                        using external means like, instruments (Prachanam), leeches(Jalookavacharanam), horns etc,
                        (Bloodletting-specific therapy for diseases Related to Blood)',
    'RECREATION OF BODY'=>'RECREATION OF BODY',
    'DHARA'=>'DHARA',
    'DHARA DESC'=>'Uninterrupted application of medicated oil (Thailadhara), buttermilk (Thakradhara),
                                    milk
                                    (Ksheeradhara), fermented whey (Dhanyamladhara) on the head, throughout body or both
                                    for
                                    Insomnia, Rheumatic complaints etc.',
    'PIZHICHIL'=>'PIZHICHIL',                                
    'PIZHICHIL DESC'=>'Pouring comfortable warm medicated oil liberally over the body
                                    followed by gentle massage for skin disease and rheumatic complaints.',
    'NAVARAKKIZHI'=>'NAVARAKKIZHI',
    'NAVARAKKIZHI DESC'=>'Massage done with small linen bags filled with medicated and
                                    cooked Navara rice, after a liberal application of medicated oils for rheumatic
                                    complaints
                                    &rejuvenation.',
    'THAKRADHARA'=>'THAKRADHARA',                                
    'THAKRADHARA DESC'=>'Uninterrupted application of medicated butter milk on the head for sleeplessness, skin diseases, mental disorders etc.',
    'THALAPOTHICHIL'=>'THALAPOTHICHIL',
    'THALAPOTHICHIL DESC'=>'Procedure in which herbal medicines are made into paste and
                                    applied over the head 9in a special manner for mental disorders etc.',
                                    'UDHWARTHANAM'=>'UDHWARTHANAM',
    'UDHWARTHANAM'=>'UDHWARTHANAM',                                
    'UDHWARTHANAM DESC'=>'Massage with medicated powders specially meant for reducing
                                    body fat, improvement of complexion land functional improvement of vital organs.',
    'NAVARATHEPPU'=>'NAVARATHEPPU',
     'NAVARATHEPPU DESC'=> 'Application of medicated cooked navara rice on whole or
                                    affected parts of the body for rheumatic complaints &rejuvenation.',
    'SNEHANAM'=>'SNEHANAM',
    'SNEHANAM DESC'=> 'Oleation therapy-Internal as well as external done prior of
                                    Panchakarma Therapy for rheumatic complaints, skin diseases, mental disorders
                                    etc.',
    'SNEHAPANAM' => 'SNEHAPANAM',
    'SNEHAPANAM DESC' =>'Intake of medicated unctuous Dravya like Ghee, Oil etc. for a
                                    specific duration especially for chronic diseases.', 
    'ABHYANGAM'=>'ABHYANGAM',
    'ABHYANGAM DESC'=>'Application of medicated oil all over the body. It relieves
                                    fatigue and provides stamina and perfect sleep..',
    'PALPPUKA'=>'PALPPUKA',
    'PALPPUKA DESC'=>'Sudation Technique with medicated milk for arthritic problems.',
    'UPANAHAM'=>'UPANAHAM',
    'UPANAHAM DESC'=>'Wrapping specific areas for a specific time interval with
                                    medicated pastes intended to subside inflammation or pain.',
    'SWEDANAM'=>'SWEDANAM',
    'SWEDANAM DESC'=> 'Sudation technique done prior to Panchakaram. Therapy for Thirteen
                                    methods of thermal Sudation like Mixed Fomentation, Hot bed Sudation, Steam kettle
                                    Sudation,
                                    Affusion Sudation, Bath Sudation, Sudatorium Sudation, Stone bed Sudation, Trench
                                    Sudation,
                                    Cabin Sudation, Ground red Sudation, Picher bed Sudation, Pit Sudation, Under bed
                                    Sudation,
                                    are followed in this method.',
    'KIZHI'=>'KIZHI',
    'KIZHI DESC'=>'Fomentation technique by using boluses of herbal leaves (Elakkizhi)
                                    or medicated powders (Choornakkizhi) or warm sand (Manalkkizhi) or roasted salt
                                    (Uppukkizhi)
                                    according to the conditions of disease',
    'PICHYU'=>'PICHYU',
    'PICHYU DESC'=>'Applying oil soaked cloth over affected body parts intended to
                                    reduce local pain or stiffness.',
    'LEPAM'=>'LEPAM',
    'LEPAM DESC'=>'Applying medicated pastes on the body parts for skin diseases, local
                                    pain & swelling.',
    'REJUVENATION'=>'REJUVENATION',
    'REJUVENATION DESC'=>' Free meditation and yoga classes are arranged for all inmates
                                to rejuvenate the body & mind.',
    'RESEARCH'=>'RESEARCH',
    'RESEARCH DESC'=>'Panchakarma Hospital has a clinical trial cell intended to carry
                                out research on chronic diseases and con ditions like Arthritis, Diabetes, Rheumatic
                                Complaints, Skin Diseases, Head ache, Calculi, Cholesterol etc. using newly
                                developed medicines by the R&D wing of Oushadhi.',
    'LOCATION'=>'LOCATION',
    'FACILITIES'=>'FACILITIES',
    'FACILITIES DESC'=>'HOSPITAL : Well furnished 75 bedded hospital which is always full to the capacity.Bookings are done well in advance',
    






    

];