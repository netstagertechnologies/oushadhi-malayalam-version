<?php


return [
    'About Oushadhi'=>'About Oushadhi',
    'content'=>'Largest producer of Ayurveda medicines in public sector in India.
                        One among the few public sector companies, consistently making profit and paying dividend to government of Kerala.
                        Supplies medicines to dispensaries under ISM Department of state and these medicines reach lakhs of patients at free of cost.
                        <br/>A GMP and ISO 9001 :2015 Certified company.<br/>
                        Produces 498 Ayurveda formulations –both classical and proprietary.
                        Sole supplier of medicines to Government Ayurveda Hospital and Dispensaries in Kerala.
                        Supplier of Ayurveda Medicines to Government Hospitals and Dispensaries of 19 other states, like Madhya Pradesh, Andhra Pradesh, Karnataka, Himachal Pradesh, Punjab, Chhattisgarh, Sikkim etc.
                        Caters to the need of public through a vast network over 650 dealers spread all over the nation.
                        Reimbursement facility available for Central Govt.& State Govt. employees on purchase of Oushadhi Medicines.
                        Equipped with AYUSH accredited laboratory facility',
    'Our Vision'=>'Our Vision',  
    'vision'=>'A leading world class Ayush medicine manufacturing organization by 2025',
    'mission'=>'mission',
    'missn'=>'Production and supply quality medicine at reasonable price',
    'Milestones'=>'Milestones',
    'milestone1'=>'Commenced by His Highness, the Maharaja of Cochin as " Sree Kerala Varma
                        Ayurveda Pharmacy',
    'milestone2'=>'Converted into co-operative society viz. Sree Kerala Varma Ayurveda Pharmacy
                        and Stores Ltd', 
    'milestone3'=>'Registered as a Company under Indian Companies Act 1956 and renamed as "The
                        Pharmaceutical Corporation (Indian Medicines) Kerala Ltd" Thrissur', 
    'milestone4'=>'Commissioned modern manufacturing unit at Kuttanellur and shifted the factory
                        to the new premises', 
    'milestone5'=>'Started a new Panachakarma Hospital and Research Institute at Thrissur', 
    'milestone6'=>'The entire office shifted to the factory premises at Kuttanellur', 
    'milestone7'=>'Commenced full-fledged R&D Centre at Kuttanellur and Regional Distribution
                        Unit at Kannur', 
    'milestone8'=>'Commissioned Center of Excellence for Manufacturing Asavarishta at
                        Kuttanellur', 
    'milestone9'=>'Started Regional Distribution center at Pathanapuram', 
    'milestone10'=>'Commissioned Modern Packing Section complying WHO GMP standard', 
    'milestone11'=>'Established Medicinal Plant Extension Center at Paryaram', 
    'milestone12'=>'Established modern manufacturing unit for Proprietary Medicines at
                        Muttathara', 
    'milestone13'=>'Inaugurated New 50 bed Oushadhi Panachakarma Hospital Block at Thrissur', 
    'milestone14'=>'Established Medicinal Plant Extension Center at Kuttanellur', 
    'Strengths'=>'Strengths',
      'Strengths1'=>   'Established brand image',
    'Strengths2'=>   'Continuous Government Support',
    'Strengths3'=>  'Huge market demand',
    'Strengths4'=>   'Dedicated work force',

    'CORE VALUES'=>'CORE VALUES',
    'CORE VALUES1'=>   'Mutual trust and respect',
    'CORE VALUES2'=>   'Customer satisfaction',
    'CORE VALUES3'=>  'Quality control',
    'CORE VALUES4'=>   'Professional ethics',
    'CORE VALUES5'=>   'March with time',
   

];