<?php 

return ['Total Ingredients'=>'Total Ingredients',
        'Main Ingredients'=>'Main Ingredients',
        'Indication'=>'Indication',
        'Special Indication'=>'Special Indication',
        'Usage'=>'Usage',
        'Dosage'=>'Dosage',
        'Presentation'=>'Presentation',
        'Share'=>'Share',
        'Review'=>'Review',
        'Roll over image to zoom in'=>'Roll over image to zoom in',
        'Description'=>'Description',
        'Additional information'=>'Additional information',
        '--Search Product--'=>'--Search Product--',
       
        ];