@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    @include('template.user.alert')
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUserUrlRoute().'/password/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Old Password</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="password" class="form-control" name="old_password" placeholder="Old Password"
                                                   value="">
                                            {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">New Password</label>
                                        <div class="col-sm-7">
                                            <input type="password" class="form-control" name="password" placeholder="Enter new password"
                                                   value="">
                                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('cpassword') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Re-enter Password</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="password" class="form-control" name="cpassword" placeholder="Re-enter Password"
                                                   value="">
                                            {!! $errors->first('cpassword', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUserUrlRoute().'/password') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
