@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUserUrlRoute().'/quotations/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ csrf_field() }}
                                    @if(Auth::user()->user_type==1)
                                        <div class="form-group {{ $errors->has('budget') ? 'has-error' : ''}}">
                                            <label class="col-sm-3 control-label required">Budget</label>

                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="budget"
                                                       placeholder="Budget"
                                                       value="{{$page_data->budget or ''}}">
                                                {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}

                                            </div>
                                        </div>
                                    @endif

                                    <table id="products"
                                           class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <td class="text-left" style="width:45%">{{ 'Product Name' }}</td>
                                            <td class="text-left" style="width:45%">{{ 'Qty, Price, Total' }}</td>
                                            <td style="width:10%"></td>
                                        </tr>
                                        </thead>
                                        <tbody id="products-tbody">

                                        @php $product_row = 0; $products=array();@endphp
                                        @foreach($products as  $product)
                                            <tr id="product-row{{ $product_row }}">
                                                <td class="text-left" style="width: 40%;"><input type="text"
                                                                                                 name="product_attribute[{{ $product_row }}][name]"
                                                                                                 value="{{ $product_attribute['name'] or ''}}"
                                                                                                 placeholder="Attribute Name"
                                                                                                 class="form-control"/>
                                                    <input type="hidden"
                                                           name="product_attribute[{{ $product_row }}][attribute_id]"
                                                           value="{{ $product_attribute['attribute_id'] or ''}}"/>
                                                </td>
                                                <td class="text-left">
                                                    @foreach($languages as $language)
                                                        <div class="input-group"><span
                                                                class="input-group-addon">{{ $language->code }}</span>
                                                            <textarea
                                                                name="product_attribute[{{ $product_row }}][product_attribute_description][{{ $language->language_id }}][text]"
                                                                rows="2" placeholder="Text"
                                                                class="form-control">{{ $product_attribute['product_attribute_description'][$language->language_id] ? $product_attribute['product_attribute_description'][$language->language_id]['text']:'' }}</textarea>
                                                        </div>
                                                    @endforeach</td>
                                                <td class="text-left">
                                                    <button type="button"
                                                            onclick="$('#product-row{{ $product_row }}').remove();"
                                                            data-toggle="tooltip" title="Remove"
                                                            class="btn btn-danger"><i
                                                            class="fa fa-minus-circle"></i></button>
                                                </td>
                                            </tr>
                                            @php $product_row = $product_row + 1; @endphp
                                        @endforeach
                                        </tbody>

                                        <tfoot>

                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left">
                                                <button type="button" onclick="addProductRowCheck();"
                                                        data-toggle="tooltip" title="Add Product"
                                                        class="btn btn-primary"><i
                                                        class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                        <tr class="dt">
                                            <td colspan="2" class="text-right"><b>Products Total</b></td>
                                            <td class="text-left" id="quot-ptotal">
                                                <b>Rs 0</b>
                                            </td>
                                        </tr>
                                        <tr class="dt">
                                            <td colspan="2" class="text-right"><b>Tax Total</b></td>
                                            <td class="text-left" id="quot-ttotal">
                                                <b>Rs 0</b>
                                            </td>
                                        </tr>
                                        <tr class="dt">
                                            <td colspan="2" class="text-right"><b>Grand Total</b></td>
                                            <td class="text-left" id="quot-total">
                                                <b>Rs 0</b>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>


                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label"></label>
                                    <div class="col-sm-10">

                                        <button type="submit" class="btn btn-success pull-right">Submit</button>
                                        <a href="{{ url(\App\Utils::getUserUrlRoute().'/quotations') }}"
                                           class="btn btn-danger pull-left"><i class="fa fa-chevron-left"
                                                                               aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>


    <script>
        var product_row = '{{ $product_row }}';

        function addProductRowCheck() {
            @if(Auth::user()->user_type==1)
                budget = $('input[name=\'budget\']').val();
            if (budget > 0) {
                totl = calcTotal();
                if (tot > budget) {
                    var r = confirm("Total amount exceeds your budget amount. Do you want to continue??");
                    if (r) {
                        addProductRow();
                        return true;
                    } else {
                        return false;
                    }
                } else
                    addProductRow();
            } else {
                alert("Enter valid budget amount");
            }
            @else
            addProductRow();
            @endif
        }

        function addProductRow() {
            html = '<tr id="product-row' + product_row + '">';
            html += '  <td class="text-left" style="width: 40%;"><input type="text" name="products[' + product_row + '][name]" value="" placeholder="{{ 'Product Name' }}" class="form-control" /><input type="hidden" name="products[' + product_row + '][product_id]" value="" /></td>';
            html += '  <td class="text-left" id="product-row-unit' + product_row + '"></td>';
            html += '  <td class="text-left"><button type="button" onclick="$(\'#product-row' + product_row + '\').remove();calcTotal();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#products-tbody').append(html);

            productautocomplete(product_row);

            product_row++;
        }

        function productautocomplete(product_row) {
            $('input[name=\'products[' + product_row + '][name]\']').autocomplete({
                'source': function (request, response) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    var query = $('input[name=\'products[' + product_row + '][name]\']').val();

                    $.ajax({
                        url: '{{ route('user.product.fetch') }}',
                        dataType: 'json',
                        method: "POST",
                        data: {query: query, _token: CSRF_TOKEN},
                        success: function (json) {
                            response($.map(json, function (item) {
                                return {
                                    value: item['product_id'],
                                    label: item['name'],
                                    //category: item['attribute_group']
                                }
                            }));
                        }
                    });
                },
                'select': function (items) {
                    $('input[name=\'products[' + product_row + '][name]\']').val(items['label']);
                    $('input[name=\'products[' + product_row + '][product_id]\']').val(items['value']);
                    modifyUnitPriceColumn(product_row, items['value'])

                }
            });
        }

        function modifyUnitPriceColumn(product_rows, product_id) {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '{{ route('user.product.price') }}',
                dataType: 'json',
                method: "POST",
                data: {product_id: product_id, _token: CSRF_TOKEN},
                success: function (json) {
                    html1 = '<table class="table table-bordered">';
                    html1 += '<thead><th style="width:20%">UNIT</th><th style="width:20%">PRICE</th>';
                    html1 += '<th style="width:30%">QTY</th><th style="width:30%">TOTAL</th></thead>';
                    col_cnt = 0;
                    $.each(json, function (i, item) {
                        html1 += '<tr><td>' + this.unit_value + '<input type="hidden" name="products[' + product_rows + '][desc][' + col_cnt + '][unit_value]"  value="' + this.unit_value + '"/></td>';
                        html1 += '<td>Rs ' + this.price + '<input type="hidden"  name="products[' + product_rows + '][desc][' + col_cnt + '][price]" value="' + this.price + '" id="hidden-price' + product_rows + '-' + col_cnt + '" />';
                        html1 += '<input type="hidden"  name="products[' + product_rows + '][desc][' + col_cnt + '][uprice]" value="' + this.uprice + '" id="hidden-uprice' + product_rows + '-' + col_cnt + '" />';
                        html1 += '<input type="hidden"  name="products[' + product_rows + '][desc][' + col_cnt + '][utax]" value="' + this.utax + '" id="hidden-utax' + product_rows + '-' + col_cnt + '" /></td>';
                        html1 += '<td><input type="number" value="0" name="products[' + product_rows + '][desc][' + col_cnt + '][qty]" min="0" class="form-control" onchange="calculatePrice(this,' + product_rows + ',' + col_cnt + ')" ></td>';
                        html1 += '<td><input type="text" name="products[' + product_row + '][' + col_cnt + '][desc][total]" value="0" readonly id="product-row-total' + product_rows + '-' + col_cnt + '" placeholder="{{ 'Total' }}"  value="0" class="form-control quot-tot-row" />';
                        html1 += '<input type="hidden" name="products[' + product_row + '][' + col_cnt + '][desc]uprice_total]" value="0" readonly id="product-row-uptotal' + product_rows + '-' + col_cnt + '" value="0" class="form-control quot-utot-row" />';
                        html1 += '<input type="hidden" name="products[' + product_row + '][' + col_cnt + '][desc][utax_total]" value="0" readonly id="product-row-uttotal' + product_rows + '-' + col_cnt + '"   value="0" class="form-control quot-ttot-row" /></td></tr>';
                        col_cnt = col_cnt + 1;
                    });
                    html1 += '</table>';

                    $('#product-row-unit' + product_rows).html(html1);
                    $('#product-row-' + product_rows).val(0);
                }
            });

        }

        function calculatePrice(col, p_row, price_row) {
            qty = parseInt($(col).val());
            total = parseFloat(($('#product-row-total' + p_row).val())).toFixed(2);
            unit_price = $('#hidden-price' + p_row + '-' + price_row).val();
            $('#product-row-total' + p_row + '-' + price_row).val((qty * unit_price));

            $('#product-row-uptotal' + p_row + '-' + price_row).val((qty * $('#hidden-uprice' + p_row + '-' + price_row).val()));
            $('#product-row-uttotal' + p_row + '-' + price_row).val((qty * $('#hidden-utax' + p_row + '-' + price_row).val()));


            tot = calcTotal();
            @if(Auth::user()->user_type==1)
                budget = parseFloat($('input[name=\'budget\']').val()).toFixed(2);
            if (tot > budget)
                alert('Total amount exceeded your budget amount.');
            @endif
        }

        function calcTotal() {
            tot = 0;
            $('input.quot-tot-row').each(function () {
                tot += parseFloat($(this).val());
            })
            $('#quot-total').html('<b>Rs ' + parseFloat(tot).toFixed(2) + '</b>');

            utot = 0;
            $('input.quot-utot-row').each(function () {
                utot += parseFloat($(this).val());
            })
            $('#quot-ptotal').html('<b>Rs ' + parseFloat(utot).toFixed(2) + '</b>');

            ttot = 0;
            $('input.quot-ttot-row').each(function () {
                ttot += parseFloat($(this).val());
            })
            $('#quot-ttotal').html('<b>Rs ' + parseFloat(ttot).toFixed(2) + '</b>');


            return tot;
        }

        $('#products tbody tr').each(function (index, element) {
            productautocomplete(index);
        });


    </script>
@endsection
