@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>


        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('template.user.alert')
                            <div class="pull-right">
                                @if($page_data->quotation_status!=5)
                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modal-default">
                                        Cancel Perfoma Invoice
                                    </button>
                                @endif
                                <a
                                    href="{{ url(\App\Utils::getUserUrlRoute().'/quotations/') }}"
                                    class="btn btn-warning btn-sm" title="Back">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back
                                </a>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-general" data-toggle="tab">{{ 'Overview' }}</a>
                                </li>
                                <li><a href="#tab-status" data-toggle="tab">{{ 'Status' }}</a></li>
                            </ul>
                            <div class="tab-content">
                                <br/>
                                <div class="tab-pane active" id="tab-general">
                                    <div class="pull-right">
                                        {{-- <a href="#" class="btn btn-info btn-sm" onclick="exportToPDF()"><i
                                                 class="fa fa-file-pdf-o" aria-hidden="true"></i> Export to PDF</a>
                                         <br/><br/>--}}

                                        <button id="expCsv" class="btn btn-success">EXPORT TO CSV</button>
                                        <button id="expPdf" class="btn btn-warning">EXPORTTO PDF</button>
                                        <br/><br/>
                                    </div>
                                    <div>
                                        <table class="table table-striped" id="quotatation">
                                            <thead>
                                            <tr class='warning'>
                                                <th scope="col">Code</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Unit</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Qty</th>
                                                <th scope="col">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @php $total=0;$utotal=0;$ttotal=0; $products=array();@endphp
                                            @if(isset($quotation_details))
                                                @foreach($quotation_details as  $product)
                                                    <tr>
                                                        <td>{{$product->product_code}}</td>
                                                        <td>{{$product->product_name}}</td>
                                                        <td>{{$product->unit_value}}</td>
                                                        <td>{{\App\Utils::formatPrice($product->price)}}</td>
                                                        <td>{{$product->qty}}</td>
                                                        <td>{{\App\Utils::formatPrice($product->total)}}</td>
                                                    </tr>
                                                    @php $total+=$product->total;
                                                    $utotal+=$product->total_uprice;
                                                    $ttotal+=$product->total_utax;@endphp
                                                @endforeach
                                            @endif
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Product Total</b></td>
                                                <td>{{\App\Utils::formatPrice($utotal)}}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Tax</b></td>
                                                <td>{{\App\Utils::formatPrice($ttotal)}}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b><strong></strong>Total</b></td>
                                                <td><strong>{{\App\Utils::formatPrice($total)}}</strong></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-status">
                                    <br/>
                                    <!-- The time line -->
                                    <ul class="timeline">
                                        <!-- timeline time label -->
                                        <li class="time-label">
                                            <span class="bg-red"> Perfoma Invoice status</span>
                                        </li>
                                        <!-- /.timeline-label -->
                                        @if($quotation_histories)
                                            @foreach($quotation_histories as $history)
                                                <li>
                                                    <i class="fa fa-check bg-green"></i>

                                                    <div class="timeline-item">
                                                        <span class="time"><i
                                                                class="fa fa-clock-o"></i> {{$history->date_added}}</span>

                                                        <h3 class="timeline-header no-border"><a
                                                                href="#">{{$history->name}}</a>
                                                            {{$history->comment}}</h3>
                                                    </div>
                                                </li><!-- timeline item -->
                                        @endforeach
                                    @endif

                                    <!-- END timeline item -->
                                        <li>
                                            <i class="fa fa-clock-o bg-gray"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Cancel Quotation</h4>
                                    </div>
                                    <form class="form-horizontal" method="POST"
                                          action="{{url(\App\Utils::getUserUrlRoute().'/cancel-quotation')}}"
                                          accept-charset="UTF-8" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-body">
                                            <p>
                                            <div class="form-group {{ $errors->has('budget') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required">Reason</label>

                                                <div class="col-sm-7">
                                                        <textarea rows="5" class="form-control" name="reason"
                                                                  placeholder="Enter cancel reason"
                                                        ></textarea>
                                                </div>
                                                <input type="hidden" class="form-control" name="quotation_id"
                                                       value="{{(int)$page_data->quotation_id}}">


                                            </div>


                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left"
                                                    data-dismiss="modal">
                                                Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Cancel Quotation</button>
                                        </div>

                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <script type="text/javascript" src="{{ asset('exprt/jspdf.debug.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
    <script src="{{ asset('exprt/tableHTMLExport.js')}}"></script>
    <script>
        $('#expCsv').on('click',function(){
            $("#quotatation").tableHTMLExport({type:'csv',filename:'PerfomaInvoice-{{$page_data->quotation_id or '0'}}'+'.csv'});
        })
        $('#expPdf').on('click',function(){
            $("#quotatation").tableHTMLExport({type:'pdf',filename:'PerfomaInvoice-{{$page_data->quotation_id or '0'}}'+'.pdf'});
        })
    </script>
@endsection
