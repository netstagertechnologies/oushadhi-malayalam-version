@extends('layouts.user_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">


                                @include('template.user.alert')
                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                    @if($notifications)
                                        <?php  $i = ($notifications->currentPage() - 1) * $notifications->perPage();?>
                                        @foreach($notifications as $d)
                                            <?php $i++; ?>
                                            <tr class="{{(!$d->read_status)?'bg-gray disabled':''}}">
                                                <td>{{ $i }}</td>

                                                <td>{{$d->not_msg}}</td>
                                                <td><a
                                                        href="{{url(\App\Utils::getUserUrlRoute().'/notifications/' . $d->id ) }}"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-eye"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">No notifications found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $notifications->appends($_GET)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
