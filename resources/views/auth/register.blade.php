@extends('layouts.frontend_template',['page_title'=>'Sign Up'])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('index.Sign Up')</h1>
            <ul class="brdcrum">
                <li><a href="{{route('/')}}" title="@lang('header.home')">@lang('header.home')</a></li>
                <li>@lang('index.Sign Up')</li>
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">

            @include('template.frontend.alert')
            <div class="contact-main">
                <div class="contact-form">
                    <h1>@lang('index.Sign Up')</h1>
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
                        <div class="formdiv {{($errors->has('email'))?'has-error':''}}">
                            <span>[@lang('index.Sign Up') @lang('index.Email id should be your username') ]</span>
                            <input type="text" placeholder="Your Email" name="email" class="cont-input" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv-split {{($errors->has('name'))?'has-error':''}}">
                            <input type="text" placeholder="Name" name="name" class="cont-input" required>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv-split last {{($errors->has('password'))?'has-error':''}}">
                            <input type="password" placeholder="Password" name="password" class="cont-input"
                                   required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="formdiv radiobox">
                           <label for="rdo1">
                                <input type="radio" id="rdo1" value="1" name="utype" checked>
                                <span class="rdo"></span>
                                <span>@lang('index.Doctor')</span>
                            </label>

                            <label for="rdo2">
                                <input type="radio" id="rdo2" value="2" name="utype" >
                                <span class="rdo"></span>
                                <span>@lang('index.Dealer')</span>
                            </label>

                        </div>
                        <hr>
                        <div class="formdiv-split {{($errors->has('mobile'))?'has-error':''}}">
                            <input type="text" placeholder="Mobile" name="mobile" class="cont-input" required>
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv-split last {{($errors->has('phone'))?'has-error':''}}">
                            <input type="text" placeholder="Phone" name="phone" class="cont-input" required>
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv-split {{($errors->has('district'))?'has-error':''}}">
                            <input type="text" placeholder="District" name="district" class="cont-input" required>
                            @if ($errors->has('district'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('district') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv-split last {{($errors->has('state'))?'has-error':''}}">
                            <input type="text" placeholder="State" name="state" class="cont-input" required>
                            @if ($errors->has('state'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv {{($errors->has('address'))?'has-error':''}}">
                            <textarea class="contacttextariea" name="address" placeholder="Your Address"></textarea>
                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <input type="submit" value="@lang('index.Submit')" class="Cont-btn">

                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
