@extends('layouts.frontend_template',['page_title'=>'Login'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('index.Login')</h1>
            <ul class="brdcrum">
                <li><a href="{{route('/')}}" title="Home">@lang('header.home')</a></li>
                <li>@lang('index.Login')</li>
            </ul>
        </div>
    </section>

    <section class="innercontentarea">
        <div class="wid">

            @include('template.frontend.alert')
            <div class="login-main">
                <div class="login-left">
                    <div class="login-head">@lang('index.Login')</div>
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        <div class="formdiv {{($errors->has('email'))?'has-error':''}}">
                            <label>@lang('index.Username or Email')</label>

                            <input type="text" name="email" required class="log-input">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv {{($errors->has('email'))?'has-error':''}}">
                            <label>@lang('index.Password')</label>
                            <input type="password" name="password" required class="log-input">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <input type="submit" class="logbtn" value="@lang('index.Login')">
                        <span><input type="checkbox" name="remember"
                                     id="remember" {{ old('remember') ? 'checked' : '' }}>@lang('index.Remember me')</span>
                        <a href="{{ route('password.request') }}">@lang('index.Lost your password?')</a>
                    </form>
                </div>
                <div class="login-right">
                    <img src="{{asset('ui/images/contact.jpg')}}" alt="" class="fullwidth">
                </div>
            </div>
        </div>
    </section>
@endsection
