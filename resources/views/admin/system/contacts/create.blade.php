@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/contacts/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('contact_type') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Type</label>

                                        <div class="col-sm-7">
                                            <select class="form-control" name="contact_type" placeholder="Type">
                                                <option value="directors">Directors</option>
                                                <option value="officers">Officers</option>
                                            </select>
                                            {!! $errors->first('contact_type', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Name</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name" placeholder="Name">
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                     <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Name For Malayalam</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name_ml" placeholder="Name Malayalam">
                                            {!! $errors->first('name_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('designation') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Designation</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="designation" placeholder="Designation">
                                            {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                     <div class="form-group {{ $errors->has('designation') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Designation For Malayalam</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="designation_ml" placeholder="Designation In Malayalam">
                                            {!! $errors->first('designation_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('mob_no') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label ">Mobile Number</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="mob_no" placeholder="Mobile Number">
                                            {!! $errors->first('mob_no', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Residence No</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="res_no" placeholder="Residence No">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="email" placeholder="Email">
                                        </div>
                                    </div>

                                    <div id="banner-image">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Image</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="upload-file" data-height="97" data-width="91"
                                                       name="image"/>
                                                <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                                        .jpeg,.png</i></p>
                                                <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                        [283*284]
                                                        pixel</i></p>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Sort Order</label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" name="sort_order" placeholder="Sort Order">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/contacts') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>


@endsection
