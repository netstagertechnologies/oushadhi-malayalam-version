@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/product/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-general" data-toggle="tab">{{ 'General' }}</a>
                                        </li>
                                        <li><a href="#tab-data" data-toggle="tab">{{ 'Data' }}</a></li>
                                        <li><a href="#tab-price" data-toggle="tab">{{ 'Price Details' }}</a></li>
                                        <li><a href="#tab-images" data-toggle="tab">{{ 'Images' }}</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab-general">
                                            <br/>
                                            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required">Title</label>

                                                {{ csrf_field() }}
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="title"
                                                           placeholder="Title" value="{{$page_data->title or ''}}">
                                                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('title_ml') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required">Title For Malayalam</label>

                                                {{ csrf_field() }}
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="title_ml"
                                                           placeholder="Title For Malayalam" value="{{$page_data->title_ml or ''}}">
                                                    {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                                <label for="content-label required"
                                                       class="col-sm-3 control-label">Description</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="5" class="form-control" placeholder="Enter ..."
                                                              name="description">{{$page_data->description or ''}}</textarea>
                                                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('description_ml') ? 'has-error' : ''}}">
                                                <label for="content-label required"
                                                       class="col-sm-3 control-label">Description For Malayalam</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="5" class="form-control" placeholder="Enter malayalam ..."
                                                              name="description_ml">{{$page_data->description_ml or ''}}</textarea>
                                                    {!! $errors->first('description_ml', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}} ">
                                                <label
                                                    class="col-md-3 control-label required ">Meta Title</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="meta_title"
                                                           placeholder="Meta Title"
                                                           value="{{$page_data->meta_title or ''}}">
                                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="content-label required"
                                                       class="col-sm-3 control-label">Meta Description</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="5" class="form-control" placeholder="Enter ..."
                                                              name="meta_description">{{$page_data->meta_description or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Keywords</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="keywords"
                                                           placeholder="Keywords"
                                                           value="{{$page_data->keywords or ''}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label required">Status</label>

                                                <div class="col-sm-7">
                                                    <?php $status = (isset($page_data->status)) ? $page_data->status : 0;?>
                                                    <select type="text" class="form-control"
                                                            name="status">
                                                        <option value="1" {{($status)? 'selected':''}}>Enable</option>
                                                        <option value="0" {{(!$status)? 'selected':''}}>Disable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-data">
                                            <br/>
                                            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required"
                                                       for="input-category"><span
                                                        data-toggle="tooltip"
                                                        title="{{ 'help category' }}">{{ 'Category' }}</span></label>
                                                <div class="col-sm-7">
                                                    <?php $category_s = (isset($page_data->category_id)) ? $page_data->category_id : 0;?>

                                                    <select name="category_id" id="product-input-category"
                                                            class="form-control" style="width: 100%"
                                                            required>
                                                        <option value="">--Select--</option>
                                                        @if(isset($categories))
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->category_id}}"
                                                                    {{($category_s==$category->category_id)?'selected':''}}>{{$category->title}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>


                                            <div
                                                class="form-group  {{ isset($error['Product Code']) ? 'has-error' : '' }}">
                                                <label for="title"
                                                       class="col-md-3 required control-label">{{ 'Model' }}</label>
                                                <div class="col-md-7">
                                                    <input class="form-control" name="model" type="text"
                                                           value="{{$page_data->model or ''}}"/>
                                                    <?php echo isset($error['model']) ? '<p class="help-block">' . $error['model'] . '</p>' : '';?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Featured</label>
                                                <div class="col-sm-4">
                                                    <?php $featured = (isset($page_data->featured)) ? $page_data->featured : 0;?>

                                                        <select name="featured" id="input-status" class="form-control">
                                                            <option value="0" {{(!$featured)?'selected':''}}>No</option>
                                                            <option value="1" {{($featured)?'selected':''}}>Yes</option>
                                                        </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="checkbox" id="show_home" value="1" @if($page_data->show_home) checked @endif name="show_home">
                                                    <label for="show_home">Show In Home</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Total Ingredients</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="total_ingradients"
                                                           placeholder="Total Ingradients"
                                                           value="{{$page_data->total_ingradients or ''}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Main Ingredients</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="main_ingradients"
                                                              placeholder="Main Ingradients">{{$page_data->main_ingradients or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Main Ingredients For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="main_ingradients_ml"
                                                              placeholder="Main Ingradients">{{$page_data->main_ingradients_ml or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Indication</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="indication"
                                                              placeholder="Indication">{{$page_data->indication or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Indication For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="indication_ml"
                                                              placeholder="Indication For Malayalam">{{$page_data->indication_ml or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Special Indication</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="special_indication"
                                                              placeholder="Special Indication">{{$page_data->special_indication or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Special Indication For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="special_indication_ml"
                                                              placeholder="Special Indication For Malayalam">{{$page_data->special_indication_ml or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Usage</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="p_usage"
                                                              placeholder="Usage">{{$page_data->p_usage or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Usage For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="p_usage_ml"
                                                              placeholder="Usage For Malayalam">{{$page_data->p_usage_ml or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Dosage</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="dosage"
                                                              placeholder="Dosage">{{$page_data->dosage or ''}}</textarea>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">Dosage For Malayalam</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="dosage_ml"
                                                              placeholder="Dosage For Malayalam">{{$page_data->dosage_ml or ''}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Presentation</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="presentation"
                                                           placeholder="Presentation"
                                                           value="{{$page_data->presentation or ''}}"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Rating</label>

                                                <div class="col-sm-7">
                                                    <input type="number" min="1" max="5" class="form-control"
                                                           name="rating"
                                                           value="5" placeholder="rating"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-price">
                                            <br/>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Tax</label>
                                                @php $sel_tax=isset($page_data->tax)?(int)$page_data->tax:0;@endphp
                                                <div class="col-sm-7">
                                                    <select class="form-control" name="tax">
                                                        @foreach($taxes as $tax)
                                                            <option
                                                                value="{{$tax['tax_id']}}" {{($sel_tax==$tax['tax_id'])?'selected':''}}>{{$tax['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <table id="prices"
                                                   class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="text-left">{{ 'Unit' }}</td>
                                                    <td class="text-left">{{ 'Price' }}</td>
                                                    <td class="text-left">{{ 'Dealer Price' }}</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php $price_row = 0; @endphp
                                                @if(isset($product_prices))
                                                    @foreach($product_prices as $product_price)
                                                        <tr id="prices-row{{ $price_row }}">
                                                            <td class="text-left" style="width: 40%;">
                                                                <input type="hidden"
                                                                       name="product_price[{{ $price_row }}][price_id]"
                                                                       value="{{ $product_price->price_id or ''}}"
                                                                       placeholder="Unit" class="form-control"/>
                                                                <input type="text"
                                                                       name="product_price[{{ $price_row }}][unit]"
                                                                       value="{{ $product_price->unit_value or ''}}"
                                                                       placeholder="Unit" class="form-control"/>
                                                            </td>
                                                            <td class="text-left">
                                                                <input type="text"
                                                                       name="product_price[{{ $price_row }}][price]"
                                                                       value="{{ $product_price->price or ''}}"
                                                                       placeholder="Price" class="form-control"/>
                                                            </td>
                                                            <td class="text-left">
                                                                <input type="text"
                                                                       name="product_price[{{ $price_row }}][dealer_price]"
                                                                       value="{{ $product_price->dealer_price or ''}}"
                                                                       placeholder="Price" class="form-control"/>
                                                            </td>
                                                            <td class="text-right">
                                                                <button type="button"
                                                                        onclick="$('#prices-row{{ $price_row }}').remove();"
                                                                        data-toggle="tooltip" title="Remove"
                                                                        class="btn btn-danger"><i
                                                                        class="fa fa-minus-circle"></i></button>
                                                            </td>
                                                        </tr>
                                                        @php $price_row = $price_row + 1; @endphp
                                                    @endforeach
                                                @endif
                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td class="text-right">
                                                        <button type="button" onclick="addPrices();"
                                                                data-toggle="tooltip" title="Add Price"
                                                                class="btn btn-primary"><i
                                                                class="fa fa-plus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="tab-images">
                                            <br/>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Main Image</label>
                                                <div class="col-sm-7">
                                                    <input type="file" class="upload-file" data-height="1921"
                                                           data-width="186"
                                                           name="image"/>
                                                    <p> * <b>Image format</b> - <i class="text-light-blue">allowed image
                                                            format
                                                            .jpeg,.png</i></p>
                                                    <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image
                                                            size
                                                            [270*269]
                                                            pixel</i></p>

                                                    @if(isset($page_data->image)&&!empty($page_data->image))
                                                        <input type="hidden" name="image_src"
                                                               value="{{$page_data->image}}"/>
                                                        <img
                                                            src="{{ asset('uploads/product/'.$page_data->image)}}"
                                                            data-placeholder="Image" width="150"/>
                                                    @endif
                                                </div>
                                            </div>
                                            <h4>Additional Images</h4>
                                            <hr/>
                                            <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                                    .jpeg,.png</i></p>
                                            <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                    [270*269] pixel</i></p>

                                            <table id="additional_images"
                                                   class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="text-left">{{ 'Image' }}</td>
                                                    <td class="text-left">{{ 'Order' }}</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php $additional_image_row = 0; @endphp
                                                @if(isset($additional_images))
                                                    @foreach($additional_images as $additional_image)
                                                        <tr id="additional-image-row{{ $additional_image_row }}">
                                                            <td class="text-left">
                                                                <input type="hidden"
                                                                       name="additional_image[{{ $additional_image_row }}][additional_image_id]"
                                                                       value="{{ $additional_image->additional_image_id or ''}}"
                                                                       class="form-control"/>
                                                                <input type="file"
                                                                       name="additional_image[{{ $additional_image_row }}][name]"
                                                                       placeholder="Choose File" class="form-control"/>
                                                                <input type="hidden"
                                                                       name="additional_image[{{ $additional_image_row }}][image]"
                                                                       value="{{ $additional_image->image or ''}}"
                                                                       class="form-control"/>
                                                                <img
                                                                    src="{{asset('uploads/product/'.$additional_image->image)}}"
                                                                    height="100"/>

                                                            </td>
                                                            <td class="text-left">
                                                                <input type="text"
                                                                       name="additional_image[{{ $additional_image_row }}][sort_order]"
                                                                       value="{{ $additional_image->sort_order or '#'}}"
                                                                       placeholder="Order" class="form-control"/>
                                                            </td>
                                                            <td class="text-right">
                                                                <button type="button"
                                                                        onclick="$('#additional-image-row{{ $additional_image_row }}').remove();"
                                                                        data-toggle="tooltip" title="Remove"
                                                                        class="btn btn-danger"><i
                                                                        class="fa fa-minus-circle"></i></button>
                                                            </td>
                                                        </tr> @php $additional_image_row = $additional_image_row + 1; @endphp
                                                    @endforeach
                                                @endif
                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td class="text-right">
                                                        <button type="button" onclick="addAdditionalImage();"
                                                                data-toggle="tooltip" title="Add Image"
                                                                class="btn btn-primary"><i
                                                                class="fa fa-plus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Save Changes</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/product') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        var price_row = '{{ $price_row }}';

        function addPrices() {
            html = '<tr id="prices-row' + price_row + '">';
            html += '  <td class="text-left" style="width: 20%;"><input type="text" name="product_price[' + price_row + '][unit]" value="" placeholder="{{ 'Unit' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="text" name="product_price[' + price_row + '][price]" value="" placeholder="{{ 'Price' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="text" name="product_price[' + price_row + '][dealer_price]" value="" placeholder="{{ 'Dealer Price' }}" class="form-control" required /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#prices-row' + price_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#prices tbody').append(html);

            price_row++;
        }

        var additional_image_row = '{{ $additional_image_row }}';

        function addAdditionalImage() {
            html = '<tr id="additional-image-row' + additional_image_row + '">';
            html += '  <td class="text-left"><input type="file" name="additional_image[' + additional_image_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control"/></td>';
            html += '  <td class="text-left"><input type="text" name="additional_image[' + additional_image_row + '][sort_order]" value="" placeholder="{{ 'Order' }}" class="form-control"/></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#additional-image-row' + additional_image_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#additional_images tbody').append(html);

            additional_image_row++;
        }

    </script>
@endsection
