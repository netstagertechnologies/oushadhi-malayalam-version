@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/product/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-general" data-toggle="tab">{{ 'General' }}</a>
                                        </li>
                                        <li><a href="#tab-data" data-toggle="tab">{{ 'Data' }}</a></li>
                                        <li><a href="#tab-price" data-toggle="tab">{{ 'Price Details' }}</a></li>
                                        <li><a href="#tab-images" data-toggle="tab">{{ 'Images' }}</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab-general">
                                            <br/>
                                            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required">Title</label>

                                                {{ csrf_field() }}
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="title"
                                                           placeholder="Title">
                                                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                                </div>
                                                 <div class="form-group {{ $errors->has('title_ml') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required">Title For Malayalam</label>
                                                 <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="title_ml"
                                                           placeholder="Title For Malayalam">
                                                    {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                                <label for="content-label required"
                                                       class="col-sm-3 control-label">Description</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="5" class="form-control" placeholder="Enter ..."
                                                              name="description"></textarea>
                                                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                                <label for="content-label required"
                                                       class="col-sm-3 control-label">Description For Malayalam</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="5" class="form-control" placeholder="Enter Malayalam  ..."
                                                              name="description_ml"></textarea>
                                                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}} ">
                                                <label
                                                    class="col-md-3 control-label required ">Meta Title</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="meta_title"
                                                           placeholder="Meta Title">
                                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="content-label required"
                                                       class="col-sm-3 control-label">Meta Description</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="5" class="form-control" placeholder="Enter ..."
                                                              name="meta_description"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Keywords</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="keywords"
                                                           placeholder="Keywords">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label required">Status</label>

                                                <div class="col-sm-7">
                                                    <select type="text" class="form-control"
                                                            name="status">
                                                        <option value="1">Enable</option>
                                                        <option value="0">Disable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-data">
                                            <br/>
                                            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required"
                                                       for="input-category"><span
                                                        data-toggle="tooltip"
                                                        title="{{ 'help category' }}">{{ 'Category' }}</span></label>
                                                <div class="col-sm-7">
                                                    <select name="category_id" id="product-input-category"
                                                            class="form-control" style="width: 100%"
                                                    >
                                                        <option value="">--Select--</option>
                                                        @if(isset($categories))
                                                            @foreach($categories as $category)
                                                                <option
                                                                    value="{{$category->category_id}}">{{$category->title}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>
                                            <div
                                                class="form-group  {{ isset($error['model']) ? 'has-error' : '' }}">
                                                <label for="title"
                                                       class="col-md-3 required control-label">{{ 'Product Code' }}</label>
                                                <div class="col-md-7">
                                                    <input class="form-control" name="model" type="text"
                                                           value=""/>
                                                    <?php echo isset($error['model']) ? '<p class="help-block">' . $error['model'] . '</p>' : '';?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Featured</label>
                                                <div class="col-sm-4">

                                                        <select name="featured" id="input-status" class="form-control">
                                                            <option value="0">No</option>
                                                            <option value="1">Yes</option>
                                                        </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="checkbox" id="show_home" value="1" name="show_home">
                                                    <label for="show_home">Show In Home</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Total Ingredients</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="total_ingradients"
                                                           placeholder="Total Ingradients">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Main Ingredients</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="main_ingradients"
                                                              placeholder="Main Ingradients"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Main Ingredients For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="main_ingradients_ml"
                                                              placeholder="Main Ingradients Malayalam"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Indication</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="indication"
                                                              placeholder="Indication"></textarea>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">Indication For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="indication_ml"
                                                              placeholder="Indication For Malayalam"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Special Indication</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="special_indication"
                                                              placeholder="Special Indication"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Special Indication For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="special_indication_ml"
                                                              placeholder="Special Indication Malayalam"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Usage</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="p_usage"
                                                              placeholder="Usage"></textarea>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">Usage For Malayalam</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="p_usage_ml"
                                                              placeholder="Usage For Malayalam"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Dosage</label>

                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="dosage"
                                                              placeholder="Dosage"></textarea>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">Dosage For Malayalam</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="3" class="form-control" name="dosage_ml"
                                                              placeholder="Dosage For Malayalam">{{$page_data->dosage_ml or ''}}</textarea>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Presentation</label>

                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="presentation"
                                                           placeholder="Presentation"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Rating</label>

                                                <div class="col-sm-7">
                                                    <input type="number" min="1" max="5" class="form-control"
                                                           name="rating"
                                                           value="5" placeholder="rating"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-price">
                                            <br/>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Tax</label>

                                                <div class="col-sm-7">
                                                    <select class="form-control" name="tax">
                                                        @foreach($taxes as $tax)
                                                            <option value="{{$tax['tax_id']}}">{{$tax['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <table id="prices"
                                                   class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="text-left">{{ 'Unit' }}</td>
                                                    <td class="text-left">{{ 'Price' }}</td>
                                                    <td class="text-left">{{ 'Dealer Price' }}</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php $price_row = 0; @endphp

                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td class="text-right">
                                                        <button type="button" onclick="addPrices();"
                                                                data-toggle="tooltip" title="Add Price"
                                                                class="btn btn-primary"><i
                                                                class="fa fa-plus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="tab-images">
                                            <br/>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Main Image</label>
                                                <div class="col-sm-8">
                                                    <input type="file" class="upload-file" data-height="1921"
                                                           data-width="186"
                                                           name="image"/>
                                                    <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image
                                                            format
                                                            .jpeg,.png</i></p>
                                                    <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image
                                                            size [270*269] pixel</i></p>

                                                </div>
                                            </div>
                                            <h4>Additional Images</h4>
                                            <hr/>
                                            <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                                    .jpeg,.png</i></p>
                                            <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                    [270*269] pixel</i></p>

                                            <table id="additional_images"
                                                   class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="text-left">{{ 'Image' }}</td>
                                                    <td class="text-left">{{ 'Order' }}</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php $additional_image_row = 0; @endphp
                                                @if(isset($additional_images))
                                                    @foreach($additional_images as $additional_image)
                                                        <tr id="additional-image-row{{ $additional_image_row }}">
                                                            <td class="text-left">
                                                                <input type="hidden"
                                                                       name="additional_image[{{ $additional_image_row }}][additional_image_id]"
                                                                       value="{{ $additional_image->additional_image_id or ''}}"
                                                                       class="form-control"/>
                                                                <input type="file"
                                                                       name="additional_image[{{ $additional_image_row }}][name]"
                                                                       placeholder="Choose File" class="form-control"/>
                                                                <input type="hidden"
                                                                       name="additional_image[{{ $additional_image_row }}][image]"
                                                                       value="{{ $additional_image->image or ''}}"
                                                                       class="form-control"/>
                                                                <img
                                                                    src="{{asset('uploads/banner/'.$additional_image->image)}}"
                                                                    height="100"/>

                                                            </td>
                                                            <td class="text-left">
                                                                <input type="text"
                                                                       name="additional_image[{{ $additional_image_row }}][sort_order]"
                                                                       value="{{ $additional_image->sort_order or '#'}}"
                                                                       placeholder="Order" class="form-control"/>
                                                            </td>
                                                            <td class="text-right">
                                                                <button type="button"
                                                                        onclick="$('#additional-image-row{{ $additional_image_row }}').remove();"
                                                                        data-toggle="tooltip" title="Remove"
                                                                        class="btn btn-danger"><i
                                                                        class="fa fa-minus-circle"></i></button>
                                                            </td>
                                                        </tr> @php $additional_image_row = $additional_image_row + 1; @endphp
                                                    @endforeach
                                                @endif
                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td class="text-right">
                                                        <button type="button" onclick="addAdditionalImage();"
                                                                data-toggle="tooltip" title="Add Image"
                                                                class="btn btn-primary"><i
                                                                class="fa fa-plus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/product') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        var price_row = '{{ $price_row }}';

        function addPrices() {
            html = '<tr id="prices-row' + price_row + '">';
            html += '  <td class="text-left" style="width: 20%;"><input type="text" name="product_price[' + price_row + '][unit]" value="" placeholder="{{ 'Unit' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="text" name="product_price[' + price_row + '][price]" value="" placeholder="{{ 'Price' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="text" name="product_price[' + price_row + '][dealer_price]" value="" placeholder="{{ 'Dealer Price' }}" class="form-control" required /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#prices-row' + price_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#prices tbody').append(html);

            price_row++;
        }

        var additional_image_row = '{{ $additional_image_row }}';

        function addAdditionalImage() {
            html = '<tr id="additional-image-row' + additional_image_row + '">';
            html += '  <td class="text-left"><input type="file" name="additional_image[' + additional_image_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control"/></td>';
            html += '  <td class="text-left"><input type="text" name="additional_image[' + additional_image_row + '][sort_order]" value="" placeholder="{{ 'Order' }}" class="form-control"/></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#additional-image-row' + additional_image_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#additional_images tbody').append(html);

            additional_image_row++;
        }


    </script>
@endsection
