@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/careers/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">

                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('title_ml') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title For Malayalam</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title_ml" placeholder="Title For Malayalam">
                                            {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div
                                        class="form-group  {{ ($errors->has('last_date')||$errors->has('last_time')) ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required" for="input-category"><span
                                                data-toggle="tooltip">@lang('tender.Last date & Time')</span></label>

                                        <div class="col-sm-7">
                                            <div class="col-sm-8">
                                                <div class="input-group date">

                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" name="last_date"
                                                           class="form-control pull-right"
                                                           id="lastdatepicker">

                                                    {!! $errors->first('last_date', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('dept') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Department</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="dept" placeholder="Title">
                                            {!! $errors->first('dept', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('dept_ml') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Department For Malayalam</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="dept_ml" placeholder="Department For Malayalam">
                                            {!! $errors->first('dept', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label ">Contact Email</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact_email"
                                                   placeholder="Contact Email">
                                            {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label ">Contact No.</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact"
                                                   placeholder="Contact No">
                                            {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="normal-tender-flds">

                                        <div id="tender-files">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Files</label>
                                                <div class="col-sm-7">
                                                    <table id="careers"
                                                           class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <td class="text-left">{{ 'Title' }}</td>
                                                            <td class="text-left">{{ 'Title Malayalam' }}</td>
                                                            <td class="text-left">{{ 'File' }}</td>
                                                            <td></td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @php $tender_row = 0; @endphp

                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td class="text-right">
                                                                <button type="button" onclick="addCareerFileRow();"
                                                                        data-toggle="tooltip" title="Add File"
                                                                        class="btn btn-primary"><i
                                                                        class="fa fa-plus-circle"></i></button>
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/careers') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script><!-- date-range-picker -->
    <script src="{{ asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script
        src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>

        $('#lastdatepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        var tender_row = '{{ $tender_row }}';

        function addCareerFileRow() {
            html = '<tr id="career-row' + tender_row + '">';
            html += '  <td class="text-left" style="width: 30%;"><input type="text" name="career_file[' + tender_row + '][title]" value="" placeholder="{{ 'Title' }}" class="form-control" required /></td>';
            html += '  <td class="text-left" style="width: 30%;"><input type="text" name="career_file[' + tender_row + '][title_ml]" value="" placeholder="{{ 'Title For Malayalam' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="file" name="career_file[' + tender_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control" accept="application/pdf,application/vnd.ms-excel"  /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#career-row' + tender_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#careers tbody').append(html);

            tender_row++;
        }

    </script>
@endsection
