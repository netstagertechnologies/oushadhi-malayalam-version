@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/careers/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title For Malayalam</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title_ml" placeholder="Title For Malayalam"
                                                   value="{{$page_data->title_ml or ''}}">
                                            {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div
                                        class="form-group  {{ ($errors->has('last_date')||$errors->has('last_time')) ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required" for="input-category"><span
                                                data-toggle="tooltip">Last Date and Time</span></label>

                                        <div class="col-sm-7">
                                            <div class="col-sm-6">
                                                <div class="input-group date">

                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" name="last_date"
                                                           class="form-control pull-right"
                                                           id="lastdatepicker"
                                                           value="{{$page_data->last_date or ''}}">

                                                    {!! $errors->first('last_date', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('dept') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Department</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="dept"
                                                   placeholder="Department"
                                                   value="{{$page_data->dept or ''}}">
                                            {!! $errors->first('dept', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('dept_ml') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Department For Malayalam</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="dept_ml"
                                                   placeholder="Department For Malayalam"
                                                   value="{{$page_data->dept_ml or ''}}">
                                            {!! $errors->first('dept_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label ">Contact Email</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact_email"
                                                   placeholder="Contact Email"  value="{{$page_data->contact_email or ''}}">
                                            {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Contact</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact"
                                                   placeholder="Contact"
                                                   value="{{$page_data->contact or ''}}">
                                            {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="normal-tender-flds">
                                        <div id="tender-files">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Files</label>
                                                <div class="col-sm-7">
                                                    <table id="careers"
                                                           class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <td class="text-left">{{ 'Title' }}</td>
                                                            <td class="text-left">{{ 'Title For Malayalam' }}</td>
                                                            <td class="text-left">{{ 'File' }}</td>
                                                            <td></td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @php $tender_row = 0; @endphp
                                                        @if(isset($career_files))
                                                            @foreach($career_files as $career_file)
                                                                <tr id="career-row{{ $tender_row }}">
                                                                    <td class="text-left" style="width: 40%;">
                                                                        <input type="hidden"
                                                                               name="career_file[{{ $tender_row }}][file_id]"
                                                                               value="{{ $career_file->file_id or ''}}"
                                                                               placeholder="Title"
                                                                               class="form-control"/>
                                                                        <input type="text"
                                                                               name="career_file[{{ $tender_row }}][title]"
                                                                               value="{{ $career_file->title or ''}}"
                                                                               placeholder="Unit" class="form-control"/>
                                                                    </td>
                                                                    <td class="text-left">
                                                                        <a href="{{asset('uploads/careers/'.$career_file->file_link)}}">Selected
                                                                            file: {{ $career_file->file_link or ''}}</a>
                                                                        <input type="file"
                                                                               name="career_file[{{ $tender_row }}][name]"
                                                                               value="{{ $career_file->title or ''}}"
                                                                               placeholder="Choose File"
                                                                               class="form-control"/>
                                                                        <input type="hidden"
                                                                               name="career_file[{{ $tender_row }}][file_link]"
                                                                               value="{{ $career_file->file_link or ''}}"
                                                                               class="form-control"/>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <button type="button"
                                                                                onclick="$('#career-row{{ $tender_row }}').remove();"
                                                                                data-toggle="tooltip" title="Remove"
                                                                                class="btn btn-danger"><i
                                                                                class="fa fa-minus-circle"></i></button>
                                                                    </td>
                                                                </tr> @php $tender_row = $tender_row + 1; @endphp
                                                            @endforeach
                                                        @endif
                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td class="text-right">
                                                                <button type="button" onclick="addTenderFileRow();"
                                                                        data-toggle="tooltip" title="Add File"
                                                                        class="btn btn-primary"><i
                                                                        class="fa fa-plus-circle"></i></button>
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/careers') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script><!-- date-range-picker -->
    <script src="{{ asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script
        src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>

        $('#lastdatepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        var tender_row = '{{ $tender_row }}';

        function addTenderFileRow() {
            html = '<tr id="career-row' + tender_row + '">';
            html += '  <td class="text-left" style="width: 30%;"><input type="text" name="career_file[' + tender_row + '][title]" value="" placeholder="{{ 'Title' }}" class="form-control" required /></td>';
            html += '  <td class="text-left" style="width: 30%;"><input type="text" name="career_file[' + tender_row + '][title_ml]" value="" placeholder="{{ 'Title For Malayalam' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="file" name="career_file[' + tender_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control" accept="application/pdf,application/vnd.ms-excel"  /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#career-row' + tender_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#careers tbody').append(html);

            tender_row++;
        }


    </script>
@endsection
