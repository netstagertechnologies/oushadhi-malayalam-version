@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/addresses/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('title_ml') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title For Malayalam</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title_ml" placeholder="Title For Malayalam"
                                                   value="{{$page_data->title_ml or ''}}">
                                            {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                        <label for="content-label required"
                                               class="col-sm-3 control-label">Address</label>
                                        <div class="col-sm-7">
                                            <textarea rows="10" cols="80" class="form-control" placeholder="Enter ..."
                                                      name="address">{{$page_data->address or ''}}</textarea>
                                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('address_ml') ? 'has-error' : ''}}">
                                        <label for="content-label required"
                                               class="col-sm-3 control-label">Address For Malayalam</label>
                                        <div class="col-sm-7">
                                            <textarea rows="10" cols="80" class="form-control" placeholder="Enter ..."
                                                      name="address_ml">{{$page_data->address_ml or ''}}</textarea>
                                            {!! $errors->first('address_ml', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Mobile</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="mobile"  value="{{$page_data->mobile}}" placeholder="Mobile">
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Phone</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="phone"  value="{{$page_data->phone or ''}}" placeholder="Phone">
                                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="email"  value="{{$page_data->email or ''}}" placeholder="Email">
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div id="banner-image">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"> Image</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="upload-file" data-height="312" data-width="555"
                                                       name="image"/>
                                                <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                                        .jpeg,.png</i></p>
                                                <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                        [385*192]
                                                        pixel</i></p>

                                                @if(isset($page_data->image)&&!empty($page_data->image))
                                                    <input type="hidden" name="bnr_src" value="{{$page_data->image}}"/>
                                                    <img src="{{ asset('uploads/address/'.$page_data->image)}}"
                                                         data-placeholder="Image" width="186"/>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Map Link</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="map_link"  value="{{$page_data->map_link}}" placeholder="Map Link">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Sort Order</label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" name="sort_order" value="{{$page_data->sort_order or ''}}" placeholder="Sort Order">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Save Changes</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/addresses') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection
