@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/testimonials/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('title_ml') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Title For Malayalam</label>
                                            <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title_ml" placeholder="Title For Malayalam">
                                            {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                        <label for="content-label required"
                                               class="col-sm-3 control-label">Content</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" class="form-control" placeholder="Enter ..." name="description"></textarea>
                                            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                        <label for="content-label required"
                                               class="col-sm-3 control-label">Content For Malayalam</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" class="form-control" placeholder="Enter ..."  name="description_ml"></textarea>
                                            {!! $errors->first('description_ml', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}} ">
                                        <label
                                            class="col-md-3 control-label required ">Name</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name" placeholder="name">
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                     <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}} ">
                                        <label
                                            class="col-md-3 control-label">Name For Malayalam</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name_ml" placeholder="name_ml" >
                                            {!! $errors->first('name_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Designation</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="designation"  placeholder="Designation">

                                        </div>
                                    </div>
                                        <div class="form-group">
                                    <label class="col-sm-3 control-label">Designation For Malayalam</label>

                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="designation_ml"placeholder="Designation For Malayalam">

                                    </div>
                                </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/testimonials') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
