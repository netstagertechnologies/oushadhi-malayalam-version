@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/departments/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title For Malayalam</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title_ml" placeholder="Title"
                                                   value="{{$page_data->title_ml or ''}}">
                                            {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('page_content') ? 'has-error' : ''}}">
                                        <label for="content" class="col-md-3 control-label">{{ 'Content *' }}</label>
                                        <div class="col-md-7">
                                            <textarea id="editor1" rows="10" cols="80"
                                                      name="page_content">{{ $page_data->page_content or ''}}</textarea>
                                            {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                     <div class="form-group {{ $errors->has('page_content') ? 'has-error' : ''}}">
                                        <label for="content" class="col-md-3 control-label">{{ 'Content For Malayalam *' }}</label>
                                        <div class="col-md-7">
                                            <textarea id="editor2" rows="10" cols="80"
                                                      name="page_content_ml">{{ $page_data->page_content_ml or ''}}</textarea>
                                            {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>



                                <div class="form-group">
                                    <label class="col-sm-3 control-label required">Status</label>

                                    <div class="col-sm-7">
                                        <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                        <select type="text" class="form-control"
                                                name="status">
                                            <option value="1" {{$status?'selected':''}}>Enable</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/departments') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            CKEDITOR.replace('editor2')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection
