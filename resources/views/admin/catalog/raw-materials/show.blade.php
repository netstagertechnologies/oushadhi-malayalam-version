@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">


                    @include('template.admin.alert')
                    <div class="panel panel-default">
                        <div class="panel-body">


                            <div class="box box-solid">
                                <div class="box-header with-border bg-aqua">
                                    <h3 class="box-title">Raw Material Required details</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <p>Title(malayalam): {{$page_data->title}}</p>
                                    <p>Title(english): {{$page_data->title_en}}</p>
                                    <p>Title(hindi): {{$page_data->title_hn}}</p>
                                    <div class="table-responsive">
                                        <table class="table no-margin">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Botanical Name</th>
                                                <th>Size</th>
                                                <th>Quantity</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <?php $image = ($page_data->image) ? $page_data->image : 'placeholder.jpg';?>

                                                <td><img src="{{ asset('uploads/raw-materials/'.$image)}}" height="100">
                                                </td>
                                                <td>{{$page_data->botanical_name}}</td>
                                                <td>{{$page_data->size}}</td>
                                                    <td>{{$page_data->quantity}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <br/>
                            <div class="box box-solid">
                                <div class="box-header with-border bg-green">
                                    <h3 class="box-title">Enquiry details</h3>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive col-md-12">

                                        <table class="table table-borderless">

                                            <tr>
                                                <th>Id</th>
                                                <th style="width:30%">Message</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                            </tr>
                                            @if(!$enquiry_data->isEmpty())
                                                <?php  $i = ($enquiry_data->currentPage() - 1) * $enquiry_data->perPage();?>
                                                @foreach($enquiry_data as $d)
                                                    <?php $i++; ?>
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>{{$d->message}}</td>
                                                        <td>{{ $d->name }}</td>
                                                        <td>{{ $d->email }}</td>
                                                        <td>{{ $d->phone }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7">No records found</td>
                                                </tr>
                                            @endif


                                        </table>


                                        {{ $enquiry_data->appends($_GET)->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
