@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">Options Managing</div>
                        <div class="panel-body">
                            <a href="{{ url(\App\Utils::getUrlRoute().'/user-role/create') }}"
                               class="btn btn-success btn-sm" title="Add New  Option">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">
                                @include('template.admin.alert')
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Sl No.</th>
                                        <th>Role Name</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(!$role_data->isEmpty())
                                        <?php  $i = ($role_data->currentPage() - 1) * $role_data->perPage();?>
                                        @foreach($role_data as $role)

                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $role->role_name }}</td>
                                                <td>
                                                    <a href="{{url(\App\Utils::getUrlRoute().'/user-role/' . $role->id . '/edit') }}"
                                                       class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>

                                                    <form method="POST" action="{{ url(\App\Utils::getUrlRoute().'/user-role/' . $role->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger" title="Delete User Role" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">No records found</td>
                                        </tr>
                                    @endif
                                </table>

                                {{ $role_data->appends($_GET)->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
