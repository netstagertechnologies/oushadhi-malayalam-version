@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle}}</div>
                        <div class="panel-body">
                            <a href="{{ url(\App\Utils::getUrlRoute().'/users/create') }}"
                               class="btn btn-success btn-sm" title="Add New User">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>


                            <div class="col-md-12">
                                <form method="GET" action="{{ url(\App\Utils::getUrlRoute().'/users') }}"
                                      accept-charset="UTF-8" class="navbar-form navbar-right" role="search">
                                    <div class="row">


                                        <div class="form-group ">
                                            <div class="col-md-3">
                                                <input type="text" class="form-control"
                                                       value="{{Request::get('search')}}" name="search"
                                                       placeholder="Search Name">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url(\App\Utils::getUrlRoute().'/users') }}"
                                               class="btn  btn-warning">Clear</a>

                                        </div>
                                    </div>
                                </form>
                            </div>

                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">
                                @include('template.admin.alert')
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Sl No.</th>
                                        <th>User Name</th>
                                        <th>Role Name</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(!$users->isEmpty())
                                        <?php  $i = ($users->currentPage() - 1) * $users->perPage();?>
                                        @foreach($users as $user)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>@foreach($roles as $role)
                                                        @if($role->id==$user->user_role)
                                                            {{$role->role_name}}
                                                        @endif
                                                    @endforeach
                                                </td>

                                                <td>
                                                    <a href="{{url(\App\Utils::getUrlRoute().'/users/' . $user->id . '/edit') }}"
                                                       class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>

                                                    <form
                                                        action="{{url(\App\Utils::getUrlRoute().'/users/'.$user->id) }}"
                                                        method="post" onsubmit="return check()" accept-charset="UTF-8"
                                                        style="display:inline">
                                                        <input type="hidden" value="{{$user->id}}">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button class="btn btn-danger"><i
                                                                class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No records found</td>
                                        </tr>
                                    @endif
                                </table>

                                {{ $users->appends($_GET)->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function changeStatus(id) {
                    $.ajax({
                        url: "{{ route('user.change_status') }}",
                        type: "GET",
                        data: {user: id},
                        dataType: "json",
                        success: function (data) {
                            if (data == 1) {
                                alert('Status Changed...');
                            }
                        }
                    });
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
