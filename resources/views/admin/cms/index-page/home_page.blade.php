@extends('layouts.backend_template')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>
        </section>

        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <div class="col-sm-12">
                                <div class="box-header  pull-left">
                                    <h2 class="box-title">{{$page_subtitle_desc}}</h2>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" form="form-banner" data-toggle="tooltip" title="Save Changes"
                                            class="btn btn-primary" data-original-title="Save"><i
                                            class="fa fa-save"></i></button>

                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <form class="form-horizontal" method="POST" id="form-banner"
                                  action="{{url(\App\Utils::getUrlRoute().'/home-cms/1')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                <div class="box-body">
                                    @include('template.admin.alert')
                                    @foreach($cms as $row)
                                        @if($row -> h_section == 1)
                                            <h4>Section One</h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Caption</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="caption1" value="{{$row -> h_caption or ''}}"
                                                           placeholder="">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label">Caption For Malayalam</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="caption1_ml" value="{{$row -> h_caption_ml or ''}}"
                                                           placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="title1" value="{{$row -> h_title or ''}}"
                                                           placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title For Malayalam</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="title1_ml" value="{{$row -> h_title_ml or ''}}"
                                                           placeholder="">
                                                </div>
                                            </div>
                                           
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Description</label>
                                                <div class="col-sm-5">
                                                <textarea rows="4" class="form-control"
                                                          name="description1">{{$row -> h_description or ''}}</textarea>
                                                </div>
                                            </div>
                                               <div class="form-group">
                                                <label class="col-sm-2 control-label">Description For Malayalam</label>
                                                <div class="col-sm-5">
                                                <textarea rows="4" class="form-control"
                                                          name="description1_ml">{{$row -> h_description_ml or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">link</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="link1" value="{{$row -> h_link or ''}}"
                                                           placeholder="">
                                                </div>
                                            </div>
                                        @elseif($row -> h_section == 2)
                                            <h4>Section Two</h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <div
                                                        class="input-group <?php echo isset($error['name']) ? 'has-error' : ''?>">
                                                        <input type="text" class="form-control"
                                                               name="title2" value="{{$row -> h_title or ''}} "
                                                               placeholder="Name">
                                                    </div><br/>
                                                    <input type="file" class="upload-file" data-height="473"
                                                           data-width="599"
                                                           onchange="fileSize(this)"
                                                           name="image2"/>
                                                    <p> * <b>Image format</b> - <i class="text-light-blue">allowed image
                                                            format
                                                            .jpeg,.png</i></p>
                                                    <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image
                                                            size
                                                            [599*473]
                                                            pixel</i></p>
                                                    <img src="{{ asset('uploads/home-cms/'.$row->h_image)}}"
                                                         data-placeholder="Image" width="250"/></a>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Description</label>
                                                <div class="col-sm-5">
                                                <textarea rows="4" class="form-control"
                                                          name="description2">{{$row -> h_description or ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">link</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="link2" value="{{$row -> h_link or ''}}"
                                                           placeholder="">
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </form>

                        </div>
                    </div>
        </section>


    </div>
    <script>


        $(document).ready(function () {

            var _URL = window.URL || window.webkitURL;

            $('.upload-file').change(function () {
                var file = $(this)[0].files[0];
                img = new Image();
                var imgwidth = 0;
                var imgheight = 0;
                var maxwidth = $(this).data('width');
                var maxheight = $(this).data('height');
                id = $(this).data('id')
                img.src = _URL.createObjectURL(file);
                img.onload = function () {
                    imgwidth = this.width;
                    imgheight = this.height;
                    $("#width").text(imgwidth);
                    $("#height").text(imgheight);
                    if (imgwidth <= maxwidth && imgheight <= maxheight) {
                    } else {
                        $('#image-' + id).val(null);
                        alert('allowed image resolution ' + maxwidth + ' X ' + maxheight + ' px');
                    }
                };
                img.onerror = function () {

                    $("#response").text("not a valid file: " + file.type);
                }

            });
        });

    </script>
@endsection
