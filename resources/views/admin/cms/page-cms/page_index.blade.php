@extends('layouts.backend_template')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{'Pages'}}
                <small></small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                @include('template.admin.alert')
                <?php $data_url = \App\Utils::crudPermissions(); ?>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">City recors</div> -->
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">

                                    {{--  @if(in_array(1 ,$data_url))
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/pages-cms/create') }}" class="btn btn-success btn-sm" title="Add New  BUsinness Category">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                        </a>
                                      @endif --}}

                                    <form method="GET" action="{{ url(\App\Utils::getUrlRoute().'/pages') }}"
                                          accept-charset="UTF-8" class="navbar-form navbar-right" role="search">
                                        <div class="input-group">
                                            <input type="text" class="form-control" value="{{Request::get('search')}}"
                                                   name="search" placeholder="Search..." value="">
                                            <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Name</th>
                                        <th>Meta Title</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pages as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->metaTitle}}</td>
                                            <td>
                                                @if(in_array(3 ,$data_url))
                                                    <a href="{{ url(\App\Utils::getUrlRoute().'/pages/' . $item->page_id . '/edit') }}"
                                                       title="Edit BusinessCategory">
                                                        <button class="btn btn-primary btn-sm"><i
                                                                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        </button>
                                                    </a>
                                                @endif
                                                @if(in_array(4 ,$data_url))
                                                    @if($item -> flag != 1)
                                                        {{-- <form method="POST" action="{{ url(\App\Utils::getUrlRoute().'/pages-cms' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                            {{ method_field('DELETE') }}
                                                            {{ csrf_field() }}
                                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Business" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                                                        </form> --}}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div
                                    class="pagination-wrapper"> {!! $pages->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
