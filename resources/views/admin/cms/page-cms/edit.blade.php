@extends('layouts.backend_template')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{'Pages'}}
                <small></small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                @include('template.admin.alert')
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">Edit Product </div> -->
                        <div class="panel-body">
                            <form method="POST" id="pageEdit"
                                  action="{{ url(\App\Utils::getUrlRoute().'/pages/' . $page[0] -> page_id) }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <a href="{{ url(\App\Utils::getUrlRoute().'/pages') }}" class="btn btn-link pull-right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
                                <br>
                                <hr>
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                    <label for="title" class="col-md-3 control-label">{{ 'Page Name *' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="title" type="text" id="title" value="{{ $page[0]->title or ''}}" required>
                                        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('title_ml') ? 'has-error' : ''}}">
                                    <label for="title" class="col-md-3 control-label">{{ 'Page Name For Malayalam' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="title_ml" type="text" id="title" value="{{ $page[0]->title_ml or ''}}" required>
                                        {!! $errors->first('title_ml', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('page_content') ? 'has-error' : ''}}">
                                    <label for="content" class="col-md-3 control-label">{{ 'Content *' }}</label>
                                    <div class="col-md-7">
                                        <textarea id="editor1" rows="10" cols="80" name="page_content">{{ $page[0]->page_content or ''}}</textarea>
                                        {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('page_content_ml') ? 'has-error' : ''}}">
                                    <label for="content" class="col-md-3 control-label">{{ 'Content For Malayalam' }}</label>
                                    <div class="col-md-7">
                                        <textarea id="editor2" rows="10" cols="80" name="page_content_ml">{{ $page[0]->page_content_ml or ''}}</textarea>
                                        {!! $errors->first('page_content_ml', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('banner') ? 'has-error' : ''}}">
                                    <label for="banner" class="col-md-3 control-label">{{ 'Banner Caption *' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="banner" type="text" id="banner" value="{{ $page[0]->banner or ''}}" required>
                                        {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                 <div class="form-group {{ $errors->has('banner_ml') ? 'has-error' : ''}}">
                                    <label for="banner" class="col-md-3 control-label">{{ 'Banner Caption For Malayalam' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="banner_ml" type="text" id="banner" value="{{ $page[0]->banner_ml or ''}}" required>
                                        {!! $errors->first('banner_ml', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('metaTitle') ? 'has-error' : ''}}">
                                    <label for="metaTitle" class="col-md-3 control-label">{{ 'Meta Title *' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="metaTitle" type="text" id="metaTitle" value="{{ $page[0]->metaTitle or ''}}" required>
                                        {!! $errors->first('metaTitle', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('metaDescription') ? 'has-error' : ''}}">
                                    <label for="metaDescription" class="col-md-3 control-label">{{ 'Meta Description *' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="metaDescription" type="text" id="metaDescription" value="{{ $page[0]->metaDescription or ''}}" required>
                                        {!! $errors->first('metaDescription', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('metaKeyword') ? 'has-error' : ''}}">
                                    <label for="metaKeyword" class="col-md-3 control-label">{{ 'Meta Keyword *' }}</label>
                                    <div class="col-md-7">
                                        <input class="form-control" name="metaKeyword" type="text" id="metaKeyword" value="{{ $page[0]->metaKeyword or ''}}" required>
                                        {!! $errors->first('metaKeyword', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-3">
                                        <input class="btn btn-primary" type="submit" value="{{ 'Save Changes' }}">
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- validation rules -->
        <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
        <script>
            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1')
                CKEDITOR.replace('editor2')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            })
        </script>
    </div>
@endsection
