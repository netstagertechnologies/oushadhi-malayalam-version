@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive col-md-12">

                                @include('template.admin.alert')
                                <div class="pull-right">
                                    <?php if (in_array(3, $crud_permissions)){?><a
                                        href="{{ url(\App\Utils::getUrlRoute().'/quotations/' . $page_data->quotation_id.'/edit' ) }}"
                                        class="btn btn-info btn-sm" title="EDIT">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> EDIT
                                    </a>
                                    <?php }?>
                                    <div class="pull-right">
                                        <button id="expCsv" class="btn btn-success">EXPORT TO CSV</button>
                                        <br/><br/>
                                    </div>
                                    <a
                                        href="{{ url(\App\Utils::getUrlRoute().'/quotations/') }}"
                                        class="btn btn-warning btn-sm" title="Back">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                                <br/>
                                <br/>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab-general" data-toggle="tab">{{ 'Overview' }}</a>
                                    </li>
                                    <li><a href="#tab-status" data-toggle="tab">{{ 'Status' }}</a></li>
                                </ul>
                                <div class="tab-content">
                                    <br/>

                                    <div class="tab-pane active" id="tab-general">

                                        <div>
                                            <table id="quotations"
                                                   class="table">
                                                <thead>
                                                <tr class=''>
                                                    <th scope="col"></th>
                                                    <th scope="col">
                                                        Perfoma Invoice # {{$page_data->quotation_id}}
                                                        {{$user_details->name}}
                                                        {{ $user_details->email}}
                                                        {{$user_details->mobile}}
                                                    </th>
                                                    <th scope="col"></th>
                                                    <th scope="col"></th>
                                                    <th scope="col"></th>
                                                    <th></th>
                                                </tr>
                                                <tr class='bg-gray'>
                                                    <th scope="col" width="10%">Code</th>
                                                    <th scope="col" width="35%">Name</th>
                                                    <th scope="col" width="10%">Unit</th>
                                                    <th scope="col" width="15%">Price</th>
                                                    <th scope="col" width="10%">Qty</th>
                                                    <th scope="col" width="20%">Total</th>
                                                </tr>
                                                </thead>
                                                <tbody class="bg-gray-light">

                                                @php $total=0;$utotal=0;$ttotal=0; $products=array();@endphp
                                                @if(isset($quotation_details))
                                                    @foreach($quotation_details as  $product)
                                                        <tr>
                                                            <td>{{$product->product_code}}</td>
                                                            <td>{{$product->product_name}}</td>
                                                            <td>{{$product->unit_value}}</td>
                                                            <td>{{\App\Utils::formatPrice($product->price)}}</td>
                                                            <td>{{$product->qty}}</td>
                                                            <td>{{\App\Utils::formatPrice($product->total)}}</td>
                                                        </tr>
                                                        @php $total+=$product->total;
                                                    $utotal+=$product->total_uprice;
                                                    $ttotal+=$product->total_utax;@endphp
                                                    @endforeach
                                                @endif
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Product Total</td>
                                                    <td>{{\App\Utils::formatPrice($utotal)}}</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Tax</td>
                                                    <td>{{\App\Utils::formatPrice($ttotal)}}</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Total</td>
                                                    <td>{{\App\Utils::formatPrice($total)}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-status">
                                        <br/>
                                        <div class="col-md-6">
                                            <!-- The time line -->
                                            <ul class="timeline">
                                                <!-- timeline time label -->
                                                <li class="time-label">
                                                    <span class="bg-red"> Perfoma Invoice status</span>
                                                </li>
                                                <!-- /.timeline-label -->
                                                @if($quotation_histories)
                                                    @foreach($quotation_histories as $history)
                                                        <li>
                                                            <i class="fa fa-check bg-green"></i>

                                                            <div class="timeline-item">
                                                        <span class="time"><i
                                                                class="fa fa-clock-o"></i> {{$history->date_added}}</span>

                                                                <h3 class="timeline-header no-border"><a
                                                                        href="#">{{$history->name}}</a>
                                                                    {{$history->comment}}</h3>
                                                            </div>
                                                        </li><!-- timeline item -->
                                                @endforeach
                                            @endif

                                            <!-- END timeline item -->
                                                <li>
                                                    <i class="fa fa-clock-o bg-gray"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="box box-widget">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Add Order Status</h3>
                                                </div>
                                                <div class="box-footer box-comments">
                                                    <form class="form-horizontal" method="POST"
                                                          action="{{url(\App\Utils::getUrlRoute().'/quotations/updateStatus/'.$page_data->quotation_id)}}">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Status</label>
                                                            <div class="col-sm-10">

                                                                <div class="input-group">
                                                                    <select name="quotation_status" class="form-control"
                                                                            required>
                                                                        <option value="">-- Select Status--</option>
                                                                        @if($quotation_status)
                                                                            @foreach($quotation_status as $qs)
                                                                                <option
                                                                                    @if($page_data->quotation_status==$qs->status_id) {{'selected'}}@endif
                                                                                    value="{{$qs->status_id}}">{{$qs->name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label"
                                                                   for="input-comment">{{ 'Comment' }}</label>
                                                            <div class="col-sm-10">
                                                                <textarea
                                                                    name="comment" rows="8"
                                                                    class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label"></label>
                                                            <div class="col-sm-10">

                                                                <button type="submit"
                                                                        class="btn btn-info pull-right">
                                                                    Submit
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- /.box-body -->

                                                <!-- /.box-footer -->
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script type="text/javascript" src="{{ asset('exprt/jspdf.debug.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
    <script src="{{ asset('exprt/tableHTMLExport.js')}}"></script>
    <script>
        $('#expCsv').on('click', function () {
            $("#quotations").tableHTMLExport({
                type: 'csv',
                filename: 'PerfomaInvoice-A{{$page_data->quotation_id or '0'}}' + '.csv',
                // your html table has html content?
                htmlContent: false,

            });
        })
        $('#expPdf').on('click', function () {
            $("#quotations").tableHTMLExport({
                type: 'pdf',
                filename: 'PerfomaInvoice-A{{$page_data->quotation_id or '0'}}' + '.pdf',
                // your html table has html content?
                htmlContent: false,
                trimContent: false,
            });
        })
    </script>
@endsection
