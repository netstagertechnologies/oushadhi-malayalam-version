@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">

                                                         <br/>

                                <div class="col-md-12">
                                    <form method="GET" action="{{ url(\App\Utils::getUrlRoute().'/product') }}"
                                          accept-charset="UTF-8" class="navbar-form navbar-right" role="search">

                                        <div class="row">
                                            <div class="form-group ">
                                                <select name="category" id="category" class="form-control">
                                                    <option value="">Select Category</option>
                                                    <?php $categorys = (Request::get('category') != '') ? Request::get('category') : 0;?>
                                                    @foreach($categories as $cat)
                                                        <option value="{{ $cat->category_id }}"
                                                                @if($categorys ==$cat->category_id) selected="" @endif>{{ $cat->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control"
                                                           value="{{Request::get('search')}}" name="search"
                                                           placeholder="Search Product">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                                <a href="{{ url(\App\Utils::getUrlRoute().'/product') }}"
                                                   class="btn  btn-warning">Clear</a>

                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <br/>
                                <br/>

                            <div class="table-responsive col-md-12">


                                @include('template.admin.alert')
                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <th style="width:50%">Title</th>
                                        <th>Total Qty</th>
                                        <th>Total Amount</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $d)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>@if(isset($d->image)&&!empty($d->image))
                                                        <img src="{{ URL::to('/')}}/uploads/product/{{$d->image}}"
                                                             data-placeholder="Image" height="75"/>
                                                    @endif</td>
                                                <td>{{ $d->title }}</td>
                                                <td>{{ $d->title }}</td>
                                                <td>{{ $d->title }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">No records found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $page_data->appends($_GET)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
