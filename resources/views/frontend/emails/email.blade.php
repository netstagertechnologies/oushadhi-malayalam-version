
@extends('layouts.email_template',['title'=>'Forgot Password'])

@section('content')

    <tr>
        <td height="50" align="center" valign="middle" bgcolor="#009209">
            <table width="650" border="0" cellspacing="0" cellpadding="0">
               <tr>
                    <td height="22" align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif"><strong
                                style="color:#fff;font-size:13px;font-weight:600;"><font face="Montserrat, sans-serif">Forgot Password</font></strong></font></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr bgcolor="#ffffff">
        <td height="35" align="center" valign="top">&nbsp;</td>
    </tr>
    <tr bgcolor="#ffffff">
        <td align="center" valign="top">
            <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" bgcolor="#f9f9f9" cellpadding="6"
                               style="border:1px #06b51d solid;padding: 8px 0;margin-top: 10px;">
                            <tr>
                                <td align="left" valign="middle" style="font-size:13px;"><font
                                        face="Montserrat, sans-serif">
                                        @foreach ($introLines as $line)
                                            {{ $line }}

                                        @endforeach


                                        {{-- Action Button --}}
                                        @isset($actionText)
                                            <?php
                                            switch ($level) {
                                                case 'success':
                                                    $color = 'green';
                                                    break;
                                                case 'error':
                                                    $color = 'red';
                                                    break;
                                                default:
                                                    $color = 'green';
                                            }
                                            ?>
                                            @component('mail::button', ['url' => $actionUrl, 'color' => $color])
                                                {{ $actionText }}
                                            @endcomponent
                                        @endisset

                                    </font></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="font-size:16px;"><font face="Montserrat, sans-serif">

                                        {{-- Outro Lines --}}
                                        @foreach ($outroLines as $line)
                                            {{ $line }}

                                        @endforeach

                                    </font></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
@endsection
