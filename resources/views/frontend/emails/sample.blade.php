@extends('layouts.email_template',['title'=>'Sample'])

@section('content')

    <tr>
        <td height="197" align="center" valign="middle" bgcolor="#008ccf"><table width="650" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="middle" style="color:#fff;font-size:24px;font-weight:300;"><font face="Montserrat, sans-serif">Order Confirmation</font></td>
                </tr>
                <tr>
                    <td height="30" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td height="22" align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font face="Montserrat, sans-serif"><strong style="color:#fff;font-size:13px;font-weight:600;"><font face="Montserrat, sans-serif">Dear (Mr Customer),</font></strong></font></td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font face="Montserrat, sans-serif">Thanks for placing your order with Canbasket. This email contains important information <br>
                            regarding your recent purchase. Please save it for reference.</font></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="35" align="center" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" valign="top"><table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-size:24px;"><font face="Montserrat, sans-serif">Order Details</font></td>
                </tr>
                <tr>
                    <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="8">
                            <tr>
                                <td width="48%" align="left" valign="top" style="border-top:1px #cacaca solid;border-bottom:1px #cacaca solid;border-right:1px #cacaca solid;border-left:1px #cacaca solid;font-size:13px;line-height:23px; font-weight:300;"><font face="Montserrat, sans-serif"><strong style="font-weight:300;font-weight:700;"><font face="Montserrat, sans-serif">Mobile Number:</font></strong> 123456789<br>
                                        <strong style="font-weight:300;font-weight:700;"><font face="Montserrat, sans-serif">Address:</font></strong> Lorem Ipsum is simply dummy <br>
                                        text of the printing and typesetting <br>
                                        industry</font></td>
                                <td width="48%" align="left" valign="top" style="border-top:1px #cacaca solid;border-bottom:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;line-height:23px;font-weight:300;"><font face="Montserrat, sans-serif"><strong style="font-weight:300;font-weight:700;"><font face="Montserrat, sans-serif">Order Date :</font></strong> 7-12-2018; 1:42:23 PM<br>
                                        <strong style="font-weight:300;font-weight:700;"><font face="Montserrat, sans-serif">Trace No:</font></strong> 240</font></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="6" style="border-bottom:1px #cacaca solid;border-left:1px #cacaca solid;">
                            <tr>
                                <th width="7%" height="28" align="center" valign="middle" style="background:#e8e8e8;border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-weight:600;font-size:12px;" scope="col"><font face="Montserrat, sans-serif">No</font></th>
                                <th width="33%" align="left" valign="middle" style="background:#e8e8e8;border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-weight:600;font-size:12px;" scope="col"><font face="Montserrat, sans-serif">Order Name</font></th>
                                <th width="20%" align="center" valign="middle" style="background:#e8e8e8;border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-weight:600;font-size:12px;" scope="col"><font face="Montserrat, sans-serif">Order ID</font></th>
                                <th width="20%" align="center" valign="middle" style="background:#e8e8e8;border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-weight:600;font-size:12px;" scope="col"><font face="Montserrat, sans-serif">Quantity</font></th>
                                <th width="20%" align="center" valign="middle" style="background:#e8e8e8;border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-weight:600;font-size:12px;" scope="col"><font face="Montserrat, sans-serif">Order Amt.</font></th>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">1</font></td>
                                <td align="left" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">Canbasket Product</font></td>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">#9893</font></td>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">5</font></td>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">CMD 200</font></td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">1</font></td>
                                <td align="left" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">Canbasket Product</font></td>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">#9893</font></td>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">5</font></td>
                                <td align="center" valign="middle" style="border-top:1px #cacaca solid;border-right:1px #cacaca solid;font-size:13px;"><font face="Montserrat, sans-serif">CMD 200</font></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right" valign="top" style="font-size:14px;line-height:25px;"><font face="Montserrat, sans-serif">Shipping Charge : <strong><font face="Montserrat, sans-serif">CMD 100</font></strong><br>
                                        Delivery  Charge : <strong><font face="Montserrat, sans-serif">CMD 100</font></strong></font></td>
                            </tr>
                            <tr>
                                <td align="right" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" style="font-size:18px;font-weight:700;"><strong style="font-size:18px;font-weight:700;"><font face="Montserrat, sans-serif">Total :CMD 600</font></strong></td>
                            </tr>
                            <tr>
                                <td align="right" valign="top">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>

@endsection
