@extends('layouts.email_template',['title'=>'Oushadhi New User'])

@section('content')

    <tr>
        <td height="197" align="center" valign="middle" bgcolor="#009209">
            <table width="650" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="22" align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif"><strong
                                style="color:#fff;font-size:13px;font-weight:600;"><font face="Montserrat, sans-serif">Dear {{ucfirst($user)}}
                                    ,</font></strong></font></td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif">Thanks for creating account with Oushadhi.</font></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="35" align="center" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" bgcolor="#f9f9f9" cellpadding="6"
                               style="border:1px #06b51d solid;padding: 8px 0;margin-top: 10px;">
                            <tr>
                                <td align="left" valign="middle" style="font-size:13px;"><font
                                        face="Montserrat, sans-serif">
                                    Click the link to verify your account<br/>
                                    <a href="{{URL::to('/verify-account/'.$user_id.'/'.$cus_token)}}">Verify Now</a> </font></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
@endsection
