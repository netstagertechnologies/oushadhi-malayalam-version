@extends('layouts.email_template',['title'=>'Raw Material Request-'.$material_data['title']])

@section('content')

    <tr>
        <td height="197" align="center" valign="middle" bgcolor="#009209">
            <table width="650" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="middle" style="color:#fff;font-size:24px;font-weight:300;"><font
                            face="Montserrat, sans-serif">{{$material_data['title']}}</font></td>
                </tr>
                <tr>
                    <td height="30" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td height="22" align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif"><strong
                                style="color:#fff;font-size:13px;font-weight:600;"><font face="Montserrat, sans-serif">Dear {{ucfirst($material_data['name'])}}
                                    ,</font></strong></font></td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif">Raw Material Enquiry Received<br/></font></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="35" align="center" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-size:16px;"><font face="Montserrat, sans-serif">Details</font></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" bgcolor="#f9f9f9" cellpadding="6"
                               style="border:1px #06b51d solid;padding: 8px 0;margin-top: 10px;">
                            <tr>
                                <td align="left" valign="middle" style="font-size:13px;"><font
                                        face="Montserrat, sans-serif">Title: {{$material_data['title']}}<br/>
                                    Common Name: {{$material_data['common_name']}} </font></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-size:16px;"><font face="Montserrat, sans-serif">Enquiry Details</font></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" bgcolor="#f9f9f9" cellpadding="6"
                               style="border:1px #06b51d solid;padding: 8px 0;margin-top: 10px;">
                            <tr>
                                <td align="left" valign="middle" style="font-size:13px;"><font
                                        face="Montserrat, sans-serif">Name: {{$request_data['name']}}<br/>
                                        Email: {{$request_data['email']}}<br/>
                                        Phone: {{$request_data['phone']}}<br/>
                                        Message: {{$request_data['message']}}<br/>
                                    </font>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
@endsection
