<!DOCTYPE html>
<html lang="en">
<head>

<body>

<div class="wid">

    <p>God&rsquo;s own country &lsquo;Kerala&rsquo; is known all over the world for many unique things and the traditional Ayurveda is one among them. There are more than 12,000 traditional healers and 800 Ayurveda medicine manufacturing units in the state. Oushadhi is the largest manufacturer of Ayurvedic Medicine in the government sector in India. Oushadhi Panchakarma Hospital &amp; Research Institute located in the heart of Thrissur, the cultural capital of Kerala, offers varieties of Ayurveda treatments at affordable price using quality medicines produced by &lsquo;Oushadhi&rsquo;. There is always heavy rush for the treatment in this government run Panchakarma Hospital and bookings are done well in advance.</p>

    <p><strong>Our Specialties </strong></p>

    <p><strong>PANCHAKARMA THERAPY </strong></p>

    <p>&nbsp;Panchakarma therapy (penta fold purificatory measures) play a vital role in Ayurvedic therapies and as such, they occupy an important place in Ayurvedic system of Medicine. The classical Ayurveda Panchakarma therapy is the comprehensive method of internal purification of the body by VAMANA, VIRECHANA, VASTHI, NASYA &amp; VAMANA</p>

    <p>&nbsp;Process by which vitiated humours are eliminated through oral route, for the purification of the upper half of the body.(Induced emesis-specific therapy for kaphaja disorders)</p>

    <ol>
        <li><p><strong>VIRECHANA : </strong>Process by which vitiated humours are eliminated through anal route, for the purification of the lower half of the body. (Induced Purgation/Laxation-specific therapy for paithika Disorders)</p></li>
        <li><p><strong>VASTHI : </strong>Purificatory measure which consists of suitable medicaments in the form of enemata through rectal, vaginal or urethral routes (Oleus and Unoleus Enemata-specified therapy for Vathika Disorders)</p></li>
        <li><p><strong>NASYA: </strong>Purificatory measures by which suitable medicaments are introduced through nasal passage in&nbsp; liquid&nbsp; or in powder form. (Errhines/Snuffing-therapy for diseases related with head)</p></li>
        <li>RAKTHAMOKSHAM<strong> : </strong>Purificatory measures in diseases related with impure blood by using external means like, instruments (Prachanam), leeches(Jalookavacharanam), horns etc, (Bloodletting-specific therapy for diseases Related to Blood).</p></li>
    </ol>

    <p><strong>RECREATION OF BODY</strong></p>

    <ol>
        <li><p><strong>DHARA: </strong>Uninterrupted application of medicated oil (Thailadhara), buttermilk (Thakradhara), milk (Ksheeradhara), fermented whey (Dhanyamladhara) on the head, throughout body or both for Insomnia, Rheumatic complaints etc.</p></li>
        <li><p><strong>PIZHICHIL: </strong>Pouring comfortable warm medicated oil liberally over the body followed by gentle massage for skin disease and rheumatic complaints.</p></li>
        <li><p><strong>NAVARAKKIZHI: </strong>Massage done with small linen bags filled with medicated and cooked Navara rice, after a liberal application of medicated oils for rheumatic complaints &amp;rejuvenation.</p></li>
        <li><p><strong>THAKRADHARA:</strong> <strong>&nbsp;</strong>Uninterrupted application of medicated butter milk on the head for sleeplessness, skin diseases, mental disorders etc.</p></li>
        <li><p><strong>THALAPOTHICHIL:</strong> Procedure in which herbal medicines are made into paste and applied over the head 9in a special manner for mental disorders etc.</p></li>
        <li><p><strong>UDHWARTHANAM: </strong>Massage with medicated powders specially meant for reducing body fat, improvement of complexion land functional improvement of vital organs.</p></li>
        <li><p><strong>NAVARATHEPPU:</strong> Application of medicated cooked navara rice on whole or affected parts of the body for rheumatic complaints &amp;rejuvenation.</p></li>
        <li><p><strong>SNEHANAM: </strong>Oleation therapy-Internal as well as external done prior of Panchakarma Therapy for rheumatic complaints, skin diseases, mental disorders etc.</p></li>
        <li><p><strong>SNEHAPANAM: </strong>Intake of medicated unctuous Dravya like Ghee, Oil etc. for a specific duration especially for chronic diseases.</p></li>
        <li><p><strong>ABHYANGAM: </strong>Application of medicated oil all over the body. It relieves fatigue and provides stamina and perfect sleep.</p></li>
        <li><p><strong>SWEDANAM: </strong>Sudation technique done prior to Panchakaram. Therapy for Thirteen methods of thermal Sudation like Mixed Fomentation, Hot bed Sudation, Steam kettle Sudation, Affusion Sudation, Bath Sudation, Sudatorium Sudation, Stone bed Sudation, Trench Sudation, Cabin Sudation, Ground red Sudation, Picher bed Sudation, Pit Sudation, Under bed Sudation, are followed in this method.</p></li>
        <li><p><strong>UPANAHAM: </strong>Wrapping specific areas for a specific time interval with medicated pastes intended to subside inflammation or pain.</p></li>
        <li><p><strong>PALPPUKA: </strong>Sudation Technique with medicated milk for arthritic problems.</p></li>
        <li><p><strong>KIZHI:</strong>&nbsp; Fomentation technique by using boluses of herbal leaves (Elakkizhi) or medicated powders (Choornakkizhi) or warm sand (Manalkkizhi) or roasted salt (Uppukkizhi) according to the conditions of disease</p></li>
        <li><p><strong>PICHYU:</strong> Applying oil soaked cloth over affected body parts intended to reduce local pain or stiffness.</p></li>
        <li><p><strong>LEPAM</strong>: Applying medicated pastes on the body parts for skin diseases, local pain &amp; swelling.</p></li>
        <li><p><strong>REJUVENATION :</strong>Free meditation and yoga classes are arranged for all inmates to rejuvenate the body &amp; mind.</p></li>
        <li><p><strong>RESEARCH :</strong>Panchakarma Hospital has a clinical trial cell intended to carry out research on chronic diseases and con ditions like Arthritis, Diabetes, Rheumatic Complaints, Skin Diseases, Head ache, Calculi, Cholesterol etc. using newly developed medicines by the R&amp;D wing of Oushadhi.</p></li>
    </ol>



    <div class="abtsec-cms">
        <p></p>
        <p><strong>CORPORATE SOCIALRESPONSIBILITY ('CSR") POLICY</strong></p>

        <p><strong>FOR THE PHARMACEUTICAL CORPORATION (I.M,) KERALA LTD.</strong></p>

        <ol>
            <li>
                <p><strong>INTRODUCTION:</strong></p>
                <p>Corporate Social Responsibility (CSR) is the Company’s commitment to its stakeholders to conduct business in an economically, socially and environmentally sustainable manner that is transparent and ethical. THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. is committed to undertake CSR activities in accordance with the provisions of Section 135 of the Indian Companies Act, 2013 and related Rules.</p>
                <p>THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. believes that corporate development has to be inclusive and every corporate has to be responsible for the development of a just and humane society that can build a national enterprise</p>
            </li>

<li> <p><strong>AIMS & OBJECTIVES</strong></p>
<ul>
    <li><p>(i)   To develop a long-term vision and strategy for THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. CSR objectives.</p></li>
    <li><p>(ii)  Establish relevance of potential CSR activities to TH E PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. core business and create an overview of activities to be undertaken, in line with Schedule VII ofthe Companies Act, 2013.</p></li>
    <li><p>(iii) THE PHARMACEUTICAL CORPORATION (LM.) KERALA LTD shall promote projects
        Those are:
        &nbsp;&nbsp;&nbsp;(a) Sustainable and create a long term change;
            &nbsp;&nbsp;&nbsp;(b) Have specific and measurable goals  in alignment with the company's philosophy;
            &nbsp;&nbsp;&nbsp;(c) Address the most deserving cause or beneficiaries.
        </p></li>
    <li><p>(iv) To establish process and mechanism for the implementation and monitoring of the CSR Activities for the Company.</p></li>

</ul></li>

            <li> <p><strong>COMMITTEE COMPOSITION </strong></p>
            <p>The CSR Committee of the Board shall be composed of at least three (3) Directors, Members of the CSR Committee may be replaced by any other member of the Board.
            </p></li>
            <li> <p><strong> COMMITTEE MEETINGS</strong></p>
            <p>The CSR Committee shall meet as often as its members deem necessary to perform the duties and responsibilities but not less than quarterly.</p></li>
            <li> <p><strong>DUTIES &amp; RESPONSIBILITIES OF CSR COMMITTEE</strong></p>
                <ol>
                    <li>
                        <p>Review of the CSR activities to be undertaken by THE PHARMACEUTICAL CORPORATION (I.M.) KERALA
                                LTD. The CSR Committee shall be guided by the list of Activities specified in Schedule VII to
                                the Companies Act, 2013 and appended to this Policy as Appendix - 1. Appendix 1 may be revised
                                in line with any amendments/inclusions made To Schedule VII of the Companies Act, 2014.
                        </p>
                    </li>
                    <li>
                        <p>Formulate and recommend to the Board the CSR activities/programs to be undertaken by
                                the Company.</p>
                    </li>
                    <li>
                        <p>Recommend the CSR Expenditure to be incurred on the CSR activities/programs.</p>
                    </li>
                    <li>
                        <p>Institute a transparent mechanism for implementation of the CSR projects and activities
                            Effectively monitor the Execution of the CSR activities.</p>
                    </li>
                    <li><p>Prepare an annual report of the CSR activities undertaken for the company and submit   such Report to the Board</p></li>
                </ol>
            </li>
            <li> <p><strong> RESPONSIBILITY OF THE BOARD</strong></p>
                <ul>
                    <li>
                        <p>(i) Approve the CSR Policy and the CSR Expenditure after taking into consideration the recommendations made by the CSR committee
                        </p>
                    </li>
                    <li><p>(ii) Ensure the CSR spending every financial year of at least 20lo of average net profits made during immediately preceding 3 financial years/ in pursuance with the Policy.</p></li>
                    <li><p>(iii)Ensure that CSR activities included in the CSR Policy are undertaken by THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. and those activities are Related to the activities specified in Schedule VII of the Companies Act.</p></li>
                    <li><p>(iv)Ensure disclosure of the contents of the CSR Policy on the THE PHARMACEUTICAL  CORPORATION (I. M) KERALA_LTD website.</p></li>
                    <li><p>(v) Directors' Report for FY 2013-14-onwards to include:<br/>
                            &nbsp;&nbsp;&nbsp;(a) Consults of the CSR Policy and Composition of the CSR committee; <br/>
                            &nbsp;&nbsp;&nbsp;(b) An annual report on the CSR in the prescribed format as per Appendix- 3; <br/>
                            &nbsp;&nbsp;&nbsp;(c) Reasons for failure (if any) to spend equired amount on CSR activities.</p></li>
                    <li><p>(vi) THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. shall undertake the CSR activities directly. The Board may, in the future, decide to undertake and implement its CSR activities through a registered trust or registered society or a Section B company (Nonprofit entity) established by the company. In case the trust, society or a Section 8 company is not established by the company or its holding or subsidiary or associate company, then such an entity will need to have a 3 years track record of undertaking similar projects or programmes.</p></li>
                    </ul>
            </li>
            <li> <p><strong>CSR EXPENDITURE</strong></p>
                <ul>
                    <li>
                        <p>(i) In every financial year, THE PHARMACEUTICAL CORPORATION (i.M.) KERALA
                            LTD. Shall spend a minimum of 2o/o of its average Net Profits in
                            the immediately preceding Three (3 Financial years. Average Net profits shall
                            mean the net profits of the Company as per The Profit & Loss Statement prepared in accordance with
                            the Companies Act, 2013CSR Expenditure shall mean all expenditure incurred in respect of
                            specific projects/programs Relating to the approved CSR activities.</p></li>
                    <li>
                        <p>(ii) CSR Expenditure shall not include expenditure on an item not in conformity or not in line With activities which fall within the purview of the CSR activities listed in Schedule VII.</p>
                    </li>
                    <li><p>(iii) CSR Expenditure shall not include Projects or programs or activities undertaken outside  India.</p></li>
                    <li><p>(iv)The surplus arising out of the CSR activities or projects shall not form pad of the
                            business Profit of THE PHARIYACEUTICAL CORPORATION (I.M.) KERALA LTD.</p></li>
                </ul></li>
            <li> <p><strong>CSR ACTIVITIES – PROJECTS</strong></p>
            <ul>
                <li><p>(i) THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. shall promote CSR Activities/Projects in the field of :
                        A. Promoting preventive health care - conducting free medical camps in all parts of
                        Kerala&nbsp;And organize continuous medical education programme for doctors and
                        sponsorship in Sports activates <br/>

                        B. Ensuring environmental sustainability, ecological balance, protection of flora and fauna
                        -to take up cultivation of medicinal Plants and distribute the same at free
                        of cost.</p></li>
                <li><p>(ii)Company may also undertake other CSR activities in line with Schedule VII.</p></li>
                <li><p>(iii)The CSR activities shall be undertaken in locations within India. Company shall give preference to the local areas and the areas around which Company operates while considering the activities to be undertaken and spending the amount earmarked for CSR activities. However, Company has a multi-state presence and hence may be guided by the requirements of the specific CSR activity/program in determining the locations within India.</p></li>
            </ul></li>
            <li> <p><strong> IMPLEMENTING CSR ACTIVITIES</strong></p>
                <ul>
                    <li><p>(i) The Company shall undertake the CSR activities directly.</p></li>
                    <li><p>(ii)The initiatives undertaken may be communicated to the employees through specific Awareness campaigns, so as to enable maximum participation,</p></li>
                    <li><p>(v)THE PHARIYACEUTICAL CORPORATION (I.M.) KERALA LTD may also collaborate or pool resources with other companies to undertake CSR activities in such a manner that each companies are in a position to report separately on such CSR projects</p></li>
                    <li><p>(v) The following activities do not qualify as CSR Activities under the Companies Act,
                            2013:<br/>
                            (a) Projects or activities not falling within Schedule VII (Appendix  1)<br/>
                            (b)Activities undertaken in pursuance of normal course of business<br/>
                            (c) Projects or programs or activities that benefit only the employees of KALYAN SILKS and their families<br/>
                            (d)Direct or indirect contribution to any political party.</p></li>
                </ul></li>

            <li> <p><strong>CSR REPORTING</strong></p>
                <p>The Board in its Annual Report shall include the details of the CSR activities’ undertaken in the
                    Financial Year the particulars to be stated in the report shall be in the format prescribed in Appendix
                    - 3.The CSR Committee shall provide a responsibility statement on the implementation and monitoring of the CSR Policy and that it is in compliance with CSR objectives of the Company, which
                    statement shall form pad of the Boards' Report.</p></li>
            <li> <p><strong>WEBSITE DISPLAY</strong></p>
                <p>THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD shall display on its website (www.kalyansilks,com) the contents of its CSR Policy and other information as may. be required to be
                    displayed.</p></li>
            <li> <p><strong> REVIEW AND AUDIT</strong></p>
                <p>The CSR committee shall be apprised on the implementation of the CSR activities and the progress
                    shall be monitored on a quarterly basis. THE PHARMACEUTCAL CORPORATION (I.M.) KERALA LTD. shall through its internal controls. Monitoring and evaluation systems implement, assess document and report the impact of its CSR activities/projects. Records relating to the CSR activities and the CSR Expenditure shall be meticulously maintained The Records shall be submitted for reporting and audit. The financial audits of the implementing agencies shall also be done through periodic audits. In this regard. The company may appoint independent external consultants for carrying out such audits.
                </p></li>
            <li> <p><strong>AMENDMENTS</strong></p>
                <p>The Policy may be reviewed and amended from time to time, By Order of the Board of Directors
                </p></li>

        </ol>


        <p>Place: Kuttanellur</p>

        <p>Date: 17.10.2018</p>

        <p class="pull-right">Chairman</p>


        <h1>APPENDIX – 1</h1>

        <p>CSR ACTIVITIES LISTED INSCHEDULE VII OFTHE COMPANIES ACT, 2013</p>

        <p>CSR shall focus on social, economic and environmental impact rather than mere output and outcome.
                Activities which are ad hoc and philanthropic in nature shall be avoided. Various activities that can be
                undertaken in general under CSR are outlined below:</p>

        <ol>
            <li>
                <p>Eradicating extreme hunger and poverty and malnutrition, promoting preventive healthcare and sanitation and making available safe drinking water;</p>
            </li>
            <li><p>Promotion of education; including special education and employment enhancing vocation skills
                    especially among children, woman, elderly and the differently abled and livelihood enhancement
                    projects;</p></li>
            <li><p>Promoting gender equality and empowering women; setting up homes and hostels for women and
                    orphans, setting up old age homes, day care centers, and such other facilities for senior citizens and
                    measures for reducing inequalities faced by socially and economically backward groups;</p></li>
            <li><p>Ensuring environmental sustainability/ ecological balance, protection of flora and fauna. Animal
                    welfare, agroforestry, conservation of natural resources and maintaining of quality of soil, air and&nbsp;water;
                </p></li>
            <li><p>protection of national heritage, art and culture including restoration of buildings and sites of
                    historical importance and works of art; setting up of public libraries; promotion and development of
                    traditional arts and handicrafts;</p></li>
            <li><p>Measures for the benefit of armed forces veterans, war widows and their dependents;</p></li>
            <li><p>Training to promote rural sports, nationally recognized sports, and Paralympic sports and Olympic
                    sports;</p></li>
            <li><p>contribution to the Prime Minister's National Relief Fund or any other fund set up by the Central
                    Government or the State Governments for socio-economic development and relief and welfare of the
                    Scheduled Castes, the Scheduled Tribes, other backward classes, minorities and women;</p></li>
            <li><p>Contributions or funds provided to technology incubators located within academic institutions
                    which are approved by the Central Government; and Rural development projects.</p></li>
           </ol>

        <p>By Order of the Board of Directors</p>

        <p><br>
            Chairman</p>


        <p>Place: Kuttanellur</p>

        <p>Date:17.10.2018</p>

    </div>

</div>
</body>
</html>
