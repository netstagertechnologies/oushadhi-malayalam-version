@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@if($language=="ml"){{$page_title}}@else {{$page_title}} @endif</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="abtsec-cms">
               <p>@if($language=="ml") {!! html_entity_decode($page_details->page_content_ml) !!} @else {!! html_entity_decode($page_details->page_content) !!} @endif</p>
            </div>

        </div>
    </section>


@endsection
