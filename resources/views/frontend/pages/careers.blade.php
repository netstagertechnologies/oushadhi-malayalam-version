@extends('layouts.frontend_template',['page_title'=>'Careers'])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.Careers')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">

            @include('template.frontend.alert')
            <div class="tenters-main">

                @foreach($careers as $career)
                    <div class="tenters-full">
                            <div class="stenter-left">
                                <p>@if($language == "ml") {{$career['title_ml']}} @else {{$career['title']}} @endif</p>
                                @if(!empty($career['last_date']))
                                     @lang('tender.Last date & Time') : <strong> {{date('d.m.Y', strtotime($career['last_date']))}}</strong><br/>
                                    @endif
                                @if(!empty($career['dept']))@lang('tender.Department') :  <strong>@if($language ==  "ml") {{$career['dept_ml'] or ''}} @else {{$career['dept'] or ''}} @endif</strong><br/>@endif
                                    @if(!empty($career['contact'])&&$career['contact']!=null)  @lang('tender.Contact No.')
                                    <strong>{{$career['contact'] or ''}}</strong><br/>
                                    @endif
                                    @if(!empty($career['contact_email'])&&$career['contact_email']!=null) @lang('tender.Contact Email'): :
                                    <strong>{{$career['contact_email'] or ''}}</strong><br/>
                                    @endif

                            </div>
                            @if(isset($career['career_files'])&&$career['career_files'])
                                <div class="stenter-right">
                                    <ul>
                                        @php $i=1;@endphp
                                        @foreach($career['career_files'] as $files)
                                            <li>@if($language ==  "ml") {{$i}}.{{$files->title_ml or ''}} @else {{$i}}.{{$files->title or ''}} @endif <a
                                                    href="{{asset('uploads/careers/'.$files->file_link)}}"
                                                    target="_blank">
                                                    <img src="{{asset('ui/images/downloadbr.png')}}"
                                                         alt="{{$files->title or ''}}"></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                    </div>
                @endforeach


            </div>
        </div>
    </section>


@endsection
