@extends('layouts.frontend_template',['page_title'=>'About Us'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('aboutus.About Oushadhi')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <section class="innercontentarea">
        <div class="wid">
            {{--<div class="abtsec">
                <h2 class="innersubhead">Largest producer of ayurveda medcines in public sector in india</h2>
                <p>Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector
                    companies, consistently <br> making profit and paying dividend to Government of Kerala since 1999.
                    Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.</p>
            </div>--}}
            <div class="abtsec1">
                <div class="abtsec1-left">
                    <p>@lang('aboutus.content')

                    </p>
                </div>
                <div class="abtsec1-right">
                    <img src="{{asset('ui/images/about-us.jpg')}}" alt="" title="" class="fullwidth">
                </div>
            </div>
        </div>
    </section>
    <section class="innercontentarea2">
        <div class="wid">
            <div class="incont2-left">
                <img src="{{asset('ui/images/visionmision.jpg')}}" alt=" title" class="fullwidth">
            </div>
            <div class="incont2-right">
                <div class="inc-heading">@lang('aboutus.Our Vision')</div>
                <p>@lang('aboutus.vision'). </p>
                <div class="inc-heading">@lang('aboutus.mission')</div>
                <p>@lang('aboutus.missn')</p>
            </div>
        </div>
    </section>
    <section class="innercontentarea3">
        <div class="incont2-left">
            <div>
                <div class="inc-heading txt-left">@lang('aboutus.Milestones')</div>
                <ul class="com-list">
                    <li><span>1941:</span> @lang('aboutus.milestone1')
                    </li>
                    <li><span>1959:</span> @lang('aboutus.milestone2').
                    </li>
                    <li><span>1975:</span> @lang('aboutus.milestone3').
                    </li>
                    <li><span>1991:</span> @lang('aboutus.milestone4').
                    </li>
                    <li><span>2004:</span> @lang('aboutus.milestone5').</li>
                    <li><span>2007:</span> @lang('aboutus.milestone6').</li>
                    <li><span>2008:</span>  @lang('aboutus.milestone7')
                    </li>
                    <li><span>2014:</span> @lang('aboutus.milestone8').
                    </li>
                    <li><span>2014:</span> @lang('aboutus.milestone9').</li>
                    <li><span>2017:</span> @lang('aboutus.milestone10').</li>
                    <li><span>2018:</span> @lang('aboutus.milestone11').</li>
                    <li><span>2018:</span> @lang('aboutus.milestone12').</li>
                    <li><span>2019:</span> @lang('aboutus.milestone13').</li>
                    <li><span>2019:</span> @lang('aboutus.milestone14').</li>


                </ul>
            </div>
        </div>
        <div class="incont2-right">
            <img src="{{asset('ui/images/milestone.jpg')}}" alt=" title" class="fullwidth">
        </div>
    </section>
    <section class="abt-bottom">
        <div class="wid">
            <div class="abtbottom-right">
                <div class="abtbt-head">@lang('aboutus.Strengths')</div>
                <ul>
                    <li> @lang('aboutus.Strengths1')</li>
                    <li>@lang('aboutus.Strengths2')</li>
                    <li>@lang('aboutus.Strengths3')</li>
                    <li>@lang('aboutus.Strengths4')</li>
                </ul>
                <div class="abtbt-head">@lang('aboutus.CORE VALUES')</div>
                <ul>
                    <li>@lang('aboutus.CORE VALUES1')</li>
                    <li>@lang('aboutus.CORE VALUES2')</li>
                    <li> @lang('aboutus.CORE VALUES3')</li>
                    <li>@lang('aboutus.CORE VALUES4') </li>
                    <li> @lang('aboutus.CORE VALUES5')</li>
                </ul>
            </div>
        </div>
    </section>

@endsection
