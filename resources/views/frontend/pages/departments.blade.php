@extends('layouts.frontend_template',['page_title'=>'Departments'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.Departments')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    @if(isset($departments))
     @php
    $language = "";
    $language = Session::get('language');
    @endphp
        <section class="innercontentarea">
            <div class="wid catogsecnew">
                <div class="tab-wrap">

                    <div class="tabBlock">
                        <h3 class="posihead">@lang('header.Departments')</h3>
                        <ul class="tabBlock-tabs">
                            @foreach($departments as $department)
                            <li class="tabBlock-tab @if ($loop->first) is-active @endif">@if($language == "ml") {{$department->title_ml or $department->title }}@endif</li>
                            @endforeach


                        </ul>
                        <!--tabBlock-content -->
                        <div class="tabBlock-content">
                            <!--tab01 -->
                            @foreach($departments as $department)
                            <div class="tabBlock-pane">
                               
                                <h2 class="headr"> @if($language == "ml") {{$department->title_ml or $department->title }}  @else {{$department->title or ''}} @endif</h2>
                           @if($language == "ml") {!! $department->page_content_ml or $department->page_content !!}  @else      {!! $department->page_content !!} @endif
                            </div>
                            @endforeach
                        </div>
                </div>
            </div>
        </section>
    @endif

@endsection
