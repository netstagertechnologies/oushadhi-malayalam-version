@extends('layouts.frontend_template',['page_title'=>$page_title])
@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title }}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <section class="innercontentarea">
        <div class="wid">
            <div class="blog-main">
                <div class="blog-img">

                    <img src="{{ asset('uploads/blog/'.$page_data->image)}}" alt="" >
                </div>
                <div class="blog-details">
                    <h3 class="smallhead">@if($language == "ml"){{$page_data->title_ml or $page_data->title }} @else {{$page_data->title or ''}}  @endif</h3>
                    <p>@if($language == "ml") {!! $page_data->description_ml or $page_data->description !!} @else {!! $page_data->description or '' !!}  @endif</p>
                </div>
            </div>
        </div>
    </section>
@endsection
