@extends('layouts.frontend_template',['page_title'=>'Downloads'])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.Downloads')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">

            @include('template.frontend.alert')
            <div class="tenters-main">
                <div class="tenters-full new">

                    <div class="stenter-right">
                        <ul>

                            @foreach($downloads as $career)
                                <li><p>
                                        <span>@if($language == "ml") {{$career['title_ml'] or $career['title']}}  @else {{$career['title']}} @endif</span>
                                    </p>
                                    <span class="smalldownld">
                                        @if($career['download_files'])
                                            @foreach($career['download_files'] as $files)
                                                <a href="{{asset('uploads/downloads/'.$files->file_link)}}" target="_blank"><img src="{{asset('ui/images/downsm.png')}}"
                                                                 alt="">{{$files->title or ''}}</a>
                                            @endforeach
                                        @endif
                                    </span>
                                </li>

                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection
