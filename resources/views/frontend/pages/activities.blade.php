@extends('layouts.frontend_template',['page_title'=>'Activities'])

@section('content')
  @php
    $language = "";
    $language = Session::get('language');
    @endphp

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.Activities')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    @if(isset($activities))
        <section class="innercontentarea">
            <div class="wid">
                <div class="activity-main">
                    @foreach($activities as $activity)
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@if($language == "ml") {{$activity->title_ml or ''}} @else {{$activity->title or ''}} @endif</div>
                                <p>@if($language == "ml") {!! $activity->description_ml or '' !!} @else {!! $activity->description or '' !!}} @endif</p>
                            </div>
                            <?php $image = ($activity->image) ? $activity->image : 'placeholder.jpg';?>

                            <div class="activiti-right">
                                <img src="{{ asset('uploads/activity/'.$image)}}" alt="" class="fullwidth">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection
