@extends('layouts.frontend_template',['page_title'=>'Activities of R&D'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.R&D Activities')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <section class="innercontentarea">
        <div class="wid">
            <ul class="com-list">
                <li>@lang('activities.New product development').</li>
                <li>@lang('activities.Process modification').</li>
                <li>@lang('activities.Product modification').</li>
                <li>@lang('activities.Standardization at Raw Material in process and product levels').</li>
                <li>@lang('activities.Inter disciplinary work pattern').</li>
                <li>@lang('activities.Corrective &preventive actions').</li>
                <li>@lang('activities.Extension activities- Training programme for Ayurveda pharmacy MD students, Biotechnology
                    scholars, Microbiology graduates, Biochemistry scholars etc').
                </li>
                <li>@lang('activities.Demonstration Garden & Drug Gallery').</li>
                <li>@lang('activities.Collaborative projects').</li>

            </ul>
        </div>
    </section>
@endsection
