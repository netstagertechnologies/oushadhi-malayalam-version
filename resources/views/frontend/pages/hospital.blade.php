@extends('layouts.frontend_template',['page_title'=>'Hospital'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.Hospital')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="abtsec-cms hosp">
                <img class="hosp-ctr-img" src="{{asset('ui/images/hospitals/hospital.jpg')}}">
                <p>
                <p>@lang('hospital.introduction')</p>

                <p><strong>@lang('hospital.Our Specialties') </strong></p>

                <p><strong>@lang('hospital.PANCHAKARMA THERAPY') </strong></p>

                <p> @lang('hospital.PANCHAKARMA DESC')</p>

                <div class="fsectionContainer">
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/virechana.jpg')}}">
                        <h4>@lang('hospital.VIRECHANA')</h4>
                    </div>
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/vasthi.jpg')}}">
                        <h4>@lang('hospital.VASTHI')</h4>
                    </div>
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/nasya.jpg')}}">
                        <h4>@lang('hospital.NASYA')</h4>
                    </div>
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/rekthamoksha.jpg')}}">
                        <h4>@lang('hospital.RAKTHAMOKSHAM')</h4>
                    </div>
                </div>
                <p> @lang('hospital.SEC1')</p>

                <ol>
                    <li>
                        <p><strong>@lang('hospital.VIRECHANA') : </strong>@lang('hospital.VIRECHANA DESC')</p>
                    </li>
                    <li>
                        <p><strong>@lang('hospital.VASTHI') : </strong>@lang('hospital.VASTHI DESC')</p>
                    </li>
                    <li>
                        <p><strong>@lang('hospital.NASYA'): </strong>@lang('hospital.NASYA DESC')</p>
                    </li>
                    <li>@lang('hospital.RAKTHAMOKSHAM')<strong> : </strong>@lang('hospital.RAKTHAMOKSHAM DESC').
                        <p></p>
                    </li>
                </ol>

                <p><strong>@lang('hospital.RECREATION OF BODY')</strong></p>

                <div class="activity-main">
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.DHARA')</div>
                                <p>@lang('hospital.DHARA DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.PIZHICHIL')</div>
                                <p>@lang('hospital.PIZHICHIL DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.NAVARAKKIZHI')</div>
                                <p>@lang('hospital.NAVARAKKIZHI DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.THAKRADHARA')</div>
                                <p>@lang('hospital.THAKRADHARA DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.THALAPOTHICHIL')</div>
                                <p>@lang('hospital.THALAPOTHICHIL DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.UDHWARTHANAM')</div>
                                <p>@lang('hospital.UDHWARTHANAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.NAVARATHEPPU')</div>
                                <p>@lang('hospital.NAVARATHEPPU DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.SNEHANAM')</div>
                                <p>@lang('hospital.SNEHANAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.SNEHAPANAM')</div>
                                <p>@lang('hospital.SNEHAPANAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.ABHYANGAM')</div>
                                <p>@lang('hospital.ABHYANGAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.PALPPUKA')</div>
                                <p>@lang('hospital.PALPPUKA DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>

                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.UPANAHAM')</div>
                                <p>@lang('hospital.UPANAHAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.SWEDANAM')</div>
                                <p>@lang('hospital.SWEDANAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.KIZHI')</div>
                                <p>@lang('hospital.KIZHI DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.PICHYU')</div>
                                <p>@lang('hospital.PICHYU DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">@lang('hospital.LEPAM')</div>
                                <p>@lang('hospital.LEPAM DESC')</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-bottom">
                        <div class="act-bot-left">
                            <h4>@lang('hospital.REJUVENATION')</h4>
                            <p>
                               @lang('hospital.REJUVENATION DESC')
                            </p>
                            <h4>@lang('hospital.RESEARCH')</h4>
                            <p>@lang('hospital.RESEARCH DESC')</p>
                        </div>
                        <div class="act-bot-right">
                            <h4>@lang('hospital.LOCATION')</h4>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.6090424229365!2d76.20977401428539!3d10.531427066549016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba7ee5a8a6ae3bd%3A0xb42d7bf1c365c2cd!2sOushadhi+Panchakarma+Ayurvefa+Hospital!5e0!3m2!1sen!2sin!4v1564831280579!5m2!1sen!2sin"
                                width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>


                </div>
                <h4>@lang('hospital.FACILITIES')</h4>
                <p>@lang('hospital.FACILITIES DESC')
                </p>
              
            </div>

        </div>
    </section>


@endsection
