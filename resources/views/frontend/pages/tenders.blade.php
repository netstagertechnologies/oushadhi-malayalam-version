@extends('layouts.frontend_template',['page_title'=>'Tenders'])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    @if(isset($tenders))
        <section class="innercontentarea">
            <div class="wid">
                <div class="tenters-main">
                    @foreach($tenders as $tender)
                        <div class="tenters-full">
                            <div class="stenter-left">
                                <p>@if($language=="ml") {{$tender['title_ml']}} @else  {{$tender['title']}} @endif 
                                    @if(!empty($tender['last_date']))      @lang('tender.Last date & Time') :
                                    <strong> {{date('d.m.Y', strtotime($tender['last_date']))}}
                                        , {{$tender['last_time']}}</strong>@endif
                                </p>
                                @if(!empty($tender['dept'])) @lang('tender.Department') : <strong>@if($language ==  "ml") {{$tender['dept_ml'] or ''}} @else {{$tender['dept'] or ''}} @endif</strong><br/>@endif
                                @if(!empty($tender['contact'])&&$tender['contact']!=null)
                                    @lang('tender.Contact No.') :<strong>{{$tender['contact'] or ''}}</strong><br/>
                                @endif
                                @if(!empty($tender['contact_email'])&&$tender['contact_email']!=null)
                                           @lang('tender.Contact Email')  :<strong>{{$tender['contact_email'] or ''}}</strong><br/>
                                @endif
                                @if(!empty($tender['tender_link']))
                                            <strong><a href="{{$tender['tender_link']}}" target="_blank">
                                                @if($tender['tender_link']==1) @lang('tender.Tender Link') @else e-@lang('tender.Tender Link') @endif</a>
                                        </strong><br/>
                                @endif
                            </div>
                                <div class="stenter-right">
                                    @if(isset($tender['tender_files'])&&$tender['tender_files'])
                                    <ul>
                                        @php $i=1;@endphp
                                        @foreach($tender['tender_files'] as $tender_file)
                                            <li>@if($language ==  "ml") {{$tender_file->title_ml or $tender_file->title}} @else {{$tender_file->title or ''}} @endif <a
                                                    href="{{asset('uploads/tenders/'.$tender_file->file_link)}}"
                                                    target="_blank">
                                                    <img src="{{asset('ui/images/downloadbr.png')}}"
                                                         alt="{{$tender_file->title or ''}}"></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>


                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

@endsection
