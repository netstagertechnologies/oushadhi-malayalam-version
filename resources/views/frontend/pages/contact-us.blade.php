@extends('layouts.frontend_template',['page_title'=>'Contact Us'])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('header.Contact Us')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>

    <section class="innercontentarea">
        <div class="wid">

            @include('template.frontend.alert')
            @if(isset($contacts))
                <ul class="ad-list">
                    @foreach($contacts as $contact)
                        <?php $image = ($contact->image) ? $contact->image : 'placeholder.jpg';?>

                        <li>
                            <img src="{{asset('uploads/address/'.$image)}}" alt="">
                            <h3 class="ad-title">@if($language == "ml") {{$contact->title_ml or $contact->title}} @else {{$contact->title or ''}} @endif</h3>
                            <p>@if($language == "ml") {{$contact->address_ml or $contact->address}} @else {{$contact->address or ''}} @endif </p>
                            <p>@lang('contact.Phone'): {{$contact->phone or ''}}</p>
                            @if(!empty($contact->mobile))
                                <p>@lang('contact.Mobile'): {{$contact->phone or ''}}</p>
                            @endif
                            @if(!empty($contact->email))
                                <p>@lang('contact.Email'): {{$contact->email or ''}}</p>
                            @endif
                            @if(!empty($contact->map_link))
                                <a href="{{$contact->map_link}}" class="addMapL"> @lang('contact.View On Map')</a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </section>

@endsection
