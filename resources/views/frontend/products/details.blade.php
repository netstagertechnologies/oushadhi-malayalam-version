@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
 
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="product-detailsin">
                <div class="productdet-left">
                    <div>

                        <input type="hidden" name="ctl00$hfMenu" id="hfMenu" value="0.0"/>

                        <input type="hidden" name="ctl00$cphBody$__VIEWxSTATE" id="__VIEWxSTATE" value="MTE2Njs2Ozk="/>

                    </div>


                    <div class="container">
                        <div class="row">

                            <ul id='girlstop1' class='gc-start'>
                                <?php $image = ($product->image) ? $product->image : 'placeholder.jpg';?>

                                <li><img src="{{asset('uploads/product/'.$image)}}" alt='image1'
                                         data-gc-caption="@if($language == 'ml'){{$product->title_ml or ''}}@else {{$product->title or ''}} @endif"/></li>
                                @if(isset($product_images))
                                    @foreach($product_images as $i=>$product_image)
                                        <?php $image = ($product->image) ? $product->image : 'placeholder.jpg';?>

                                        <li><img src="{{asset('uploads/product/'.$image)}}" alt='image{{$i+2}}'
                                                 data-gc-caption="{{$product->title.'-'.($i+2) }}"/></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="pInstructions">
                               @lang('products.Roll over image to zoom in') 
                            </div>


                        </div>
                    </div>
                </div>
                <div class="productdet-right">
                    <div class="pro-rigt-top">
                        <div class="prodet-head">@if($language == "ml"){{$product->title_ml or $product->title}}@else {{$product->title or ''}} @endif</div>
                        @if(!empty($product->total_ingradients))
                            <p><strong>@lang('products.Total Ingredients')</strong>:  {{$product->total_ingradients}} </p>
                        @endif
                        @if(!empty($product->main_ingradients))
                            <p><strong>@lang('products.Main Ingredients')</strong>: @if($language == "ml"){{$product->main_ingradients_ml}} @else {{$product->main_ingradients}} @endif </p>
                        @endif
                        @if(!empty($product->indication))
                            <p><strong>@lang('products.Indication')</strong>:  @if($language == "ml"){{$product->indication_ml}} @else {{$product->indication}} @endif </p>
                        @endif
                        @if(!empty($product->special_indication))
                            <p><strong>@lang('products.Special Indication')</strong>: @if($language == "ml"){{$product->special_indication_ml}} @else {{$product->special_indication}} @endif </p>
                        @endif
                        @if(!empty($product->usage))
                            <p><strong>@lang('products.Usage')</strong>: {{$product->usage}} </p>
                        @endif
                        @if(!empty($product->dosage))
                            <p><strong>@lang('products.Dosage')</strong>: @if($language == "ml") {{$product->dosage_ml or $product->dosage }} @else {{$product->dosage}} @endif</p>
                        @endif
                        @if(!empty($product->presentation))
                            <p><strong>@lang('products.Presentation')</strong>: {{$product->presentation}} </p>
                        @endif
                    </div>
                    <div class="pro-rigt-bottom">
                        <span class="somedia">@lang('products.Share') : <a target="_blank" href="https://www.facebook.com/dialog/feed?app_id=295465317810903&display=popup&caption=Oushadhi&name={{$product->title or '' }}&link={{ URL::to('/').'/product/'.$product->slug}}&picture={{asset('uploads/product/'.$image)}}&description=Click to view more details&redirect_uri={{ URL::to('/').'/product/'.$product->slug}}"><img src="{{asset('ui/images/fbp.png')}}" alt=""> </a>
                            <a target="_blank" href="https://www.tumblr.com/share"><img src="{{asset('ui/images/tp.png')}}" alt=""></a>
                            <a target="_blank" href="http://twitter.com/share?text=$product->title or '' Visit : &url={{ URL::to('/').'/product/'.$product->slug}}"><img src="{{asset('ui/images/twp.png')}}" alt=""></a>
                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{ URL::to('/').'/product/'.$product->slug}}&title={{$product->title or '' }}&source=LinkedIn"><img src="{{asset('ui/images/inp.png')}}" alt=""></a>
                            <a target="_blank"href="https://plus.google.com/share?url={{ URL::to('/').'/product/'.$product->slug}}"> <img src="{{asset('ui/images/gpp.png')}}" alt=""></a>

                        </span>
                        <span class="star">@lang('products.Review')  :<ul>
                                @for($rt=0;$rt<5;$rt++)
                                    @if($rt<(int)$product->rating)
                                        <li class="active"></li>
                                    @else
                                        <li></li>
                                    @endif
                                @endfor
                            </ul> </span>


                    </div>
                </div>

                <div class="product-tab">
                    <div class="tab-wrap">
                        <div class="tabBlock">
                            <ul class="tabBlock-tabs">
                                <li class="tabBlock-tab is-active">@lang('products.Description')</li>
                                <li class="tabBlock-tab">@lang('products.Additional information')</li>
                            </ul>
                            <!--tabBlock-content -->
                            <div class="tabBlock-content">
                                <!--tab01 -->
                                <div class="tabBlock-pane">
                                   @if($language == "ml"){!! $product->description_ml or $product->description !!}@else {!! $product->description !!} @endif
                                </div>
                                <!--/tab01 -->
                                <!--tab02 -->
                                <div class="tabBlock-pane">
                        @if($language == "ml"){!! $product->description_ml or $product->description !!}@else {!! $product->description !!} @endif

                                </div>
                                <!--tab02 -->
                            </div>
                            <!--/tabBlock-content -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--Slider Ends-->
    <script src="{{ asset('ui/js/jquery-1.12.4.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function (event) {
            $('.pInstructions').hide();
            //ZOOM
            $("#girlstop1").glassCase({
                'widthDisplay': 430,
                'heightDisplay': 300,
                'isSlowZoom': true,
                'isSlowLens': true,
                'capZType': 'in',
                'thumbsPosition': 'bottom',
                'isHoverShowThumbs': true,
                'colorIcons': '#F15129',
                'colorActiveThumb': 'rgb(255, 216, 0)',
                'mouseEnterDisplayCB': function () {
                    $('.pInstructions').text('Click to open expanded view');
                },
                'mouseLeaveDisplayCB': function () {
                    $('.pInstructions').text('Roll over image to zoom in');
                }
            });
            setTimeout(function () {
                $('.pInstructions').css({
                    'width': $('.gc-display-area').outerWidth(),
                    'left': parseFloat($('.gc-display-area').css('left'))
                });
                $('.pInstructions').fadeIn();
            }, 1000);

            $('#btnFeatures').on('click', function () {
                $('html, body').animate({
                    scrollTop: $('.tc-all-features').offset().top - 50 + 'px'
                }, 800);
            });
        });
    </script>

    <script id="tumblr-js" async src="https://assets.tumblr.com/share-button.js"></script>

@endsection
