@extends('layouts.frontend_template',['page_title'=>($page_title)?$page_title:'Siddha Products'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title or ''}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="productSec">
                <div class="product-left">
                    <div class="product-lefthead">Product Categories</div>
                    <ul>
                        @if(isset($categories))
                         @php
                        $language = "";
                        $language = Session::get('language');
                        @endphp
                            @foreach($categories as $category)
                                <li class="@if(isset($category_selected)){{($category->slug==$category_selected)?'active':''}}@endif">
                                    <a href="{{ URL::to('/').'/cs/'.$category->slug}}">@if($language == "ml") {{$category->title_ml or $category->title}}  @else {{$category->title}} @endif</a></li>
                            @endforeach
                        @endif
                    </ul>

                </div>
                <div class="product-right">
                    <div class="prodLeftTop">
                        <form action="{{$form_action}}" method="" id="prodListSear" class="produSerForm">
                            <div class="proFrm-left">
                                <input type="text" name="search" value="{{ Request::get('search') }}"
                                       placeholder="@lang('products.--Search Product--')" class="serchproinput">
                                <input type="submit" value="Search" class="serchbtn">
                            </div>
                            @php $sort=(Request::get('sort'))? Request::get('sort'):'';@endphp
                            <div class="proFrm-right">@lang('header.Sort By') : <select name="sort" onchange="this.form.submit()">
                                    <option {{$sort==''?'selected':''}} value="">Default</option>
                                    <option {{$sort=='name-az'?'selected':''}} value="name-az">Name A-Z</option>
                                    <option {{$sort=='name-za'?'selected':''}} value="name-za">Name Z-A</option>
                                </select></div>
                        </form>
                    </div>
                    <div class="@if(isset($category_selected)&&$category_selected=='patent-&-proprietary')product-listing @else product-listing smallProducts @endif">
                        @if(isset($products))
                            <ul class="productslist">
                                @foreach($products as $product)
                                    <li>
                                        <?php $image = ($product->image) ? $product->image : 'placeholder.jpg';?>

                                        <a href="{{ URL::to('/').'/product/'.$product->slug}}"
                                           title="{{$product->title or ''}}">
                                            <div class="cvrprdts">
                                                <div class="prdtimg"><img src="{{asset('uploads/product/'.$image)}}"
                                                                          title="{{$product->title or ''}}"
                                                                          alt="{{$product->title or ''}}"></div>
                                                <div class="prdtname">@if($language == "ml") {{$product->title_ml or $product->title}}  @else {{$product->title or ''}} @endif</div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>

                            {{ $products->appends($_GET)->links() }}
                        @endif
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection
