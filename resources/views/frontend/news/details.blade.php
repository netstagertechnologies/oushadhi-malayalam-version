@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">@lang('index.News')</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="News-list">
                <div class="news-box news-details">

                    <div class="news-main">
                        <?php $news_date = strtotime($page_data->date_added);?>

                        <div class="ImageWrapper">
                            <img class="fullwidth" src="{{ asset('uploads/news/'.$page_data->image)}}" alt="">
                            <span class="date">
                                <span class="day">{{date('d',$news_date)}}</span>
                                       <span class="month">{{date('M',$news_date)}}</span>
                                        <span class="day">{{date('Y',$news_date)}}</span>
                                </span>
                        </div>
                        <div class="news-content">
                            <h2>@if($language == "ml") {{$page_data->title_ml or $page_data->title_ml}} @else {{$page_data->title}} @endif</h2>
                               @if($language == "ml") {!! $page_data->description_ml or  $page_data->description !!} @else  {!! $page_data->description  !!}  @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="innercontentarea">
        <div class="wid">
            <div class="gallery-tab">

                <div class="demo-gallery">

                    <ul id="lightgallery" class="list-unstyled row">
                        @if(isset($media_images))
                            @foreach($media_images as $media_image)
                                <li data-responsive="{{ asset('uploads/media/'.$media_image->image)}}"
                                    data-src="{{ asset('uploads/media/'.$media_image->image)}}"
                                    data-sub-html="">
                                    <a href="">
                                        <div class="hover-effect">
                                            <i class="zoom" aria-hidden="true"></i>
                                            <img src="{{ asset('uploads/media/'.$media_image->image)}}" alt="">
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('page-script')
    <!--Phto gallery scripts -->
    <script src="{{ asset('ui/js/jquery-3.3.1.min.js')}}"
            integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lightgallery').lightGallery();
        });
    </script>
    <script src="{{ asset('ui/js/lightgallery-all.min.js')}}"></script>

@endsection
