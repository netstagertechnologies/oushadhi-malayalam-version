@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    @php
    $language = "";
    $language = Session::get('language');
    @endphp
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title or ''}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="News-list">
                @if(isset($news))
                    @foreach($news as $medi)
                        <?php $image = ($medi->image) ? $medi->image : 'placeholder.jpg';
                        $news_date  = strtotime($medi->date_added);?>
                        <div class="news-box">

                            <div class="news-main">
                                   <span class="date">
                                       <span class="day">{{date('d',$news_date)}}</span>
                                       <span class="month">{{date('M',$news_date)}}</span>
                                        <span class="day">{{date('Y',$news_date)}}</span>
                                   </span>
                                <div class="ImageWrapper">
                                    <img class="fullwidth" src="{{ asset('uploads/news/'.$image)}}" alt="">
                                </div>
                                <div class="news-content">
                                    <h2>@if($language == "ml") {{$medi->title_ml or $medi->title}} @else{{$medi->title}} @endif</h2>
                                        @if($language == "ml") {!! $medi->description_ml or  $medi->description !!} @else  {!! $medi->description  !!}  @endif 
                                    <a href="{{ URL::to('/').'/news/'.$medi->slug}}" class="readmore">@lang('index.Read More')</a>
                                </div>
                            </div>

                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>


@endsection
