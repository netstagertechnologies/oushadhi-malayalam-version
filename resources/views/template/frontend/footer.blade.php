 @php
    $language = "";
    $language = Session::get('language');
    @endphp
.<footer class="{{$footer_class or ''}}">
    <div class="wid">
        <div class="fotdrs">
            <a href="{{route('/')}}" class="footerlogo" title="Oushadhi"><img
                    src="{{ asset('ui/images/footerlogo.png')}}" title="Oushadhi"></a>
            <p>@if($language == "ml") {!! $site_settings['config_address_ml'] !!} @else {!! $site_settings['config_address'] or '' !!} @endif </p>
            <ul class="phnemail">
                <li class="pHne"><a href="tel:{{$site_settings['config_contact_number1'] or ''}}"
                                    title="{{$site_settings['config_contact_number1'] or ''}}">{{$site_settings['config_contact_number1'] or ''}}</a>
                </li>
                <li><a href="tel:{{$site_settings['config_contact_number2'] or ''}}"
                       title="{{$site_settings['config_contact_number2'] or ''}}">{{$site_settings['config_contact_number2'] or ''}}</a>
                </li>
                <li class="maIl"><a href="mailto:{{$site_settings['config_contact_email'] or ''}}"
                                    title="{{$site_settings['config_contact_email'] or ''}}">{{$site_settings['config_contact_email'] or ''}}</a>
                </li>
            </ul>
        </div>
        <div class="fotmap">
            <iframe
                src="{{ $site_settings['config_map_link'] or '#' }}"
                width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="qklinks">
            <span class="ftrhed">@lang('header.QUICK LINKS')</span>
            <ul>
                <li><a href="{{route('/')}}">@lang('header.home')</a></li>
                <li><a href="{{route('products')}}">@lang('header.Products')</a></li>
                <li><a href="{{route('about-us')}}">@lang('header.about us')</a></li>
                <li><a href="{{ URL::to('/').'/hospital'}}">@lang('header.Hospital')</a></li>
                <li><a href="{{route('contact-us')}}">@lang('header.Contact Us')</a></li>
                <li><a href="{{route('downloads')}}">@lang('header.Downloads')</a></li>
                <li><a href="{{ URL::to('/').'/rti-act'}}">@lang('header.RTI')</a></li>


            </ul>
        </div>
        <div class="copyrgt">
            <p>© <?php echo date("Y")?> @lang('header.copyright')</p>
            <div class="social">
                <span>@lang('header.FOLLOW US') :</span>
                <ul>
                    <li><a class="fb" target="_blank" href="{{ $site_settings['config_facebook_page'] or '#' }}"
                           title="facebook"></a></li>
                    <li><a class="tw" target="_blank" href="{{ $site_settings['config_twitter_page'] or '#' }}"
                           title="twitter"></a></li>
                    <li><a class="ig" target="_blank" href="{{ $site_settings['config_instagram_page'] or '#' }}"
                           title="instagram"></a></li>
                    <p id="container-box"></p>
                </ul>
            </div>

        </div>
    </div>
</footer>
<script>

</script>