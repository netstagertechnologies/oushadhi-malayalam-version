@if ($message = Session::get('info'))
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Note!</strong> {{ $message }}
    </div>
@elseif($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{ $message }}
    </div>
@elseif($message = Session::get('error'))
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Sorry!</strong> {{ $message }}
    </div>
@elseif($message = Session::get('warning'))
    <div class="alert alert-warning">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Warning!</strong> {{ $message }}
    </div>
@elseif ($message = Session::get('status'))
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Note!</strong> {{ $message }}
    </div>
@endif
