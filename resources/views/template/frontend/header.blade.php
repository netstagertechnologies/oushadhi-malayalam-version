<header>
    <div class="wid">
        <div class="logo"><a href="{{route('/')}}" title="Oushadhi"><img src="{{ asset('ui/images/logo.png')}}"
                                                                         title="Oushadhi" alt="Oushadhi logo"></a></div>
        <div class="logorgt">
            <div class="navtop">
                <ul class="phnemail">
                    <li class="pHne"><a href="tel:{{$site_settings['config_contact_number1'] or ''}}"
                                        title="{{$site_settings['config_contact_number1'] or '#'}}">{{$site_settings['config_contact_number1'] or '#'}}</a>
                    </li>
                    <li><a href="tel:{{$site_settings['config_contact_number2'] or ''}}"
                           title="{{$site_settings['config_contact_number2'] or ''}}">{{$site_settings['config_contact_number2'] or ''}}</a>
                    </li>
                    <li class="maIl"><a href="mailto:marketing@oushadhi.org" title="marketing@oushadhi.org">marketing@oushadhi.org</a>
                    </li>
                </ul>
                <ul class="logpanel">
                    @guest
                        <li><a href="{{ route('register') }}">Doctors Sign Up</a>/</li>
                        <li><a href="{{ route('login') }}">Login</a></li>
                    @else
                        <li><a href="{{ route('user.home') }}">{{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endguest
                    
                    <li>
                    @php $lang ="" @endphp
                    @if(Session::has('language'))
                    @php $lang = Session::get('language') @endphp
                    @endif
                    <select name="lang" id="lang">
                    <!-- <option>Lang</option> -->
                    <option value="en" @if($lang=="en" )selected="selected" @endif>ENGLISH</option>
                    <option value="ml"  @if($lang=="ml" )selected="selected" @endif >MALAYALAM</option>
                    </select>
                    </li>
                </ul>

            </div>
            <div class="menuD">
                <a href="#menu" id="nav-toggle" class="menu-link">
                    <span></span>
                </a>
                <nav id="menu" class="menu">
                    <ul class="level-01">
                        <li><a href="{{route('/')}}" title="@lang('header.home')">@lang('header.home')</a></li>
                 <li><a href="{{route('about-us')}}" title="@lang('header.about us')">@lang('header.about us')</a>
                            <span class="has-subnav"></span>
                            <ul class="level-1">
                                <li><a href="{{route('about-us')}}">@lang('header.about us')</a></li>
                                <li><a href="{{route('directors')}}">@lang('header.Board Directors')</a></li>
                                <li><a href="{{route('officers')}}">@lang('header.Officers')</a></li>
                                <li><a href="{{route('departments')}}">@lang('header.Departments')</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('products')}}" title="@lang('header.Products')">@lang('header.Products')</a>
                            <span class="has-subnav"></span>
                            <ul class="level-1">
                                <li><a href="{{route('products')}}">@lang('header.Ayurveda')</a></li>
                                <li><a href="{{route('s-products')}}">@lang('header.Siddha')</a></li>
                            </ul>
                        </li>
                                        <li><a href="{{route('activities')}}" title="@lang('header.Activities')">@lang('header.Activities')
                                <span class="has-subnav"></span>
                                <ul class="level-1">
                                    <li><a href="{{route('r-and-d-activities')}}">@lang('header.R&D Activities')</a></li>
                                    <li><a href="{{ URL::to('/').'/csr-activities'}}">@lang('header.CSR Activities')</a></li>
                                    <li><a href="{{ URL::to('/').'/medicinal-plant-cultivation'}}">@lang('header.Medicinal Plant Cultivation')</a></li>
                                </ul>
                            </a>
                        </li>
                        <li><a href="{{ URL::to('/').'/hospital'}}" title="@lang('header.Hospital')">@lang('header.Hospital')</a></li>
                        <li><a href="{{route('media')}}" title="@lang('header.Media')">@lang('header.Media')</a></li>
                        <li><a href="{{route('tenders')}}" title="@lang('header.Tenders')">@lang('header.Tenders')</a></li>
                        <li><a href="{{ URL::to('/').'/careers'}}" title="@lang('header.Careers')">@lang('header.Careers')</a></li>
                        <li><a href="{{route('contact-us')}}" title="@lang('header.Contact Us')">@lang('header.Contact Us')</a></li>
                        <li><a href="{{route('raw-materials-required')}}" title="@lang('header.Raw Materials')">@lang('header.Raw Materials')</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
