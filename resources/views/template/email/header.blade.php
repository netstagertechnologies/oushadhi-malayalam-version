<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{$title or 'Oushadhi'}}</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>

<body style="margin:0;padding:0;">
<table width="650" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" bgcolor="#fff" style="border-bottom:1px #fff solid;" valign="top"><img src="{{asset('/ui/images/logo.png')}}" width="108" height="61"></td>
    </tr>
